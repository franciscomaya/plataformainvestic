﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    /// <summary>
    /// Cadena de conexión que se verifica en tiempo de ejecución, buena
    /// práctica de seguridad.
    /// </summary>
    public static class Utilities
    {
        public static string ConnectionString = "";
    }

    /// <summary>
    /// Codificación de los errores de excepciones del sistema.
    /// </summary>
    public static class ErrorCodes
    {
        public static string ErrorCodeToString(int code)
        {
            switch (code)
            {
                case 10:
                    return "Datos ingresados satisfactoriamente";
                case 11:
                    return "La identificación o la contraseña del usuario no son correctos.";
                case 12:
                    return "La identificación del usuario no se encuentra registrada en la plataforma.";
                case 13:
                    return "Para acceder a la plataforma necesitas confirmar tu cuenta siguiendo el enlace que se envió a tu correo durante el registro.";
                case 14:
                    return "Se han eliminado los usuarios registrados que no confirmaron su correo electrónico.";
                case 15:
                    return "Datos modificados satisfactoriamente";
                case 16:
                    return "Contraseña cambiada satisfactoriamente";
                case 17:
                   return "No fue posible cambiar la contraseña, revisa que has ingresado correctamente tu contraseña actual.";
                case 20:
                    return "El usuario ya se encuentra registrado";
                case 25:
                    return "Se presentó un problema al modificar los datos";
                case 30:
                    return "El usuario se registro con éxito";
                case 31:
                    return "Datos de usuario actualizados.";
                case 32:
                    return "El usuario ha sido eliminado de la plataforma.";
                case 33:
                    return "No se pudo eliminar, revise las referencias circulares que tiene este registro.";
                case 34:
                    return "No se pudo editar, ya existe un usuario con el número de identificación ingresado.";
                case 35:
                    return "No se pudo crear el usuario, ya existe un usuario con el número de identificación ingresado.";
                case 100:
                    return "Grupo registrado satisfactoriamente.";
                case 101:
                    return "Colaborador agregado. Se acaba de enviar una invitación al maestro para pertenecer al grupo, cuando acepte la invitación será visible en el listado.";
                case 102:
                    return "Estudiante agregado. Se acaba de enviar una invitación al estudiante para pertenecer al grupo, cuando acepte la invitación será visible en el listado.";
                case 103:
                    return "No existe una invitación para ese grupo de investigación.";
                case 104:
                    return "La invitación al grupo de investigación se ha rechazado.";
                case 105:
                    return "No se pudo registrar el grupo, verifique que el nombre del grupo no se encuentre repetido en su institución.";
                case 106:
                    return "Este usuario fue invitado anteriormente para pertenecer al grupo, cuando acepte la invitación será visible en el listado.";
                case 110:
                    return "Bitácora para el maestro actualizada satisfactoriamente.";
                case 120:
                    return "Bitácora para el grupo actualizada satisfactoriamente.";
                case 121:
                    return "Feria agregada satisfactoriamente";
                case 122:
                    return "Error al agregar la feria";
                case 130:
                    return "Archivo cargado satisfactoriamente.";
                case 135:
                    return "Error al cargar el archivo.";
                case 150:
                    return "Solo puede existir un grupo de investigación por usuario.";
                case 160:
                    return "Error al agregar maestro coinvestigador, contacte con el administrador de la plataforma.";
                case 200:
                    return "Datos ingresados satisfactoriamente.";
                case 300:
                    return "Miembro agregado satisfactoriamente.";
                case 400:
                    return "Presupuesto actualizado satisfactoriamente.";
                case 410:
                    return "Presupuesto eliminado satisfactoriamente.";
                case 411:
                    return "Aún no ha registrado presupuesto.";
                case 500:
                    return "Foro creado satisfactoriamente.";
                case 510:
                    return "Respuesta a foro creada satisfactoriamente.";
                case 511:
                    return "No fue posible crear el foro, ya existe un registro con el mismo título y mensaje en el grupo.";
                case 600:
                    return "Estado del arte agregado satisfactoriamente";
                case 601:
                    return "Concepto agregado satisfactoriamente";
                case 602:
                    return "Información agregada satisfactoriamente";
                case 603:
                    return "Reflexión de la onda agregada satisfactoriamente";
                case 604:
                    return "Concepto editado satisfactoriamente";
                case 605:
                    return "Información editada satisfactoriamente";
                case 606:
                    return "Feria editada satisfactoriamente";
                case 999:
                    return "No tiene privilegios para esta operación.";
                case 13901:
                    return "El usuario ha sido eliminado del grupo de investigación";
                case 13902:
                    return "El usuario que intentas eliminar es el creador del grupo";
                case 13903:
                    return "No es posible eliminar ese registro pues está siendo utilizado en otros módulos de la plataforma";
                case 1390301:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390302:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390303:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390304:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390305:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390306:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390307:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 1390308:
                    return "No es posible eliminar ese registro pues se encuentra asociado a una investigación";
                case 13904:
                    return "No es posible modificar ese registro";
                case 13905:
                    return "No puedes crear dos elementos con el mismo nombre";
                case 13906:
                    return "Información proyecto actualizada con éxito.";
                case 13907:
                    return "El archivo XML que intentas subir no hace referencia al proyecto actual";
                case 13908:
                    return "Error al leer archivo XML";
                case 13909:
                    return "No tienes privilegios para eliminar este recurso";
                case 13910:
                    return "El recurso educativo ha sido eliminado";
                case 13911:
                    return "Email de contáctanos actualizado con éxito";
                case 13912:
                    return "Error al actualizar email de contáctanos";
                case 13913:
                    return "Hola, después de haber aceptado las condiciones para la eliminación de tu cuenta de la Plataforma Tecnológica Investic, hemos recibido tu solicitud y una vez realizado el procedimiento te informaremos por tú cuenta de correo electrónico. Gracias, Plataforma Tecnológica Investic. ";
                case 13914:
                    return "Error de solicitud, intenta de nuevo.";
                case 13915:
                    return "Si tus datos son correctos, llegará a tu cuenta de correo una solicitud de “Revisión de un intento de acceso bloqueado” y activa  el acceso. Después de que actives el acceso, regresa a la Plataforma y haz el proceso nuevamente. Para más información, revisa https://support.google.com/accounts/answer/6010255?hl=es-419";
                case 13916:
                    return "Registro eliminado satisfactoriamente.";
                case 13917:
                    return "No se puede eliminar esta categoría porque tiene imágenes asociadas.";
                case 13918:
                    return "Link Centro Virtual de Emprendimiento actualizado.";
                case 13919:
                    return "Error al actualizar link.";
                  default:
                    return "";
            }
        }
    }

    /// <summary>
    /// Codigos para algunos objetos del sistema.
    /// </summary>
    public static class Codigos
    {
        /// <summary>
        /// Mapa Conceptual
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        public static string CMap(string groupName, string ext)
        {
            var name = string.Format("MapaC-{0}{1}", groupName.Replace(" ", ""), ext.ToUpper());
            return name;
        }

        /// <summary>
        /// Evidencia informacion
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        public static string Evidence(string groupName, int index, string ext)
        {
            var name = string.Format("EvIn-{2}-{0}{1}", groupName.Replace(" ", ""), ext.ToUpper(), index.ToString());
            return name;
        }

        /// <summary>
        /// Carta de Aceptacion
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        public static string Acceptance(string groupName, string ext)
        {
            var name = string.Format("Aceptación-{0}{1}", groupName.Replace(" ", ""), ext.ToUpper());
            return name;
        }

        /// <summary>
        /// Carta de compromiso
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        public static string Commitment(string groupName, string ext)
        {
            var name = string.Format("Compromiso-{0}{1}", groupName.Replace(" ", ""), ext.ToUpper());
            return name;
        }

        public static string ImgInstitucional(string groupName, string ext, int index)
        {
            var name = string.Format("ImgInst-{2}-{0}{1}", groupName.Replace(" ", ""), ext.ToUpper(), index);
            return name;
        }

        /// <summary>
        /// Código brupo de investigación
        /// </summary>
        /// <returns></returns>
        public static string ResearchGroupCode()
        {
            string cod = string.Format("GI-{0}", Codigo());
            return cod;
        }

        /// <summary>
        /// Código aleatorio
        /// </summary>
        /// <returns></returns>
        public static string Codigo()
        {
            StringBuilder cod = new StringBuilder();
            string[] letras = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            Random rnd = new Random();
            for (int i = 0; i < 5; i++)
            {
                cod.Append(letras[rnd.Next(0, 35)]);
            }
            return cod.ToString();
        }
    }
}
