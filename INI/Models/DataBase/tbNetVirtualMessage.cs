//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(tbNetVirtualMessage_Metadata))]
    public partial class tbNetVirtualMessage
    {
        public int id { get; set; }
        public string message { get; set; }
        public int idGroup { get; set; }
        public System.DateTime dateSend { get; set; }
    
        public virtual tbNetVirtualGroup tbNetVirtualGroup { get; set; }
    }
}
