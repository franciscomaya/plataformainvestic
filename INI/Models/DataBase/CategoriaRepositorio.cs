//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(CategoriaRepositorio_Metadata))]
    public partial class CategoriaRepositorio
    {
        public CategoriaRepositorio()
        {
            this.SubCategoriaRepositorio = new HashSet<SubCategoriaRepositorio>();
        }
    
        public int id { get; set; }
        public string nombre { get; set; }
        public string urlimg { get; set; }
    
        public virtual ICollection<SubCategoriaRepositorio> SubCategoriaRepositorio { get; set; }
    }
}
