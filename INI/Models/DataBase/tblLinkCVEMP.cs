﻿
namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(tblLinkCVEMP_Metadata))]
    public partial class tblLinkCVEMP
    {    
		public int id { get; set; }
		
        public string categoria { get; set; }
		
        public string elemento { get; set; }

        public string link { get; set; }

        public IEnumerable<tblLinkCVEMP> tblLinkCVEMPs { get; set; }
    }
}