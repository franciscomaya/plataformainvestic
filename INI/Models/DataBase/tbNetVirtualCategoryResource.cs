//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(tbNetVirtualCategoryResource_Metadata))]
    public partial class tbNetVirtualCategoryResource
    {
        public tbNetVirtualCategoryResource()
        {
            this.tbNetVirtualResource = new HashSet<tbNetVirtualResource>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public byte[] image { get; set; }
        public string JsonMetadata { get; set; }
    
        public virtual ICollection<tbNetVirtualResource> tbNetVirtualResource { get; set; }
    }
}
