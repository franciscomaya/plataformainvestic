﻿
namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;
    public partial class tblValoracionBitacora
    {
        [Display(Name="idValoracion")]
        [Range(0,int.MaxValue)]
		public int idValoracion { get; set; }
		
        [Display(Name= "Grupo Investigación")]
        [Range(0,int.MaxValue)]
		public int idGrupoInvestigacion { get; set; }
		
        [Display(Name= "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string descripcionValoracion { get; set; }
		
        [Display(Name= "Archivo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo es obligatorio")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string archivoValoracion { get; set; }
		
        public virtual tblGrupoInvestigacion tblGrupoInvestigacion { get; set; }
    }
}