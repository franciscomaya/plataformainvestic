﻿
namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;
    public partial class tblParticipacionGrupo
    {
        [Display(Name="idParticipacion")]
        [Range(0,int.MaxValue)]
		public int idParticipacion { get; set; }
		
        [Display(Name= "Grupo Investigación")]
        [Range(0,int.MaxValue)]
		public int idGrupoInvestigacion { get; set; }
		
        [Display(Name= "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(200, ErrorMessage = "Máximo 200 caracteres")]
        public string descripcionParticipacion { get; set; }
		
        [Display(Name= "Archivo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo es obligatorio")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string archivoParticipacion { get; set; }
		
        public virtual tblGrupoInvestigacion tblGrupoInvestigacion { get; set; }
    }
}