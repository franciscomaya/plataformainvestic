﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class tblDesactivarCuenta
    {
        public int id { get; set; }

        public string idUsuario { get; set; }

        public string PersonalID { get; set; }

        public string Name { get; set; }

        public string SurName { get; set; }

        public string Email { get; set; }

        public string Celular { get; set; }

        public string Fecha { get; set; }

        public Boolean Activo { get; set; }
    }
}