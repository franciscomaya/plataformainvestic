//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(tblTipoInstitucion_Metadata))]
    public partial class tblTipoInstitucion
    {
        public tblTipoInstitucion()
        {
            this.tblInstitucion = new HashSet<tblInstitucion>();
        }
    
        public long id { get; set; }
        public string Nombre { get; set; }
    
        public virtual ICollection<tblInstitucion> tblInstitucion { get; set; }
    }
}
