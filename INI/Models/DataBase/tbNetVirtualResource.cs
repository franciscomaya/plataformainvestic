//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(tbNetVirtualResource_Metadata))]
    public partial class tbNetVirtualResource
    {
        public int id { get; set; }
        public System.Guid idNetVirtualUser { get; set; }
        public byte[] resource { get; set; }
        public string JsonMetadata { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<int> idCategory { get; set; }
    
        public virtual tbNetVirtualUser tbNetVirtualUser { get; set; }
        public virtual tbNetVirtualCategoryResource tbNetVirtualCategoryResource { get; set; }
    }
}
