//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(tbNetVirtualForo_Metadata))]
    public partial class tbNetVirtualForo
    {
        public tbNetVirtualForo()
        {
            this.tbNetVirtualForo1 = new HashSet<tbNetVirtualForo>();
        }
    
        public int id { get; set; }
        public System.Guid idUser { get; set; }
        public int idGrupoInvestigacion { get; set; }
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public System.DateTime Fecha { get; set; }
        public int Respuestas { get; set; }
        public Nullable<int> idForo { get; set; }
        public Nullable<System.DateTime> FechaUltimaRespuesta { get; set; }
        public bool ishide { get; set; }
    
        public virtual ICollection<tbNetVirtualForo> tbNetVirtualForo1 { get; set; }
        public virtual tbNetVirtualForo tbNetVirtualForo2 { get; set; }
        public virtual tbNetVirtualGroup tbNetVirtualGroup { get; set; }
        public virtual tbNetVirtualUser tbNetVirtualUser { get; set; }
    }
}
