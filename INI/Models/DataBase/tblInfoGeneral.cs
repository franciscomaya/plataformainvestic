﻿
namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(tblInfoGeneral_Metadata))]
    public partial class tblInfoGeneral
    {
		public int id { get; set; }
		
        public string nombre { get; set; }
		
        public string campo { get; set; }

        public string password { get; set; }
    }
}