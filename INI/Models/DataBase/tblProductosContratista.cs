//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INI.Models.DataBase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(tblProductosContratista_Metadata))]
    public partial class tblProductosContratista
    {
        public int Id_Producto { get; set; }
        public string Id_Contratista { get; set; }
        public int Id_Actividad { get; set; }
        public string Nombre_Producto { get; set; }
        public string Descripcion_Producto { get; set; }
    
        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual tblActividadContratista tblActividadContratista { get; set; }
    }
}
