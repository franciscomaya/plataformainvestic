﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace INI.Models.RVC
{
    public class NetVirtualResource
    {
        public int id { get; set; }

        public string name { get; set; }


        public string description { get; set; }

        public int? idCategory { get; set; }
        public String Category { get; set; }
    }
}