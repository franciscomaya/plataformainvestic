﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Web.Mvc;
using System.Collections.Generic;

namespace INI.Models
{    

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Constraseña actual")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "La {0} debe tener al menos {2} caracteres de longitud.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Las contraseñas no coinciden.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Usuario", Prompt = "Usuario")]
        public string UserName { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }



        [Display(Name = "Recordarme?")]
        public bool RememberMe { get; set; }


        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Altitud { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Usuario", Prompt = "Usuario")]
        public string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La clave es obligatoria")]
        [StringLength(32, ErrorMessage = "La {0} debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña", Prompt = "Contraseña")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La confirmación de clave es obligatoria")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña", Prompt = "Confirmar Contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Las claves no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [Display(Name = "Nombres", Prompt = "Nombres")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido para nombres")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El apellido es obligatorio")]
        [Display(Name = "Apellidos", Prompt = "Apellidos")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido para apellidos")]
        [StringLength(50, ErrorMessage = "Máximo {1} caracteres")]
        public string SureName { get; set; }
        

        [Display(Name = "Tipo de Documento")]
        public int TipoDoc { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El número del documento es obligatorio")]
        [Display(Name = "Documento", Prompt = "Documento")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido en documento")]
        [StringLength(12, ErrorMessage = "El {0} debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 8)]
        public string PersonalID { get; set; }

        [Display(Name = "Género")]        
        public int Genre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [Display(Name = "Teléfono")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Número de teléfono no válido. Solo debes ingresar números sin puntos ni comas.")]
        [StringLength(16, ErrorMessage = "Máximo {1} caracteres.")]
        public string Phone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El correo es obligatorio")]
        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Dirección de correo no válida")]
        [StringLength(128, ErrorMessage = "Máximo {1} caracteres.")]
        public string Mail { get; set; }

        [Display(Name = "Dirección")]
        [StringLength(128, ErrorMessage = "Máximo {1} caracteres.")]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de nacimiento es obligatoria")]
        [Display(Name = "Fecha Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]
        [CustomValidation(typeof(ValidacionFechaBirthDay), "IsValid",ErrorMessage ="Error en fecha")]
        public DateTime BirthDay { get; set; }

        // Variable utilizada para valiar el rol para usuarios
        [Display(Name = "RolId")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El rol es obligatorio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string RolId { get; set; }
    }

    public class RegisterViewModelUpdate
    {
        public string Id { get; set; }

        [Required]
        [Display(Name = "Usuario", Prompt = "Usuario")]
        public string UserName { get; set; }

        [StringLength(32, ErrorMessage = "La {0} debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña", Prompt = "Contraseña")]
        public string Password { get; set; }

        [StringLength(32, ErrorMessage = "La {0} debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña", Prompt = "Nueva contraseña")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña", Prompt = "Confirmar Contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Las claves no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [Display(Name = "Nombres", Prompt = "Nombres")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido para nombres")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El apellido es obligatorio")]
        [Display(Name = "Apellidos", Prompt = "Apellidos")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido para apellidos")]
        [StringLength(50, ErrorMessage = "Máximo {1} caracteres")]
        public string SureName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El número del documento es obligatorio")]
        [Display(Name = "Documento", Prompt = "Documento")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido en documento")]
        [StringLength(16, ErrorMessage = "Máximo {1} caracteres.")]
        public string PersonalID { get; set; }

        [Display(Name = "Género")]
        public int Genre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [Display(Name = "Teléfono")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Número de teléfono no válido. Solo debes ingresar números sin puntos ni comas.")]
        [StringLength(16, ErrorMessage = "Máximo {1} caracteres.")]
        public string Phone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El correo es obligatorio")]
        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Dirección de correo no válida")]
        [StringLength(128, ErrorMessage = "Máximo {1} caracteres.")]
        public string Mail { get; set; }

        [Display(Name = "Dirección")]
        [StringLength(128, ErrorMessage = "Máximo {1} caracteres.")]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de nacimiento es obligatoria")]
        [Display(Name = "Fecha Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}")]
        //[CustomValidation(typeof(ValidacionFechaBirthDay), "IsValid", ErrorMessage = "Error en fecha")]
        public DateTime BirthDay { get; set; }

        // Variable utilizada para validar el rol para usuarios
        [Display(Name = "RolId")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El rol es obligatorio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string RolId { get; set; }
    }


    public class ResetPasswordViewModel
    {
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El documento es obligatorio")]
        //[Display(Name = "Documento", Prompt = "Documento")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caractér no valido en documento")]
        //public string PersonalID { get; set; }

        [Required]
        [StringLength(32, ErrorMessage = "La {0} debe contener entre {2} y {1} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "La contraseña no coincide.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Code { get; set; }
    }
    public class ForgotPasswordViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de identificación es obligatorio")]
        [Display(Name = "Documento", Prompt = "Documento")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caracter no válido en el número de identificación")]
        public string PersonalID { get; set; }
    }

    public static class ValidacionFechaBirthDay
    {
        public static ValidationResult IsValid(DateTime value)
        {

            //--03/09/2015<=  02/09/2010 && 03/09/2015>=  02/09/1910 No nace  false
            //--02/09/2013<=  02/09/2010 && 02/09/2013>=  02/09/1910 2años    false
            //--12/05/1990<=  02/09/2010 && 12/05/1990>=  02/09/1910 25 años  true
            //--01/01/1890<=  02/09/2010 && 01/01/1890>=  02/09/1910 125 años false

            if (value <= DateTime.Now.AddYears(-5) && value >= DateTime.Now.AddYears(-100))
            {
                return ValidationResult.Success;
            }



            return new ValidationResult("La fecha debe estar relacionada con una edad comprendida entre 5 y 100 años a la fecha actual");
        }
    }

}
