﻿using INI.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INI.Models.BusquedaInvestigacion
{
    public class Mis_Proyectos
    {
        public SearchProyect busqueda_docentes { get; set; }

        public List<tblProyectosInvestigacion> proyectos_docentes { get; set; }

    }
}