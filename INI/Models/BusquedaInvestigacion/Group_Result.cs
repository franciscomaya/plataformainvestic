﻿using INI.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INI.Models.BusquedaInvestigacion
{
    public class Group_Result
    {
        public List<MisGrupos> Mis_Grupos { get; set; }
        public SearchProyect Search_Proyect { get; set; }
    }
}