﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace INI.Models.Mymodels
{
    public class NavegationModel
    {
        public DateTime ImputDateUrl { get; set; }
        public String Url { get; set; }
    }

   public class NavigationLog
    {
        
        public int id { get; set; }

        public int reporttype { get; set; }

        public string Rol { get; set; }

        
        public string Usuario { get; set; }

        //public string Nombre { get; set; }

        public string IP { get; set; }

        
        public string Latitud { get; set; }

        
        public string Longitud { get; set; }

        
        public string Altitud { get; set; }
        
        public Nullable<System.DateTime> FechaInicioSesion { get; set; }
        
        public Nullable<System.DateTime> FechaCierreSesion { get; set; }

        public List<NavegationModel> navigations { get; set; }
    }
}