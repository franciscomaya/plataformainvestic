﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace INI.Models
{
    public partial class AspNetRoles_Metadata
    {


        [Display(Name = "Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Id")]
        //[StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string Id { get; set; }

        [Display(Name = "Nombre")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        //[StringLength(256, ErrorMessage = "Máximo 256 caracteres")]

        public string Name { get; set; }

    }

    public partial class AspNetUserClaims_Metadata
    {
        [Display(Name = "Id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }

        [Display(Name = "UserId")]
        [Required]
        public string UserId { get; set; }

        [Display(Name = "ClaimType")]
        public string ClaimType { get; set; }

        [Display(Name = "ClaimValue")]
        public string ClaimValue { get; set; }
        
    }

    public partial class AspNetUserLogins_Metadata
    {
        [Display(Name = "LoginProvider")]
        [Required]
        public string LoginProvider { get; set; }

        [Display(Name = "ProviderKey")]
        [Required]
        public string ProviderKey { get; set; }

        [Display(Name = "UserId")]
        [Required]
        public string UserId { get; set; }        
    }

    public partial class AspNetUserRoles_Metadata
    {
        [Display(Name = "Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Id")]
        //[StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }

        [Display(Name = "User Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El User Id es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para User Id")]
        //[StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string UserId { get; set; }

        [Display(Name = "Role Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Role Id es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Role Id")]
        //[StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string RoleId { get; set; }

    }

    public partial class AspNetUsers_Metadata
    {

        [Display(Name = "Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Id")]
        //[StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string Id { get; set; }

        [Display(Name = "Nombre")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Name { get; set; }

        [Display(Name = "Apellido")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Apellido es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Apellido")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string SureName { get; set; }

        [Display(Name = "Personal ID")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Apellido")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string PersonalID { get; set; }

        [Display(Name = "Género")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Género es obligatorio")]
        //[Range(0,int.MaxValue)]
        public int Genre { get; set; }

        [Display(Name = "Dirección")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "La Dirección es obligatoria")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ#-]+$", ErrorMessage = "Carácter no valido para Dirección")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Address { get; set; }

        [Display(Name = "Fecha Nacimiento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha Nacimiento es obligatoria")]
        //[StringLength(10, ErrorMessage = "Máximo 10 caracteres")]
        public System.DateTime BirthDay { get; set; }

        [Display(Name = "Email")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Email es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Email")]
        //[StringLength(256, ErrorMessage = "Máximo 256 caracteres")]
        public string Email { get; set; }

        [Display(Name = "Email Confirmado")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Email Confirmado es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Email Confirmado")]
        public bool EmailConfirmed { get; set; }

        [Display(Name = "Contraseña Hash")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Contraseña Hash")]
        public string PasswordHash { get; set; }

        [Display(Name = "Sello de Seguridad")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Sello de Seguridad")]
        public string SecurityStamp { get; set; }

        [Display(Name = "Número de Teléfono")]
        //[RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Número de Teléfono")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Confirmar Número de teléfono")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Número de teléfono Confirmar es obligatorio")]
        //[RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Email Confirmado")]
        public bool PhoneNumberConfirmed { get; set; }

        [Display(Name = "Dos factores Activado")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Dos factores Activado es obligatorio")]
        public bool TwoFactorEnabled { get; set; }

        [Display(Name = "Fin Fecha Bloqueo Utc")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }

        [Display(Name = "Bloqueo Habilitado")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Bloqueo Habilitado es obligatorio")]
        public bool LockoutEnabled { get; set; }

        [Display(Name = "Acceso fallido Error")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Acceso es obligatorio")]
        //[Range(0,int.MaxValue)]
        public int AccessFailedCount { get; set; }

        [Display(Name = "Nombre de Usuario")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre de Usuario es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre de Usuario")]
        //[StringLength(256, ErrorMessage = "Máximo 256 caracteres")]
        public string UserName { get; set; }

        [Display(Name = "Apellido")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string LastName { get; set; }

        [Display(Name = "Celular")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Celular { get; set; }

        [Display(Name = "Cargo")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Cargo { get; set; }

        [Display(Name = "Contrato")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Contrato { get; set; }

        [Display(Name = "Cdp")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Cdp { get; set; }

        [Display(Name = "Equipo")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Equipo { get; set; }

        [Display(Name = "Fecha Inicio Contrato")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha_IniContrato { get; set; }

        [Display(Name = "Fecha Fin Contrato")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha_FinContrato { get; set; }

        [Display(Name = "Cedula")]
        //[RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Cedula")]
        //[StringLength(10, ErrorMessage = "Máximo 10 caracteres")]
        public string Cedula { get; set; }

        [Display(Name = "Nombres")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombres")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Nombres { get; set; }

        [Display(Name = "Apellidos")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Apellidos")]
        //[StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Apellidos { get; set; }

        [Display(Name = "TipoDoc")]
        //[Range(0,int.MaxValue)]
        public Nullable<int> TipoDoc { get; set; }
    }

    public partial class CategoriaRepositorio_Metadata
    {

        public int id { get; set; }

        [Display(Name = "nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string nombre { get; set; }

        [Display(Name = "Url img")]

        public string urlimg { get; set; }


    }

    public partial class CategoriaRevista_Metadata
    {

        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Id")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string nombre { get; set; }

        [Display(Name = "Url img")]
        public string urlimg { get; set; }
    }

    public partial class Repositorio_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "El título es requerido")]
        public string title { get; set; }

        [Display(Name = "URL del Repositorio")]
        [Required(ErrorMessage = "La url del repositorio es requerida")]
        [DataType(DataType.Url)]
        [Url(ErrorMessage = "La url tiene un formato incorrecto")]
        public string urlRepositorio { get; set; }

        [Display(Name = "SubCategoria")]
        [Required(ErrorMessage = "La subcategoría es requerida")]
        [Range(0, int.MaxValue)]
        public Nullable<int> id_SubCategoria { get; set; }

        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Display(Name = "Portada")]
        [Required]
        public string urlfront { get; set; }
        
    }

    public partial class Revista_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Id")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Título")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para título")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string title { get; set; }

        [Display(Name = "Url Pdf")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo PDF es obligatorio")]
        public string urlPdf { get; set; }

        [Display(Name = "Sub Categoria Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> id_SubCategoria { get; set; }

        [Display(Name = "Descripción")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para descripción")]
        public string Description { get; set; }

        [Display(Name = "Url Front")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo de portada es obligatorio")]
        public string urlfront { get; set; }


    }

    public partial class SubCategoriaRepositorio_Metadata
    {

        public int id { get; set; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string nombre { get; set; }

        [Display(Name = "Url img")]
        public string urlimg { get; set; }

        [Display(Name = "Categoria Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> id_categoria { get; set; }

    }

    public partial class SubCategoriaRevista_Metadata
    {


        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Id")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string nombre { get; set; }

        [Display(Name = "Url img")]
        public string urlimg { get; set; }

        [Display(Name = "Categoria Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> id_categoria { get; set; }

    }

    public partial class tblActividadContratista_Metadata
    {
        [Display(Name = "Actividad Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Id")]
        [Range(0, int.MaxValue)]
        public int Id_Actividad { get; set; }

        [Display(Name = "Fecha Inicio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha Inicio es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime Fecha_Ini { get; set; }

        [Display(Name = "Fecha Fin")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha Fin es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime Fecha_Fin { get; set; }

        [Display(Name = "Contratista Id")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Contratista Id es obligatorio")]
        public string Id_Contratista { get; set; }

        [Display(Name = "Alternativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Id")]
        [Range(0, int.MaxValue)]
        public int Id_Alternativa { get; set; }

        [Display(Name = "Des_Resp_Contratista")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Des Resp Contratista es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no valido para Des Resp Contratista")]
        [Range(0, int.MaxValue)]
        public int Des_Resp_Contratista { get; set; }

        [Display(Name = "Des Actividad")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Des Actividad es obligatorio")]
        public string Des_Actividad { get; set; }

        [Display(Name = "Estado Id")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no válido para Estado Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int Id_Estado { get; set; }

        [Display(Name = "Des Observaciones")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Des_Observaciones { get; set; }

        [Display(Name = "Periodo_Reporte")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Carácter no válido para periodo reporte")]
        [Range(0, int.MaxValue)]
        public Nullable<int> Periodo_Reporte { get; set; }
    }

    public partial class tblAlternativas_Metadata
    {

        [Display(Name = "Alternativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int Id_Alternativa { get; set; }

        [Display(Name = "Des Alternativa")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Des Alternativa es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Des Alternativa")]
        public string Des_Alternativa { get; set; }


    }

    public partial class tblAreaConocimiento_Metadata
    {

        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Nombre { get; set; }

    }

    public partial class tblAsesor_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "User Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El User Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUser { get; set; }

        [Display(Name = "Está Activo")]
        public Nullable<bool> estaActivo { get; set; }


    }

    public partial class tblAsesorGrupoInvestigacion_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Proyecto Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idProyectoInvestigacion { get; set; }

        [Display(Name = "Asesor Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idAsesor { get; set; }

    }

    public partial class tblAsesorZona_Metadata
    {
        [Display(Name = "Asesor Zona ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Asesor Zona ID es obligatorio")]
        public long tblAsesorZona_ID { get; set; }

        [Display(Name = "Nombre Asesor Zona")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del asesor de zona es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Nombre Asesor Zona")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string aseZon_nombre { get; set; }

        [Display(Name = "Apellido Asesor Zona")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El apellido del asesor de zona es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Apellido Asesor Zona")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string aseZon_apellido { get; set; }

        [Display(Name = "Correo Asesor Zona")]

        [Required(AllowEmptyStrings = false, ErrorMessage = "El correo del asesor de zona es obligatorio")]
        [EmailAddress(ErrorMessage = "El formato de correo electrónico no es válido")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string aseZon_correo { get; set; }

        [Display(Name = "Teléfono Asesor Zona")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Número de teléfono no válido. Solo debes ingresar números sin puntos ni comas, con un máximo de 15 dígitos")]
        [StringLength(15, ErrorMessage = "Máximo 15 dígitos")]
        public string aseZon_telefono { get; set; }

        [Display(Name = "Equipo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Equipo Id es obligatorio")]
        public long tblEquipo_ID { get; set; }

        [Display(Name = "Municipio ID")]        
        [StringLength(5, ErrorMessage = "Máximo 5 caracteres")]
        public string tblMunicipio_ID { get; set; }

        [Display(Name = "Está activo")]
        public Nullable<bool> estaActivo { get; set; }

    }

    public partial class tblAsesorZonaMunicipio_Metadata
    {
        [Display(Name = "Asesor Zona Municipio Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Asesor Zona Municipio Id es obligatorio")]
        public long tblAsMun_ID { get; set; }

        [Display(Name = "Asesor Zona Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Asesor Zona Id es obligatorio")]
        public long tblAsesorZona_ID { get; set; }

        [Display(Name = "Municipio Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Municipio Id es obligatorio")]
        public long tblMunicipio_ID { get; set; }


    }

    public partial class tblBitacoraGrupoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Título")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el título")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Título es obligatorio")]
        public string Titulo_1 { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para nombre")]

        public string Nombre_1 { get; set; }

        [Display(Name = "Maestro")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para maestro")]
        public string Maestro_1 { get; set; }

        [Display(Name = "Introducción")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Introduccion_1 { get; set; }

        [Display(Name = "Resumen Final")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ResumenFinal_1 { get; set; }

        [Display(Name = "Insignia Grupo URL")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string InsigniaGrupoURL_2 { get; set; }

        [Display(Name = "Pregunta Investigación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string PreguntaInvestigacion_2 { get; set; }

        [Display(Name = "Descripción Problema")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string DescripcionProblema_2 { get; set; }

        [Display(Name = "Objetivos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Objetivos_2 { get; set; }

        [Display(Name = "Actividades Realizadas")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ActividadesRealizadas_2 { get; set; }

        [Display(Name = "Conceptos Principales")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ConceptosPrincipales_3 { get; set; }

        [Display(Name = "Resultados")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Resultalos_3 { get; set; }

        [Display(Name = "Participación 3")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Participacion_3 { get; set; }

        [Display(Name = "Conclusiones 4")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Concluciones_4 { get; set; }

        [Display(Name = "Bibliografía 5")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Bibliografia_5 { get; set; }

        [Display(Name = "Ponencia 6")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Ponencia_6 { get; set; }


    }

    public partial class tblCaracteristicasProy_Metadata
    {

        [Display(Name = "Características Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Características Proyecto Id es obligatoria")]
        public long tblCaracteristicasProy_ID { get; set; }

        [Display(Name = "Características Proyecto Resultados Esperados")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string carProy_resultadosEsperadosProy { get; set; }

        [Display(Name = "Características proyecto caracterización")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string carProy_caracterizacionProy { get; set; }


    }

    public partial class tblCaracteristicasProy_Rev_Metadata
    {

        [Display(Name = "Características proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Características proyecto Rev Id es obligatoria")]
        public long tblCaracteristicasProy_Rev_ID { get; set; }

        [Display(Name = "Características proyecto resultados Esperados")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string carProy_resultadosEsperadosProy { get; set; }

        [Display(Name = "Características proyecto caracterización")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string carProy_caracterizacionProy { get; set; }

    }

    public partial class tblCargos_Metadata
    {
        [Display(Name = "Cargo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cargo Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int Id_Cargo { get; set; }

        [Display(Name = "Nombre Cargo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del cargo es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el nombre del cargo")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Nombre_Cargo { get; set; }

    }

    public partial class tblCategoriaImagenes_Metadata
    {
        [Display(Name = "Categoría Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Categoría Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int cat_ID { get; set; }

        [Display(Name = "Nombre Categoría")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre de la categoría es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el nombre de la categoría")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]

        public string cat_Nombre { get; set; }

        [Display(Name = "Categoría Imágenes Portada")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La imagen es obligatoria")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para la imágenes portada")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string cat_Imagen_Portada { get; set; }

    }

    public partial class tblCategoriaProductos_Metadata
    {
        [Display(Name = "Categoría Productos Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Categoría Productos Id es obligatoria")]
        public long tblCategoriaProductos_ID { get; set; }

        [Display(Name = "Nombre  Categoría Productos")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre  Categoría Productos es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Nombre Categoría Productos")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string catpro_nombre { get; set; }

    }

    public partial class tblCentrosPoblado_Metadata
    {
        [Display(Name = "Código Departamento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Código Departamento es obligatorio")]
        [StringLength(2, ErrorMessage = "Máximo 2 caracteres")]
        public string CodigoDepartamento { get; set; }

        [Display(Name = "Código Municipio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Código Municipio es obligatorio")]
        [StringLength(5, ErrorMessage = "Máximo 5 caracteres")]
        public string CodigoMunicipio { get; set; }

        [Display(Name = "Código Centro Poblado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Código Centro Poblado es obligatorio")]
        [StringLength(8, ErrorMessage = "Máximo 8 caracteres")]
        public string CodigoCentroPoblado { get; set; }

        [Display(Name = "Nombre Departamento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Departamento es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para nombre del Departamento")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string NombreDepartamento { get; set; }

        [Display(Name = "Nombre Municipio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Municipio es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el nombre del Municipio")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string NombreMunicipio { get; set; }

        [Display(Name = "Nombre Centro Poblado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Centro Poblado es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Nombre Centro Poblado")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string NombreCentroPoblado { get; set; }

        [Display(Name = "Tipo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El tipo es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para tipo")]
        [StringLength(5, ErrorMessage = "Máximo 5 caracteres")]

        public string Tipo { get; set; }

    }

    public partial class tblCoInvestigadorGrupoInvestigacion_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Usuario")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUsuario { get; set; }

        [Display(Name = "Grupo Investigación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }
    }

    public partial class tblColecciones_Metadata
    {

        [Display(Name = "Colección Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Colección Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id_coleccion { get; set; }

        [Display(Name = "Colección Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Colección Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Colección Nombre")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]

        public string nom_coleccion { get; set; }

        [Display(Name = "Colección Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Colección Descripción es obligatoria")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Colección Descripción")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string desc_coleccion { get; set; }

    }

    public partial class tblComentarioGrupo_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupo { get; set; }

        [Display(Name = "User Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El User Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string UserId { get; set; }

        [Display(Name = "Comentario")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El comentario es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para comentario")]
        [StringLength(256, ErrorMessage = "Máximo 256 caracteres")]

        public string Comentario { get; set; }

        [Display(Name = "Aprobado")]
        public Nullable<bool> Aprobado { get; set; }

    }

    public partial class tblConceptosEstadoArte_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Estado Arte Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Arte Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idEstadoArte { get; set; }

        [Display(Name = "Autor")]

        [Required(AllowEmptyStrings = false, ErrorMessage = "El autor es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Autor")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string Autor { get; set; }

        [Display(Name = "año")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El año es obligatorio")]
        [Range(1450, 2100, ErrorMessage = "El año debe ser superior a {1} y menor de 2100")]
        public int Publicacion { get; set; }

        [Display(Name = "Texto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Texto { get; set; }

    }

    public partial class tblConfiguration_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "DateOperation")]
        [Required]
        public System.DateTime DateOperation { get; set; }

        [Display(Name = "operation")]
        [Required]
        [Range(0, int.MaxValue)]
        public int operation { get; set; }

        [Display(Name = "IsEnabled")]
        [Required]
        public bool IsEnabled { get; set; }

        [Display(Name = "element")]
        [Required]
        [Range(0, int.MaxValue)]
        public int element { get; set; }

    }

    public partial class tblCriticoSocial_Metadata
    {
        [Display(Name = "Critico Social Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Critico Social Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblCriticoSocial_ID { get; set; }

        [Display(Name = "Critico Social Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Critico Social Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Critico Social Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string critSoc_nombre { get; set; }
    }

    public partial class tblCronogramaProy_Metadata
    {
        [Display(Name = "Cronograma Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Proyecto Id es obligatorio")]
        public long tblCronogramaProy_ID { get; set; }

    }

    public partial class tblCronogramaProy_Rev_Metadata
    {
        [Display(Name = "Cronograma Proyecto Revisión Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Proyecto Revisión Id es obligatorio")]
        public long tblCronogramaProy_Rev_ID { get; set; }

        [Display(Name = "Cronograma Proyecto Revision")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string croProy_revision { get; set; }

    }

    public partial class tblDiseniosProy_Metadata
    {
        [Display(Name = "Diseños del proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Diseños del proyecto Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblDiseniosProy_ID { get; set; }

        [Display(Name = "Nombre diseños del proyecto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del diseño del proyecto es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para el nombre del diseño del proyecto")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string disProy_nombre { get; set; }

        public Nullable<int> tblTipoEstudio_ID { get; set; }

    }

    public partial class tblDocumentosSoporte_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Carta Compromiso")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string CartaCompromiso { get; set; }

        [Display(Name = "Carta Aceptación")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string CartaAceptacion { get; set; }

        [Display(Name = "Grupo Investigación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

    }

    public partial class tblEjeInvestigacion_Metadata
    {
        
        [Display(Name = "tblEjeInvestigacion_ID")]
        [Required]
        [Range(0, int.MaxValue)]
        public int tblEjeInvestigacion_ID { get; set; }

        [Display(Name = "Nombre eje de investigación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del eje de investigación es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para el nombre del eje de investigación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ejeInv_nombre { get; set; }

                
    }

    public partial class tblEquipo_Metadata
    {
        [Display(Name = "Equipo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Equipo Id es obligatorio")]
        public long tblEquipo_ID { get; set; }

        [Display(Name = "Equipo Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Equipo Descripción es obligatorio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string equ_descripcion { get; set; }

        [Display(Name = "Equipo Observación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Equipo Observación es obligatorio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string equ_Observacion { get; set; }

    }

    public partial class tblEquiposTrabajo_Metadata
    {
        [Display(Name = "Equipo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Equipo Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int Id_Equipo { get; set; }

        [Display(Name = "Nombre Equipo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Equipo es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Nombre Equipo")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string Nombre_Equipo { get; set; }

        [Display(Name = "Coordinador Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Coordinador Id es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Coordinador")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string Id_Coordinador { get; set; }

        [Display(Name = "Nombre Coordinador")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del coordinador es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Nombre Coordinador")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Nombre_Coordinador { get; set; }
    }

    public partial class tblEstado_Metadata
    {
        [Display(Name = "Estado Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Id es obligatorio")]
        public long tblEstado_ID { get; set; }

        [Display(Name = "Estado Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Estado Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string est_nombre { get; set; }

    }

    public partial class tblEstadoArteProyectoInvestigacion_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Tema Investigación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El tema de investigación es obligatorio")]
        [StringLength(256, ErrorMessage = "Máximo 256 caracteres")]
        public string TemaInvestigacion { get; set; }

        [Display(Name = "Mapa Conceptual")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string MapaConceptual { get; set; }

    }

    public partial class tblEstadoTarea_Metadata
    {
        [Display(Name = "Estado Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int Id_Estado { get; set; }

        [Display(Name = "Descripción Estado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Descripción Estado es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Descripción Estado")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Des_Estado { get; set; }

    }

    public partial class tblEventosAcademicos_Metadata
    {
        [Display(Name = "Eventos Académicos Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Eventos Académicos Id es obligatorio")]
        public long tblEventosAcademicos_ID { get; set; }

        [Display(Name = "Eventos Académicos Tìtulo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título del evento académico es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el título")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string eveaca_tituloEvento { get; set; }

        [Display(Name = "Eventos Académicos Evento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El evento académico es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el evento académico")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string eveaca_evento { get; set; }

        [Display(Name = "Eventos Académicos Lugar")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El lugar del evento académico es obligatorio")]        
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string eveaca_lugarEvento { get; set; }

        [Display(Name = "Eventos Académicos Ano Terminación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "LA fecha de terminación es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> eveaca_anoTerminacion { get; set; }

        [Display(Name = "Hoja Vida Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Hoja Vida Id es obligatoria")]
        public long tblHojaVida_ID { get; set; }

    }

    public partial class tblEvidenciasContratista_Metadata
    {
        [Display(Name = "Evidencias Contratista Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Evidencias Contratista Id es obligatoria")]
        [Range(0, int.MaxValue)]
        public int Id_Evidencia { get; set; }

        [Display(Name = "Contratista Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Contratista Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string Id_Contratista { get; set; }

        [Display(Name = "Actividad Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Actividad Id es obligatoria")]
        [Range(0, int.MaxValue)]
        public int Id_Actividad { get; set; }

        [Display(Name = "Nombre Evidencia")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Evidencia es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Nombre Evidencia")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string Nombre_Evidencia { get; set; }

        [Display(Name = "Descripción Evidencia")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Descripción Evidencia es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Descripción Evidencia")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion_Evidencia { get; set; }


    }

    public partial class tblExperienciaProyectos_Metadata
    {
        [Display(Name = "Experiencia Proyectos Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Experiencia Proyectos Id es obligatoria")]
        public long tblExperienciaProyectos_ID { get; set; }

        [Display(Name = "Experiencia Proyectos Título")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título del proyecto es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el título del proyecto")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string exppro_tituloProyecto { get; set; }

        [Display(Name = "Experiencia Proyectos Ano Terminación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de terminación del proyecto es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> exppro_anoTerminacion { get; set; }

        [Display(Name = "Hoja Vida ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Hoja Vida ID es obligatoria")]
        public long tblHojaVida_ID { get; set; }

    }

    public partial class tblFechaCronograma_Metadata
    {
        [Display(Name = "Fecha Cronograma Id")]
        [Required]
        public long tblFechaCronograma_ID { get; set; }

        [Display(Name = "Cronograma Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Proyecto Id es obligatorio")]
        public long tblCronogramaProy_ID { get; set; }

        [Display(Name = "Cronograma Actividad")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La actividad es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string cro_Actividad { get; set; }

        [Display(Name = "Cronograma Fecha Inicio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Fecha Inicio es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime cro_FechaInicio { get; set; }

        [Display(Name = "Cronograma Fecha Fin")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Fecha Fin es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime cro_FechaFin { get; set; }

        [Display(Name = "Cronograma Indicador")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El indicador es obligatorio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string cro_Indicador { get; set; }

    }

    public partial class tblForoProyectoInvestigacion_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Usuario Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUser { get; set; }

        [Display(Name = "Grupo Investigación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Título")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para titulo")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Titulo { get; set; }

        [Display(Name = "Mensaje")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El mensaje es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para mensaje")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Mensaje { get; set; }

        [Display(Name = "Fecha")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime Fecha { get; set; }

        [Display(Name = "Respuestas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La respuesta es obligatoria")]
        [Range(0, int.MaxValue)]
        public int Respuestas { get; set; }

        [Display(Name = "Foro Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> idForo { get; set; }

        [Display(Name = "Fecha Última Respuesta")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FechaUltimaRespuesta { get; set; }

    }

    public partial class tblGaleriaImagenes_Metadata
    {
        [Display(Name = "Imagen Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Imagen Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int Imagen_ID { get; set; }

        [Display(Name = "Imagen Título")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para el título")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Imagen_titulo { get; set; }

        [Display(Name = "Imagen Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para la descripción")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]
        public string Imagen_descripcion { get; set; }

        [Display(Name = "Imagen Url")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La imagen es obligatoria")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]
        public string Imagen_url { get; set; }

        [Display(Name = "Categoría Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Categoría Id es obligatoria")]
        [Range(0, int.MaxValue)]
        public int cat_ID { get; set; }

    }

    public partial class tblGrupoInvestigacion_Metadata
    {

        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Código")]
        [StringLength(10, ErrorMessage = "Máximo 10 caracteres")]
        public string Codigo { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no valido para el nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Nombre { get; set; }

        [Display(Name = "Fecha Creación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de creación es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime FechaCreacion { get; set; }

        [Display(Name = "Tipo Grupo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El tipo del grupo es obligatorio")]
        [Range(0, int.MaxValue)]
        public int TipoGrupo { get; set; }

        [Display(Name = "Avatar")]
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Solo se admiten imagenes .jpg, .png o .gif")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Avatar { get; set; }

        [Display(Name = "Institución Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La institución es obligatoria")]
        [Range(0, int.MaxValue)]
        public int idInstitucion { get; set; }

        [Display(Name = "Línea Investigación ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La línea de investigación es obligatoria")]
        [Range(0, int.MaxValue)]
        public int idLineaInvestigacion { get; set; }

        [Display(Name = "Usuario Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El usuario es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUsuario { get; set; }

    }

    public partial class tblGruposInvestigacion_Metadata
    {

        [Display(Name = "Grupos Investigación Id")]
        [Required]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Grupos Investigación Id es obligatorio")]
        public long tblGruposInvestigacion_ID { get; set; }

        [Display(Name = "Usuario Plataforma Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario Plataforma Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string tblUsuarioPlataforma_ID { get; set; }

        [Display(Name = "Grupos Investigación Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupos Investigación Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido Grupos Investigación Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string gruInv_nombreGrupo { get; set; }

        [Display(Name = "Grupos Investigación Emblema")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string gruInv_emblema { get; set; }

        [Display(Name = "Estado Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Id es obligatorio")]
        public long tblEstado_ID { get; set; }

        [Display(Name = "Grupos Investigación Fecha Creación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Fecha Creación es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime gruInv_fechaCreacion { get; set; }

        [Display(Name = "Grupos Investigación Proyectos")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupos Investigación Proyectos es obligatorio")]
        [Range(0, int.MaxValue)]
        public int gruInv_proyectos { get; set; }

    }

    public partial class tblHerramientasRecoleccionInformacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Herramienta Recolección")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La herramienta de recolección es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no valido para la herramienta recolección")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string HerramientaRecoleccion { get; set; }

        [Display(Name = "Descripción")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para la descripción")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion { get; set; }

    }

    public partial class tblHistoricoHermeneutico_Metadata
    {
        [Display(Name = "Histórico Hermenéutico Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Histórico Hermenéutico Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblHistoricoHermeneutico_ID { get; set; }

        [Display(Name = "Histórico Hermenéutico Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Histórico Hermenéutico Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Histórico Hermenéutico Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string hisHerm_nombre { get; set; }

    }

    public partial class tblHojaVida_Metadata
    {
        [Display(Name = "Hoja Vida Id")]        
        public long tblHojaVida_ID { get; set; }

        [Display(Name = "Hoja Vida Ano Grado Secundaria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> hojVid_anoGradoSecundaria { get; set; }

        [Display(Name = "Hoja Vida Título Secundaria")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "La Hoja Vida Título Secundariaes obligatorio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título de secundaria es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para título de educación secundaria")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string hojVid_tituloSecundaria { get; set; }

        [Display(Name = "Usuario Plataforma Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El usuario plataforma Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string tblUsuarioPlataforma_ID { get; set; }

        [Display(Name = "Hoja Vida Institución Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> hojVid_Institucion_ID { get; set; }
    }

    public partial class tblIdiomas_Metadata
    {
        [Display(Name = "Idiomas Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El id idioma es obligatorio")]
        [Range(0, int.MaxValue)]
        public long tblIdiomas_ID { get; set; }

        [Display(Name = "Idiomas Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del idioma es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para el nombre del idioma")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string idi_nombre { get; set; }

        [Display(Name = "Idiomas Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción del idioma es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para la descripción del Idioma")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string idi_descripcion { get; set; }
    }

    public partial class tblInstitucion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Código Dane")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Código Dane es obligatorio")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string CodigoDane { get; set; }

        [Display(Name = "Consecutivo Sede")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Consecutivo Sede es obligatorio")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string ConsecutivoSede { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres")]

        public string Nombre { get; set; }

        [Display(Name = "Nombre Sede")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre Sede")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres")]
        public string NombreSede { get; set; }

        [Display(Name = "Tipo Institución Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Tipo Institución Id es obligatorio")]
        public long idTipoInstitucion { get; set; }

        [Display(Name = "Municipio Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Municipio Id es obligatorio")]
        [StringLength(5, ErrorMessage = "Máximo 5 caracteres")]
        public string idMunicipio { get; set; }

        [Display(Name = "Zona Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> idZona { get; set; }

        [Display(Name = "Dirección")]
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string Direccion { get; set; }

        [Display(Name = "Latitud")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Latitud { get; set; }

        [Display(Name = "Longitud")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Longitud { get; set; }

        [Display(Name = "Teléfono")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Formato de teléfono no valido")]

        public string Telefono { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El correo es obligatorio")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Dirección de mail Inválida")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string CorreoElectronico { get; set; }

        [Display(Name = "Nombre Director")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Director es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre Director")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string NombreDirector { get; set; }

        [Display(Name = "Teléfono Director")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Formato de teléfono no valido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string TelefonoDirector { get; set; }

        [Display(Name = "Correo Director")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El correo es obligatorio")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Dirección de mail Inválida")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string CorreoDirector { get; set; }

        [Display(Name = "Sitio Web")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string SitioWeb { get; set; }
    }

    public partial class tblInstitucionEducativa_Metadata
    {
        [Display(Name = "Institución Educativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblInstitucionEducativa_ID { get; set; }

        [Display(Name = "Institución Educativa Código Dane")]
        [Range(0, int.MaxValue)]
        public Nullable<int> insEdu_codigoDane { get; set; }

        [Display(Name = "Institución Educativa Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Institución Educativa Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Institución Educativa Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string insEdu_nombre { get; set; }

        [Display(Name = "Institución Educativa Dirección")]
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string insEdu_direccion { get; set; }

        [Display(Name = "Institución Educativa Teléfono")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Formato de teléfono no valido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string insEdu_telefono { get; set; }

        [Display(Name = "Institución Educativa Nombre Sede")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Institución Educativa Nombre Sede es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Institución Educativa Nombre Sede")]
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]

        public string insEdu_nombresede { get; set; }

        [Display(Name = "Tipo Establecimiento Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Tipo Establecimiento Id es obligatorio")]
        public long tblTipoEstablecimiento_ID { get; set; }

        [Display(Name = "Municipio Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Municipio Id es obligatorio")]
        public long tblMunicipio_ID { get; set; }
    }

    public partial class tblIntegrante_Metadata
    {
        [Display(Name = "Integrante Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        public long tblIntegrante_ID { get; set; }

        [Display(Name = "Integrante Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Integrante Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Integrante Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string int_nombre { get; set; }

        [Display(Name = "Integrante Apellido")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Integrante Apellido es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Integrante Apellido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string int_apellido { get; set; }

        [Display(Name = "Integrante Correo")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Dirección de mail Inválida")]
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string int_correo { get; set; }

        [Display(Name = "Integrante Teléfono")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Formato de teléfono no valido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string int_telefono { get; set; }

        [Display(Name = "Integrante Cargo")]
        [StringLength(200, ErrorMessage = "Máximo 200 caracteres")]
        public string int_cargo { get; set; }

        [Display(Name = "Equipo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Equipo Id es obligatorio")]
        public long tblEquipo_ID { get; set; }

    }

    public partial class tblIntegrantesGrupoInv_Metadata
    {
        [Display(Name = "Integrantes Grupo Inv Id")]        
        public long tblIntegrantesGrupoInv_ID { get; set; }

        [Display(Name = "Grupos Investigación Id")]        
        public long tblGruposInvestigacion_ID { get; set; }

        [Display(Name = "Usuario Plataforma Id")]    
        public string tblUsuarioPlataforma_ID { get; set; }

        [Display(Name = "Integrantes Grupo Inv Fecha Vinculación")]        
        public System.DateTime intGruInv_fechaVinculacion { get; set; }

    }

    public partial class tblInvitacionGrupo_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Usuario Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario Id es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Usuario Id")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUsuario { get; set; }

        [Display(Name = "Grupo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupo { get; set; }

        [Display(Name = "Rol Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Rol Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idRol { get; set; }

        [Display(Name = "Aceptada")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Aceptado es obligatorio")]
        public bool Aceptada { get; set; }
    }

    public partial class tblLecturaAnalisisFuentesinformacionPoryectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Lectura")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Lectura es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Lectura")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string Lectura { get; set; }

        [Display(Name = "Análisis")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Analisis { get; set; }

    }

    public partial class tblLenguas_Metadata
    {
        [Display(Name = "Lenguas Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Lenguas Id es obligatoria")]
        public long tblLenguas_ID { get; set; }

        [Display(Name = "Lenguas Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Lenguas Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Lenguas Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string len_nombre { get; set; }

        [Display(Name = "Lenguas Descripción")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Lenguas Descripción")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string len_descripcion { get; set; }
    }

    public partial class tblLineaInvestigacion_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Nombre { get; set; }

        [Display(Name = "Categoria")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Categoria es obligatoria")]
        [Range(0, int.MaxValue)]
        public int Categoria { get; set; }
    }

    public partial class tblLogAcceso_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Rol")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Rol es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Rol")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Rol { get; set; }

        [Display(Name = "Usuario")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ.]+$", ErrorMessage = "Carácter no valido para Usuario")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Usuario { get; set; }

        [Display(Name = "IP")]
        [StringLength(20, ErrorMessage = "Máximo 20 caracteres")]
        public string IP { get; set; }

        [Display(Name = "Latitud")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Latitud { get; set; }

        [Display(Name = "Longitud")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Longitud { get; set; }

        [Display(Name = "Altitud")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Altitud { get; set; }

        [Display(Name = "Navegación")]
        public string Navegacion { get; set; }

        [Display(Name = "Fecha Inicio Sesión")]
        [DataType(DataType.Date)]        
        public Nullable<System.DateTime> FechaInicioSesion { get; set; }

        [Display(Name = "Fecha Cierre de Sesión")]
        [DataType(DataType.Date)]        
        public Nullable<System.DateTime> FechaCierreSesion { get; set; }


    }

    public partial class tblMaestroCoInvestigador_Metadata
    {
        [Display(Name = "id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Usuario Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUsuario { get; set; }

        [Display(Name = "Tiempo ondas")]
        [Range(1, 100, ErrorMessage = "El tiempo ondas debe estar entre 1 y 100")]
        public Nullable<int> TiempoOndas { get; set; }

        [Display(Name = "Pregrado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El pregrado es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para pregrado")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string Pregrado { get; set; }

        [Display(Name = "Postgrado")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para postgrado")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string Postgrado { get; set; }

        [Display(Name = "Otro")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para otro")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string Otro { get; set; }

        [Display(Name = "Área Conocimiento Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El área de conocimiento es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idAreaConocimiento { get; set; }

        [Display(Name = "Experiencia Área Conocimiento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La experiencia en el área de conocimiento es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ExperienciaAreaConocimiento { get; set; }

        [Display(Name = "Institución Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Institución Id es obligatoria")]
        [Range(0, int.MaxValue)]
        public int idInstitucion { get; set; }

        // Variable utilizada para interactuar con el campo que muestra automáticamente las instituciones educativas
        // cuando se accede a IEP como maestro investigador
        [Display(Name = "Institution")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Institución es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Institution { get; set; }

    }

    public partial class tblMarcoReferenciaProy_Metadata
    {
        [Display(Name = "Marco Referencia Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Marco Referencia Proyecto Id es obligatorio")]
        public long tblMarcoReferenciaProy_ID { get; set; }

        [Display(Name = "Marco Referencia Proyecto Teórico")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]

        public string marRefProy_marcoTeoricoProy { get; set; }

        [Display(Name = "Marco Referencia Proyecto Antecedentes")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string marRefProy_marcoAntecedentesProy { get; set; }

        [Display(Name = "Marco Referencia Proyecto Conceptual")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string marRefProy_marcoConceptualProy { get; set; }

    }

    public partial class tblMarcoReferenciaProy_Rev_Metadata
    {
        [Display(Name = "Marco Referencia Proyecto Rev ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Paradigma Epistemológico es obligatorio")]

        public long tblMarcoReferenciaProy_Rev_ID { get; set; }

        [Display(Name = "Marco Referencia Proyecto Teórico")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Marco Referencia de Proyecto Teórico")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string marRefProy_marcoTeoricoProy { get; set; }

        [Display(Name = "Marco Referencia Proyecto Antecedentes")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Marco Referencia de Proyecto Antecedentes")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string marRefProy_marcoAntecedentesProy { get; set; }

        [Display(Name = "Marco Referencia Proyecto Conceptual")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Marco Referencia Proyecto Conceptual")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string marRefProy_marcoConceptualProy { get; set; }
    }

    public partial class tblMeta_Metadata
    {
        [Display(Name = "Meta Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Meta Id es obligatoria")]
        public long tblMeta_ID { get; set; }

        [Display(Name = "Meta grupo Investigación Estudiantil")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El grupo investigación estudiantil es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_grupoInvestigacionEstudiantil { get; set; }

        [Display(Name = "Meta Estudiante Investigado")]        
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de estudiantes es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_estudianteInvestigando { get; set; }

        [Display(Name = "Meta Grupo Investigación Docente")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El grupo de docentes es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_grupoInvestigacionDocente { get; set; }

        [Display(Name = "Meta Docente Investigando")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de docentes es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_docenteInvestigando { get; set; }

        [Display(Name = "Meta Establecimientos Educativos")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de las sedes educativas es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage ="El número no debe ser negativo")]
        
        public int met_establecimientosEducativos { get; set; }

        [Display(Name = "Meta Padres Formados 40")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de padres formados en 40h es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_padresFormados40 { get; set; }

        [Display(Name = "Meta estudiantes Formados 40")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de estudiantes formados en 40h es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_estudiantesFormados40 { get; set; }

        [Display(Name = "Meta Docentes Formados 120")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de padres formados en 120h es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_docentesFormados120 { get; set; }

        [Display(Name = "Meta Estudiantes Formados 180")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de estudiantes formados en 180h es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_estudiantesFormados180 { get; set; }

        [Display(Name = "Meta Docentes Formados 180")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El número de padres formados en 180h es obligatorio")]
        [Range(0, int.MaxValue, ErrorMessage = "El número no debe ser negativo")]
        public int met_docentesFormados180 { get; set; }

        [Display(Name = "Municipio Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Municipio Id es obligatoria")]
        [StringLength(5, ErrorMessage = "Máximo 5 caracteres")]

        public string idMunicipio { get; set; }

    }

    public partial class tblMetodoProy_Metadata
    {
        [Display(Name = "Método Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Método Proyecto Id es obligatorio")]
        public long tblMetodoProy_ID { get; set; }

        [Display(Name = "Paradigma Metodológico Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblParadigmaMetodologico_ID { get; set; }

        [Display(Name = "Met Proyecto Paradigma Metodológico")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string metProy_paradigmaMetodologicoProy { get; set; }

        [Display(Name = "Paradigma Epistemológico Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblParadigmaEpistemologico_ID { get; set; }

        [Display(Name = "Tipo Estudio Proyecto Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblTipoEstudioProy_ID { get; set; }

        [Display(Name = "Diseño Proyecto ID")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblDisenioProy_ID { get; set; }

        [Display(Name = "Histórico Hermenéutico")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblHistoricoHermeneutico { get; set; }

        [Display(Name = "Critico Social")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblCriticoSocial { get; set; }

        [Display(Name = "Met Proyecto paradigma Epistemológico")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]

        public string metProy_paradigmaEpistemologicoProy { get; set; }

        [Display(Name = "Met Proyecto población Muestra")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string metProy_poblacionMuestraProy { get; set; }

        [Display(Name = "Met Proyecto técnicas Instrumentos")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string metProy_tecnicasInstrumentosProy { get; set; }

        [Display(Name = "Met Proyecto Procedimiento")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string metProy_procedimientoProy { get; set; }

        [Display(Name = "Met Proyecto plan Análisis Datos")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string metProy_planAnalisisDatosProy { get; set; }

    }

    public partial class tblMetodoProy_Rev_Metadata
    {
        [Display(Name = "Método Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Método Proyecto Rev Id es obligatorio")]

        public long tblMetodoProy_Rev_ID { get; set; }

        [Display(Name = "Método Proyecto paradigma Metodológico")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string metProy_paradigmaMetodologicoProy { get; set; }

        [Display(Name = "Método paradigma Epistemológico")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string metProy_paradigmaEpistemologicoProy { get; set; }

        [Display(Name = "Método población Muestra")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string metProy_poblacionMuestraProy { get; set; }

        [Display(Name = "Método Proyecto técnicas Instrumentos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string metProy_tecnicasInstrumentosProy { get; set; }

        [Display(Name = "Método Proyecto procedimiento")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string metProy_procedimientoProy { get; set; }

        [Display(Name = "Método Proyecto plan Análisis Datos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string metProy_planAnalisisDatosProy { get; set; }

    }

    public partial class tblMiembroGrupo_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Usuario Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Usuario Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string idUsuario { get; set; }

        [Display(Name = "Rol Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Rol Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idRol { get; set; }

        [Display(Name = "Grado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grado es obligatorio")]
        [StringLength(10, ErrorMessage = "Máximo 10 caracteres")]
        public string Grado { get; set; }
    }

    public partial class tblMunicipios_Metadata
    {
        [Display(Name = "Municipio Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Municipio Id es obligatorio")]
        [StringLength(5, ErrorMessage = "Máximo 5 caracteres")]
        public string idMunicipio { get; set; }

        [Display(Name = "Departamento Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Departamento Id es obligatorio")]
        [StringLength(2, ErrorMessage = "Máximo 2 caracteres")]
        public string idDepartamento { get; set; }

        [Display(Name = "Nombre Municipio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Municipio es obligatorio")]
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string NombreMunicipio { get; set; }

        [Display(Name = "Nombre Departamento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Departamento es obligatorio")]
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string NombreDepartamento { get; set; }

        [Display(Name = "Región Id")]
        public Nullable<long> tblRegion_ID { get; set; }

    }

    public partial class tblNivel_Metadata
    {
        [Display(Name = "Nivel Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel Id es obligatorio")]

        public long tblNivel_ID { get; set; }

        [Display(Name = "Nivel nivel")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel nivel es obligatorio")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string tblNivel_nivel { get; set; }
    }

    public partial class tblNivelAcademicoEducacionSuperior_Metadata
    {

        [Display(Name = "Nivel Académico Educación Superior Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Paradigma Epistemológico es obligatorio")]

        public long tblNivelAcademicoEducacionSuperior_ID { get; set; }

        [Display(Name = "Nivel Académico Educación Superior Nombre")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para paradigma Epistemológico")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string nivAcaEduSup_nombre { get; set; }


    }

    public partial class tblNivelIdioma_Metadata
    {
        [Display(Name = "Nivel Idioma Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel Idioma Id es obligatorio")]

        public long tblNivelIdioma_ID { get; set; }

        [Display(Name = "Hoja Vida Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Hoja Vida Id es obligatoria")]

        public long tblHojaVida_ID { get; set; }

        [Display(Name = "Idiomas Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Idiomas Id es obligatorio")]

        public long tblIdiomas_ID { get; set; }

        [Display(Name = "Nivel Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel Id es obligatorio")]

        public long tblNivel_ID { get; set; }
    }

    public partial class tblNivelLengua_Metadata
    {
        [Display(Name = "Nivel Lengua Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel Lengua Id es obligatorio")]

        public long tblNivelLengua_ID { get; set; }

        [Display(Name = "Hoja Vida Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Hoja Vida Id es obligatorio")]

        public long tblHojaVida_ID { get; set; }

        [Display(Name = "Lenguas Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Lenguas Id es obligatorio")]

        public long tblLenguas_ID { get; set; }

        [Display(Name = "Nivel Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel Id es obligatorio")]

        public long tblNivel_ID { get; set; }
    }

    public partial class tblNoticias_Metadata
    {
        [Display(Name = "Noticia Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Noticia_ID { get; set; }

        [Display(Name = "Noticia Titular")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El titular es obligatorio")]
        //[RegularExpression(@"^[A-Z a-z0-9ÑñáéíóúÁÉÍÓÚ\\-\\#]+$", ErrorMessage = "Carácter no valido para Titular")]
        [StringLength(80, ErrorMessage = "Máximo 80 caracteres")]

        public string not_titulo { get; set; }

        [Display(Name = "Noticia Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(160, ErrorMessage = "Máximo 160 caracteres")]

        public string not_descripcion { get; set; }

        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        [Display(Name = "Noticia contenido")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El contenido es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ.[]()#]+$", ErrorMessage = "Carácter no valido para Contenido")]
        [StringLength(4000, ErrorMessage = "Máximo 4000 caracteres")]

        public string not_contenido { get; set; }

        [Display(Name = "Noticia url imagen")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Url imagen es obligatoria")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]

        public string not_urlimage { get; set; }

        [Display(Name = "Noticia pie de foto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El pie de foto es obligatorio")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]
        public string not_piedefoto { get; set; }

        [Display(Name = "Noticia Fecha")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public System.DateTime not_fecha { get; set; }

        [Display(Name = "Noticia Autor")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El autor es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no válido para Autor")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string not_autor { get; set; }

        [Display(Name = "Noticia Url Potcast")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string not_urlpotcast { get; set; }

        [Display(Name = "Está Activo")]
        public Nullable<bool> estaActivo { get; set; }

    }

    public partial class tblParadigmaEpistemologico_Metadata
    {
        [Display(Name = "Paradigma Epistemológico ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblParadigmaEpistemologico_ID { get; set; }

        [Display(Name = "Paradigma Epistemológico")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El paradigma epistemológico es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para paradigma epistemológico")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string parEpi_nombre { get; set; }
    }

    public partial class tblParadigmaMetodologico_Metadata
    {
        [Display(Name = "Paradigma Metodológico ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblParadigmaMetodologico_ID { get; set; }

        [Display(Name = "Nombre paradigma metodológico")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El paradigma metodológico es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para el paradigma metodológico")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string parMet_nombre { get; set; }
    }

    public partial class tblPreguntaInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Consecutivo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Consecutivo es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Consecutivo { get; set; }

        [Display(Name = "Pregunta")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Pregunta es obligatoria")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ¿?.,:;¡!()'-]+$", ErrorMessage = "Carácter no valido para Pregunta")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]


        public string Pregunta { get; set; }

        [Display(Name = "Fecha de Creación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de Creación es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime FechaCreacion { get; set; }

        [Display(Name = "Fecha de Modificación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FechaModificacion { get; set; }

        [Display(Name = "Pregunta Principal")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Pregunta Principal es obligatoria")]

        public bool PreguntaPrincipal { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }
    }

    public partial class tblPreguntaProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Información uno")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La información uno es obligatoria")]

        public string InformacionUno { get; set; }

        [Display(Name = "Fuente Uno")]
        [StringLength(256, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fuente de información para uno es obligatoria")]
        public string FuenteUno { get; set; }

        [Display(Name = "Información Dos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La información dos es obligatoria")]
        public string InformacionDos { get; set; }

        [Display(Name = "Fuente Dos")]
        [StringLength(256, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fuente de información para dos es obligatoria")]
        public string FuenteDos { get; set; }

        [Display(Name = "Información Tres")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La información tres es obligatoria")]
        public string InformacionTres { get; set; }

        [Display(Name = "Fuente Tres")]
        [StringLength(256, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fuente de información para tres es obligatoria")]
        public string FuenteTres { get; set; }

        [Display(Name = "Reflexión")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La reflexión es obligatoria")]
        public string Reflexion { get; set; }

        [Display(Name = "Concepto Asesor")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ConceptoAsesor { get; set; }

        [Display(Name = "Revisión")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Revisión es obligatoria")]

        public bool Revision { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

    }

    public partial class tblPresentacionProyecto_Metadata
    {
        [Display(Name = "Presentación Proyecto Id")]
        public long tblPresentacionProyecto_ID { get; set; }

        [Display(Name = "Presentación Proyecto titulo")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Título es obligatorio")]
        public string preProy_tituloProy { get; set; }

        [Display(Name = "Presentación Proyecto resumen")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string preProy_resumenProy { get; set; }

        [Display(Name = "Presentación Proyecto palabra clave")]

        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string preProy_palabrasClavesProy { get; set; }

        [Display(Name = "Eje Investigación Id")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblEjeInvestigacion_ID { get; set; }

        [Display(Name = "Presentación proyecto eje investigación")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string preProy_ejeInvestigacionProy { get; set; }
    }

    public partial class tblPresentacionProyecto_Rev_Metadata
    {
        [Display(Name = "Presentación Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presentación Proyecto Rev Id es obligatorio")]

        public long tblPresentacionProyecto_Rev_ID { get; set; }

        [Display(Name = "Presentación Proyecto Título")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string preProy_tituloProy { get; set; }

        [Display(Name = "Presentación Proyecto Resumen ")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string preProy_resumenProy { get; set; }

        [Display(Name = "Presentación Proyecto palabras Claves")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string preProy_palabrasClavesProy { get; set; }

        [Display(Name = "Presentación Proyecto Eje Investigación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string preProy_ejeInvestigacionProy { get; set; }
    }

    public partial class tblPresupuestoProy_Metadata
    {
        [Display(Name = "Presupuesto Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        public long tblPresupuestoProy_ID { get; set; }

        [Display(Name = "Presupuesto Financiación Investic")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presupuesto Financiación Investic es obligatorio")]

        public long pre_financiacionInvestic { get; set; }

        [Display(Name = "Presupuesto Total Investic")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ElPresupuesto Total Investic es obligatorio")]

        public long pre_totatInvestic { get; set; }

        [Display(Name = "Presupuesto Total Otra Fuente")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presupuesto Total Otra Fuente es obligatorio")]

        public long pre_totalOtraFuente { get; set; }

        [Display(Name = "Presupuesto Total")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presupuesto Total es obligatorio")]

        public long pre_total { get; set; }
    }

    public partial class tblPresupuestoProy_Rev_Metadata
    {
        [Display(Name = "Presupuesto Proyecto Revisión Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Paradigma Epistemológico es obligatorio")]

        public long tblPresupuestoProy_Rev_ID { get; set; }

        [Display(Name = "Presupuesto Proyecto revisión")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string preProy_revision { get; set; }
    }

    public partial class tblPresupuestoProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Rubro Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El rubro es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idRubro { get; set; }

        [Display(Name = "Descripción Gasto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción del gasto es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ,.]+$", ErrorMessage = "Carácter no válido para la descripción del gasto")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string DescripcionGasto { get; set; }

        [Display(Name = "Valor Rubro")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El valor del rubro es obligatorio")]
        [Range(1, 1000, ErrorMessage = "La cantidad debe estar entre 1 y 1000")]
        public double ValorRubro { get; set; }

        [Display(Name = "Valor Unitario")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El valor unitario es obligatorio")]

        [Range(1, 10000000, ErrorMessage = "El valor unitario debe estar entre $1 y $10.000.000")]
        public double ValorUnitario { get; set; }

        [Display(Name = "Total")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El total es obligatorio")]

        [Range(1, double.MaxValue, ErrorMessage = "El total debe ser superior a $1")]
        public double Total { get; set; }
    }

    public partial class tblProblemaInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion { get; set; }

        [Display(Name = "Justificación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La justificación es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Justificacion { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

    }

    public partial class tblProblemaInvestigacionProy_Metadata
    {
        [Display(Name = "Problema Investigación Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        public long tblProblemaInvestigacionProy_ID { get; set; }

        [Display(Name = "Problema Investigación Proyecto planteamiento Problema")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]

        public string proInvProy_planteamientoProblemaProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto pregunta Investigación ")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string proInvProy_preguntaInvestigacionProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Sub-pregunta Investigación")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string proInvProy_subpreguntaInvestigacionProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto justificación")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string proInvProy_justificacionProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Objetivo General")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string proInvProy_objetivoGeneralProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Objetivos Específicos ")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]
        public string proInvProy_objetivosEspecificosProy { get; set; }
    }

    public partial class tblProblemaInvestigacionProy_Rev_Metadata
    {
        [Display(Name = "Problema Investigación Proyecto Revisión ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El ID es obligatorio")]

        public long tblProblemaInvestigacionProy_Rev_ID { get; set; }

        [Display(Name = "Problema Investigación Proyecto planteamiento Problema")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string proInvProy_planteamientoProblemaProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto pregunta Investigación ")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string proInvProy_preguntaInvestigacionProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Sub-pregunta investigación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string proInvProy_subpreguntaInvestigacionProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Justificación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string proInvProy_justificacionProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Objetivo General")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string proInvProy_objetivoGeneralProy { get; set; }

        [Display(Name = "Problema Investigación Proyecto Objetivos Específicos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string proInvProy_objetivosEspecificosProy { get; set; }
    }

    public partial class tblProblemaProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Como")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El relato es obligatorio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string Como { get; set; }

        [Display(Name = " Reflexión")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La reflexión es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Reflexion { get; set; }

        [Display(Name = "Concepto Asesor")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ConceptoAsesor { get; set; }

        [Display(Name = "Revisión")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Revisión es obligatoria")]

        public bool Revision { get; set; }

        [Display(Name = "Grupo investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }
    }

    public partial class tblProductosAcademicos_Metadata
    {
        [Display(Name = "Productos Académicos Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        public long tblProductosAcademicos_ID { get; set; }

        [Display(Name = "Productos Académicos Título Producto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título del producto es obligatorio")]        
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string proaca_tituloProducto { get; set; }

        [Display(Name = "Productos Académicos Terminación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de publicación del producto es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> proaca_anoTerminacion { get; set; }

        [Display(Name = "Categoría Productos Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Categoría Productos Id es obligatorio")]

        public long tblCategoriaProductos_ID { get; set; }

        [Display(Name = "Hoja Vida Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Hoja Vida Id es obligatorio")]

        public long tblHojaVida_ID { get; set; }
    }

    public partial class tblProductosContratista_Metadata
    {
        [Display(Name = "Producto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Producto Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Id_Producto { get; set; }

        [Display(Name = "Contratista Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Contratista Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]


        public string Id_Contratista { get; set; }

        [Display(Name = "Actividad Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Actividad Id es obligatoria")]


        [Range(0, int.MaxValue)]
        public int Id_Actividad { get; set; }

        [Display(Name = "Nombre del Producto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre del Producto es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre del Producto")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]


        public string Nombre_Producto { get; set; }

        [Display(Name = "Descripción Producto")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion_Producto { get; set; }
    }

    public partial class tblProgramaTareasContratista_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Id { get; set; }

        [Display(Name = "Contratista Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Contratista Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]

        public string Id_Contratista { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]


        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Descripción es obligatoria")]

        public string Descripcion { get; set; }

        [Display(Name = "Alternativa")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Alternativa es obligatoria")]

        [Range(0, int.MaxValue)]
        public int Alternativa { get; set; }

        [Display(Name = "Responsabilidad")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Responsabilidad es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Responsabilidad { get; set; }

        [Display(Name = "Fecha de Inicio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de Inicio es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime Fecha_Ini { get; set; }

        [Display(Name = "Fecha final")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha final es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public System.DateTime Fecha_Fin { get; set; }

        [Display(Name = "Estado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Estado { get; set; }

    }

    public partial class tblPropagacionGrupo_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo investigación Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Tipo Feria Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El tipo feria es obligatorio")]
        [Range(0, int.MaxValue)]
        public int idTipoFeria { get; set; }

        [Display(Name = "Archivo")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El archivo es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Archivo")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]

        public string Archivo { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion { get; set; }

    }

    public partial class tblProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]


        public string Nombre { get; set; }

        [Display(Name = "Fecha de Inicio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de Inicio es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public System.DateTime FechaInicio { get; set; }

        [Display(Name = "Fecha de Finalización")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de Finalización es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public System.DateTime FechaFinalizacion { get; set; }
    }

    public partial class tblProyectosGruposInvstigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        public long id { get; set; }

        [Display(Name = "Proyectos de investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Proyecto de investigación Id es obligatorio")]

        public long tblProyectosInvestigacion_ID { get; set; }

        [Display(Name = "Título Id")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string tituloProy { get; set; }

        [Display(Name = "Resumen")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string resumenProy { get; set; }

        [Display(Name = "Palabras Claves")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string palabrasClavesProy { get; set; }

        [Display(Name = "Eje investigación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ejeInvestigacionProy { get; set; }

        [Display(Name = "Justificación Eje Inv ")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string justEjeInvProy { get; set; }

        [Display(Name = "Problema")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string problemaProy { get; set; }

        [Display(Name = "Pregunta")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string preguntaProy { get; set; }

        [Display(Name = "Sub-pregunta ")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string subpreguntaProy { get; set; }

        [Display(Name = "Justificación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string justProy { get; set; }

        [Display(Name = "Objetivo General")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string objGeneralProy { get; set; }

        [Display(Name = "Objetivos Específicos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string objEspecificosProy { get; set; }

        [Display(Name = "Marco Teórico")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string marcoTeoricoProy { get; set; }

        [Display(Name = "Marco Antecedentes")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string marcoAntecedentesProy { get; set; }

        [Display(Name = "Marco Contextual")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string marcoContextualProy { get; set; }

        [Display(Name = "Paradigma Método")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string paradigmaMetodoProy { get; set; }

        [Display(Name = "Paradigma Epistemológico")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string paradigmaEpistemProy { get; set; }

        [Display(Name = "Tipo Estudio")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string tipoEstudioProy { get; set; }

        [Display(Name = "Diseños")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string diseniosProy { get; set; }

        [Display(Name = "Histórico Herramienta")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string historicoHermProy { get; set; }

        [Display(Name = "Critico Social")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string criticoSocialProy { get; set; }

        [Display(Name = "Población")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string poblacionProy { get; set; }

        [Display(Name = "Procedimiento")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string procedimientoProy { get; set; }

        [Display(Name = "Técnicas Recolección de Información")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string tecnicasRecInfoProy { get; set; }

        [Display(Name = "Plan de Datos")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string planDatosProy { get; set; }

        [Display(Name = "Resultados")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string resultadosProy { get; set; }

        [Display(Name = "Caracterización")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string caracterizacionProy { get; set; }

        [Display(Name = "Referencias")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string referenciasProy { get; set; }

    }

    public partial class tblProyectosInvestigacion_Metadata
    {
        [Display(Name = "Proyectos de investigación Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Proyecto de investigación Id es obligatorio")]
        [Required]
        public long tblProyectosInvestigacion_ID { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]
        public long tblGruposInvestigacion_ID { get; set; }

        [Display(Name = "Estado Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Id es obligatorio")]
        public long tblEstado_ID { get; set; }

        [Display(Name = "Presentación Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Presentación Proyecto Id es obligatoria")]
        public long tblPresentacionProyecto_ID { get; set; }

        [Display(Name = "Problema investigación Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Problema investigación Proyecto Id es obligatorio")]
        public long tblProblemaInvestigacionProy_ID { get; set; }

        [Display(Name = "Marco Referencia Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Paradigma Epistemológico es obligatorio")]
        public long tblMarcoReferenciaProy_ID { get; set; }

        [Display(Name = "Método Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Marco Referencia Proyecto Id es obligatorio")]
        public long tblMetodoProy_ID { get; set; }

        [Display(Name = "Características Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Las Características Proyecto Id es obligatorio")]
        public long tblCaracteristicasProy_ID { get; set; }

        [Display(Name = "Cronograma Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Proyecto Id es obligatorio")]
        public long tblCronogramaProy_ID { get; set; }

        [Display(Name = "Presupuesto Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presupuesto Proyecto Id es obligatorio")]
        public long tblPresupuestoProy_ID { get; set; }

        [Display(Name = "Referencias Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Referencia del Proyecto Id es obligatorio")]
        public long tblReferenciasProy_ID { get; set; }

        [Display(Name = "Evaluación Proyecto Colciencias Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Evaluación del Proyecto Colciencias Id es obligatorio")]
        public long tblEvaluacionProyColciencias_ID { get; set; }

        [Display(Name = "Evaluación Proyecto Investic Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Evaluación Proyecto Investic Id es obligatorio")]
        public long tblEvaluacionProyInvestic_ID { get; set; }

        [Display(Name = "Evaluación Proyecto Evaluador1 Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Evaluación del Proyecto Evaluador1 Id es obligatorio")]
        public long tblEvaluacionProyEvaluador1_ID { get; set; }

        [Display(Name = "Evaluación Proyecto Evaluador2 Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Evaluación del Proyecto Evaluador2 Id es obligatorio")]
        public long tblEvaluacionProyEvaluador2_ID { get; set; }

        [Display(Name = "Proyecto Inv nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del proyecto es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para el nombre")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string proyInv_nombreProyecto { get; set; }

        [Display(Name = "Proyecto Inv fecha Creación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Proyecto Inv fecha Creación es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime proyInv_fechaCreacion { get; set; }

        [Display(Name = "Proyecto Inv fecha Ultima Modificación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha Ultima Modificación del proyecto es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime proyInv_fechaUltimaModificacion { get; set; }

    }

    public partial class tblProyectosInvestigacion_Rev_Metadata
    {
        [Display(Name = "Proyecto Investigación Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Proyecto de Investigación Rev Id es obligatorio")]
        public long tblProyectosInvestigacion_Rev_ID { get; set; }

        [Display(Name = "Proyectos Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Proyecto de Investigación Id es obligatorio")]
        public long tblProyectosInvestigacion_ID { get; set; }

        [Display(Name = "Estado Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Estado Id es obligatorio")]
        public long tblEstado_ID { get; set; }

        [Display(Name = "Presentación Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Presentación Proyecto Rev Id es obligatoria")]
        public long tblPresentacionProyecto_Rev_ID { get; set; }

        [Display(Name = "Problema investigación Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Problema investigación Proyecto Rev Id es obligatorio")]
        public long tblProblemaInvestigacionProy_Rev_ID { get; set; }

        [Display(Name = "Marco Referencia Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Marco Referencia Proyecto Rev Id es obligatorio")]
        public long tblMarcoReferenciaProy_Rev_ID { get; set; }

        [Display(Name = "Método Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Método Proyecto Rev Id es obligatorio")]
        public long tblMetodoProy_Rev_ID { get; set; }

        [Display(Name = "Características Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Las Características Proyecto Rev Id es obligatorio")]
        public long tblCaracteristicasProy_Rev_ID { get; set; }

        [Display(Name = "Cronograma Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Cronograma Proyecto Rev Id es obligatorio")]
        public long tblCronogramaProy_Rev_ID { get; set; }

        [Display(Name = "Presupuesto Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presupuesto Proyecto Rev Id es obligatorio")]
        public long tblPresupuestoProy_Rev_ID { get; set; }

        [Display(Name = "Referencias Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Referencias Proyecto Rev Id es obligatorio")]
        public long tblReferenciasProy_Rev_ID { get; set; }

        [Display(Name = "Usuario Plataforma Evaluador Id")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]

        public string tblUsuarioPlataforma_Evaluador_ID { get; set; }

        [Display(Name = "Proyecto Inv Rev Fecha Asignación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> proyInvRev_fechaAsignacion { get; set; }

        [Display(Name = "Proyecto Inv Rev Fecha Limite Evaluación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> proyInvRev_fechaLimiteEvaluacion { get; set; }

        [Display(Name = "Proyecto Inv Rev Fecha Ultima Evaluación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> proyInvRev_fechaUltimaEvaluacion { get; set; }
        
    }

    public partial class tblRecoleccionInformacionProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El identificador es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Instrumento Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El instrumento es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idInstrumento { get; set; }

        [Display(Name = "Evidencia")]
        //[Required(ErrorMessage = "La evidencia es obligatoria")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ.-]+$", ErrorMessage = "Carácter no valido para la Evidencia")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string Evidencia { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(256, ErrorMessage = "Máximo 256 caracteres")]
        public string Descripcion { get; set; }
    }

    public partial class tblRecursosEducativos_Metadata
    {
        [Display(Name = "Recurso educativo Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id_rec_educativo { get; set; }

        [Display(Name = "Nombre Recurso Educativo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ.,@]+$", ErrorMessage = "Caractéres no valido para nombre")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string nom_rec_educativo { get; set; }

        [Display(Name = "Descripción Recursos Educativo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ.,@]+$", ErrorMessage = "Caractéres no valido para descripción")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]
        public string desc_rec_educativo { get; set; }

        [Display(Name = "Icono Recursos Educativo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El icono es obligatorio")]
        ////[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ/]+$", ErrorMessage = "Caractéres No valido para Icono")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]
        public string icono_rec_educativo { get; set; }

        [Display(Name = "Archivo Recurso Educativo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo es obligatorio")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ/]+$", ErrorMessage = "Caractéres No valido para Archivo")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string archivo_rec_educativo { get; set; }

        [Display(Name = "Autor Recurso Educativo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El autor es obligatorio")]
        ////[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caractéres No valido para Autor")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres")]
        public string autor_rec_educativo { get; set; }

        [Display(Name = "Usuario Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El usuario es obligatorio")]
        ////[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Caractéres No valido para Autor")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string id_user { get; set; }

        [Display(Name = "Colección Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La colección es obligatorio")]
        [Range(1, int.MaxValue, ErrorMessage = "La Colección es obligatorio")]
        public int id_coleccion { get; set; }


        [Display(Name = "Palabras Clave")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Lac palabras clave son obligatorias")]
        //[RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ/]+$", ErrorMessage = "Caractéres No valido para Archivo")]
        [StringLength(300, ErrorMessage = "Máximo 300 caracteres")]
        public string palabra_clave { get; set; }

        [Display(Name = "Idioma Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El idioma es obligatorio")]
        [Range(1, int.MaxValue, ErrorMessage = "El idioma es obligatorio")]
        public Nullable<long> tblIdiomas_ID { get; set; }

    }

    public partial class tblReferenciasProy_Metadata
    {
        [Display(Name = "Referencias Proyecto Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Referencias Proyecto Id es obligatorio")]

        public long tblReferenciasProy_ID { get; set; }

        [Display(Name = "Referencias del Proyecto")]
        [StringLength(3000, ErrorMessage = "Máximo 3000 caracteres")]

        public string refProy_referencias { get; set; }
    }

    public partial class tblReferenciasProy_Rev_Metadata
    {
        [Display(Name = "Referencias Proyecto Rev Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Referencias Proyecto Rev Id es obligatorio")]

        public long tblReferenciasProy_Rev_ID { get; set; }

        [Display(Name = "Referencias del Proyecto")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string refProy_referencias { get; set; }
    }

    public partial class tblReflexionOnda_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Introducción")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]


        public string Introduccion { get; set; }

        [Display(Name = "Conformación Grupo")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]
        public string ConformacionGrupo { get; set; }

        [Display(Name = "Objetivo investigación")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]
        public string ObjetivoInvestigacion { get; set; }

        [Display(Name = "Actividades Realizadas")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]
        public string ActividadasRealizadas { get; set; }

        [Display(Name = "Conceptos Principales")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]
        public string ConceptosPrincipales { get; set; }

        [Display(Name = "Espacios Participación")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]
        public string EspaciosParticipacion { get; set; }

        [Display(Name = "Conclusiones")]
        [StringLength(8000, ErrorMessage = "Máximo 8000 caracteres")]
        public string Conclusiones { get; set; }

        [Display(Name = "Grupo investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Fecha Inicio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FechaInicio { get; set; }

        [Display(Name = "Fecha Fin")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FechaFin { get; set; }
    }

    public partial class tblReflexionProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Fecha inicio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de inicio es obligatoria")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public System.DateTime FechaInicio { get; set; }

        [Display(Name = "Ultima modificación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> UltimaModificacion { get; set; }

        [Display(Name = "Proceso")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El proceso es obligatorio")]

        public string Proceso { get; set; }

        [Display(Name = "Motivación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La motivación es obligatoria")]
        public string Motivacion { get; set; }

        [Display(Name = "Reflexión")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La reflexión es obligatoria")]
        public string Reflexion { get; set; }

        [Display(Name = "Concepto asesor")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string ConceptoAsesor { get; set; }

        [Display(Name = "Revisado")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El revisado es obligatorio")]

        public bool Revisado { get; set; }

        [Display(Name = "Grupo investigación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El grupo de investigación es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }
    }

    public partial class tblRegion_Metadata
    {
        [Display(Name = "Región Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Región Id es obligatoria")]
        public long tblRegion_ID { get; set; }

        [Display(Name = "Región Nombre")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string reg_nombre { get; set; }

    }

    public partial class tblResponsabContratista_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Id { get; set; }

        [Display(Name = "Contratista Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Contratista Id es obligatorio")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]


        public string Id_Contratista { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Descripción es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Descripción")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]


        public string Descripcion { get; set; }

        [Display(Name = "Coordinador Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Coordinador Id es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Coordinador Id")]
        [StringLength(128, ErrorMessage = "Máximo 128 caracteres")]
        public string Id_Coordinador { get; set; }

        [Display(Name = "Consecutivo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Consecutivo es obligatorio")]

        [Range(0, int.MaxValue)]
        public int Consecutivo { get; set; }

        [Display(Name = "Identificador Responsable")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Identificador Responsable es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Identificador Responsable")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string IdentificadorResponsa { get; set; }

    }

    public partial class tblRol_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Nombre { get; set; }

    }

    public partial class tblRubro_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Rubro")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El rubro es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para el rubro")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string Rubro { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion { get; set; }
    }

    public partial class tblRubroPresupuesto_Metadata
    {
        [Display(Name = "Rubro Presupuesto Id")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "El Rubro Presupuesto Id es obligatorio")]
        [Required]
        public long tblRubroPresupuesto_ID { get; set; }

        [Display(Name = "Presupuesto Proy Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Presupuesto Proy Id es obligatorio")]
        public long tblPresupuestoProy_ID { get; set; }

        [Display(Name = "Rubro Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Rubro Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int tblRubro_ID { get; set; }

        [Display(Name = "Rubro Presupuesto Valor")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Rubro Presupuesto Valor es obligatorio")]
        public long rubPre_valor { get; set; }

        [Display(Name = "Rubro Presupuesto Fuente")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fuente del rubro es obligatoria")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para fuente del rubro")]
        public string rubPre_fuente { get; set; }

        [Display(Name = "Rubro Presupuesto Justificación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La justificación del rubro es obligatoria")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para la justificación del rubro")]
        public string rubPre_justificacion { get; set; }

    }

    public partial class tblSedeEducativa_Metadata
    {
        [Display(Name = "Sede Educativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Sede Educativa Id es obligatoria")]
        public long tblSedeEducativa_ID { get; set; }

        [Display(Name = "Sede Educativa Código DANE")]
        [Range(0, int.MaxValue)]
        public Nullable<int> sedEdu_codigoDane { get; set; }

        [Display(Name = "Sede Educativa Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string sedEdu_nombre { get; set; }

        [Display(Name = "Sede Educativa Dirección")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string sedEdu_direccion { get; set; }

        [Display(Name = "Sede Educativa Teléfono")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string sedEdu_telefono { get; set; }

        [Display(Name = "Institución Educativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Institución Educativa Id es obligatoria")]
        [Range(0, int.MaxValue)]
        public int tblInstitucionEducativa_ID { get; set; }

    }

    public partial class tblSeguimientoProyectoInvestigacion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Grupo Investigación Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Grupo Investigación Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Fecha de Creación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de Creación es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public System.DateTime FechaCreacion { get; set; }

        [Display(Name = "Fecha de Modificación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FechaModificacion { get; set; }

        [Display(Name = "Origen")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Origen es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para El Origen")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]


        public string Origen { get; set; }

        [Display(Name = "Motivación")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Motivación es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para El Origen")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string Motivacion { get; set; }

        [Display(Name = "Reflexión")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Reflexión es obligatoria")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para El Origen")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]

        public string Reflexion { get; set; }

        [Display(Name = "Observación")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Observacion { get; set; }

    }

    public partial class tblTipoEstablecimiento_Metadata
    {
        [Display(Name = "Tipo Establecimiento Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Tipo Establecimiento Id es obligatorio")]

        public long tblTipoEstablecimiento_ID { get; set; }

        [Display(Name = "Tipo Establecimiento Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string tipEst_nombre { get; set; }

    }

    public partial class tblTipoEstudioProy_Metadata
    {
        
        [Display(Name = "tblTipoEstudioProy_ID")]
        [Required]
        [Range(0, int.MaxValue)]
        public int tblTipoEstudioProy_ID { get; set; }

        [Display(Name = "Nombre tipo de estudio del proyecto")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del tipo de estudio del proyecto es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ-]+$", ErrorMessage = "Carácter no válido para el nombre del tipo de estudio del proyecto")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string tipEst_nombre { get; set; }

        public Nullable<int> id_tblParadigmaEpistemologico { get; set; }


    }

    public partial class tblTipoFeria_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Tipo Feria")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Tipo Feria es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Tipo Feria")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string TipoFeria { get; set; }

        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

    }

    public partial class tblTipoInstitucion_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]
        public long id { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string Nombre { get; set; }

    }

    public partial class tblTituloEducacionSuperior_Metadata
    {
        [Display(Name = "Título Educación Superior Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Título Educación Superior Id es obligatorio")]
        public long tblTituloEducacionSuperior_ID { get; set; }

        [Display(Name = "Título Educación Superior Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título es obligatorio")]
        [RegularExpression(@"^[a-zA-Z ñáéíóúÁÉÍÓÚÑ.]+$", ErrorMessage = "Carácter no válido para título de educación superior")]       
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]

        public string titEduSup_nombre { get; set; }

        [Display(Name = "Título Educación Superior Año Graduación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> titEduSup_anoGraduacion { get; set; }

        [Display(Name = "Hoja Vida Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El título educación superior Año Graduación es obligatorio")]
        public long tblHojaVida_ID { get; set; }

        [Display(Name = "Nivel Académico Educación Superior Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nivel Académico Educación Superior Id es obligatorio")]
        public long tblNivelAcademicoEducacionSuperior_ID { get; set; }
    }

    public partial class tblTutorZona_Metadata
    {
        [Display(Name = "Tutor Zona Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        public long tblTutorZona_ID { get; set; }

        [Display(Name = "Tutor Zona Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string tutZon_nombre { get; set; }

        [Display(Name = "Tutor Zona Apellido")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El apellido es obligatorio")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string tutZon_apellido { get; set; }

        [Display(Name = "Tutor Zona Correo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El correo electrónico es obligatorio")]
        [EmailAddress(ErrorMessage ="El formato del correo electrónico no es valido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]

        public string tutZon_correo { get; set; }

        [Display(Name = "Tutor Zona Teléfono")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El teléfono es obligatorio")]
        [RegularExpression(@"^[0-9 ]+$", ErrorMessage = "Número de teléfono no válido. Solo debes ingresar números sin puntos ni comas, máximo hasta 15 dígitos")]
        [StringLength(15, ErrorMessage = "Máximo 15 dígitos")]

        public string tutZon_telefono { get; set; }

        [Display(Name = "Institución Educativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La institución es obligatoria")]
        [Range(0, int.MaxValue)]
        public Nullable<int> tblInstitucionEducativa_ID { get; set; }

        [Display(Name = "Está activo")]
        public Nullable<bool> estaActivo { get; set; }

    }

    public partial class tblTutorZonaSedeEducativa_Metadata
    {
        [Display(Name = "Tutor Zona Sede Educativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Tutor Zona Sede Educativa Id es obligatorio")]

        public long tblTutorZonaSedeEducativa_ID { get; set; }

        [Display(Name = "Tutor Zona Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Tutor Zona Id es obligatorio")]

        public long tblTutorZona_ID { get; set; }

        [Display(Name = "Sede Educativa Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Sede Educativa Id es obligatoria")]

        public long tblSedeEducativa_ID { get; set; }

    }

    public partial class tblValoracionBitacora_Metadata
    {
        [Display(Name = "idValoracion")]
        [Range(0, int.MaxValue)]
        public int idValoracion { get; set; }

        [Display(Name = "Grupo Investigación")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(200, ErrorMessage = "Máximo 200 caracteres")]
        public string descripcionValoracion { get; set; }

        [Display(Name = "Archivo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo es obligatorio")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string archivoValoracion { get; set; }
        
    }

    public partial class tblParticipacionGrupo_Metadata
    {
        [Display(Name = "idParticipacion")]
        [Range(0, int.MaxValue)]
        public int idParticipacion { get; set; }

        [Display(Name = "Grupo Investigación")]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Descripción")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La descripción es obligatoria")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string descripcionParticipacion { get; set; }

        [Display(Name = "Archivo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El archivo es obligatorio")]
        [StringLength(255, ErrorMessage = "Máximo 255 caracteres")]
        public string archivoParticipacion { get; set; }

    }

    public partial class tblZona_Metadata
    {
        [Display(Name = "Id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Id es obligatorio")]

        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        [RegularExpression(@"^[a-zA-Z0-9 ñáéíóúÁÉÍÓÚÑ]+$", ErrorMessage = "Carácter no valido para Nombre")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        public string Descripcion { get; set; }

    }

    public partial class tbNetVirtualCategoryResource_Metadata
    {
        
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "nombre")]
        [Required]
        public string name { get; set; }

        [Display(Name = "imagen")]        
        [DataType(DataType.Upload)]
        public byte[] image { get; set; }

        [Display(Name = "JsonMetadata")]
        public string JsonMetadata { get; set; }


    }

    public partial class tbNetVirtualForo_Metadata
    {
        
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "idUser")]
        [Required]
        public System.Guid idUser { get; set; }

        [Display(Name = "idGrupoInvestigacion")]
        [Required]
        [Range(0, int.MaxValue)]
        public int idGrupoInvestigacion { get; set; }

        [Display(Name = "Titulo")]
        [Required]
        [StringLength(300, ErrorMessage = "Solo se permite 300 caracteres para el título.")]
        public string Titulo { get; set; }

        [Display(Name = "Mensaje")]
        [Required]
        public string Mensaje { get; set; }

        [Display(Name = "Fecha")]
        [Required]
        public System.DateTime Fecha { get; set; }

        [Display(Name = "Respuestas")]
        [Required]
        [Range(0, int.MaxValue)]
        public int Respuestas { get; set; }

        [Display(Name = "idForo")]
        [Range(0, int.MaxValue)]
        public Nullable<int> idForo { get; set; }

        [Display(Name = "FechaUltimaRespuesta")]
        public Nullable<System.DateTime> FechaUltimaRespuesta { get; set; }


        public bool ishide { get; set; }
        
    }

    public partial class tbNetVirtualGroup_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "nombre")]
        [Required]
        [StringLength(300, ErrorMessage = "Solo se permite 300 caracteres para el nombre.")]
        public string name { get; set; }

        [Display(Name = "descripción")]
        [Required]
        public string description { get; set; }

        [Display(Name = "emblema")]
        //[Required]
        [DataType(DataType.Upload)]
        public byte[] photo { get; set; }

        [Display(Name = "state")]
        [Required]
        public bool state { get; set; }

        [Display(Name = "JsonMetadata")]
        public string JsonMetadata { get; set; }

        [Display(Name = "createDate")]
        public Nullable<System.DateTime> createDate { get; set; }

    }

    public partial class tbNetVirtualMessage_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "message")]
        [Required]
        public string message { get; set; }

        [Display(Name = "idGroup")]
        [Required]
        [Range(0, int.MaxValue)]
        public int idGroup { get; set; }

        [Display(Name = "dateSend")]
        [Required]
        public System.DateTime dateSend { get; set; }


    }

    public partial class tbNetVirtualResource_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "idNetVirtualUser")]
        [Required]
        public System.Guid idNetVirtualUser { get; set; }

        [Display(Name = "resource")]
        [DataType(DataType.Upload)]
        public byte[] resource { get; set; }

        [Display(Name = "JsonMetadata")]
        public string JsonMetadata { get; set; }

        [Display(Name = "name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatorio")]
        public string name { get; set; }

        [Display(Name = "description")]
        public string description { get; set; }

        [Display(Name = "idCategory")]
        [Range(0, int.MaxValue)]
        public Nullable<int> idCategory { get; set; }
    }

    public partial class tbNetVirtualUser_Metadata
    {
        
        [Display(Name = "id")]
        [Required]
        public System.Guid id { get; set; }

        [Display(Name = "photo")]
        [DataType(DataType.Upload)]
        public byte[] photo { get; set; }

        [Display(Name = "state")]
        [Required]
        public bool state { get; set; }

        [Display(Name = "JsonMetadata")]
        public string JsonMetadata { get; set; }

    }

    public partial class tbNetVirtualUserGroup_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "idNetVirtualUser")]
        [Required]
        public System.Guid idNetVirtualUser { get; set; }

        [Display(Name = "idNetVirtualGroup")]
        [Required]
        [Range(0, int.MaxValue)]
        public int idNetVirtualGroup { get; set; }

        [Display(Name = "StateUserAceptGroup")]
        [Required]
        [Range(0, int.MaxValue)]
        public int StateUserAceptGroup { get; set; }

        [Display(Name = "isOwner")]
        [Required]
        public bool isOwner { get; set; }

    }

    public partial class tbNetVirtualWall_Metadata
    {
        [Display(Name = "id")]
        [Required]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "message")]
        public string message { get; set; }

        [Display(Name = "idNetVirtualUserGroup")]
        [Range(0, int.MaxValue)]
        public Nullable<int> idNetVirtualUserGroup { get; set; }

        [Display(Name = "dateSend")]
        public Nullable<System.DateTime> dateSend { get; set; }

    }

    public partial class tblInfoGeneral_Metadata
    {
        [Display(Name = "Id")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre es obligatoriore")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        public string nombre { get; set; }

        [Display(Name = "Campo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El email es obligatorio")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Dirección de email inválida")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres")]
        public string campo { get; set; }

        [Display(Name = "Contraseña")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo es obligatorio")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres")]
        public string password { get; set; }
    }

    public partial class tblLinkCVEMP_Metadata
    {
        [Display(Name = "Id")]
        [Range(0, int.MaxValue)]
        public int id { get; set; }

        [Display(Name = "Categoria")]
        public string categoria { get; set; }

        [Display(Name = "Elemento")]
        public string elemento { get; set; }

        [Display(Name = "Link")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo es obligatorio")]
        [StringLength(300, ErrorMessage = "Máximo 300 caracteres")]
        [Url(ErrorMessage = "Dirección de url es inválida")]
        public string link { get; set; }
    }

}