﻿<% Response.StatusCode = 403 %>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" style="overflow-y: hidden;" lang="en">
<!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>403 Prohibido</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Libs CSS -->
    <link type="text/css" media="all" href="errorfolder/bootstrap.css" rel="stylesheet">
    <!-- Template CSS -->
    <link type="text/css" media="all" href="errorfolder/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link type="text/css" media="all" href="errorfolder/respons.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="errorfolder/css.css" rel="stylesheet" type="text/css">

</head>
<body>

    <!-- Load page -->
    <div class="animationload" style="display: none;">
        <div class="loader" style="display: none;">
        </div>
    </div>
    <!-- End load page -->
    <!-- Content Wrapper -->
    <div id="wrapper">
        <div class="container">
            <div class="col-xs-12 col-sm-7 col-lg-7">
                <!-- Info -->
                <div class="info">
                    <h1>403</h1>
                    <h2>¡Prohibido!</h2>
                    <p>No tiene permiso para acceder al recurso solicitado.</p>
                    <a href="http://investic.udenar.edu.co" class="btn">Ir al inicio</a>
                    <a href="http://investic.udenar.edu.co/Home/Contactenos" class="btn btn-green">Contactarnos</a>
                </div>
                <!-- end Info -->
            </div>

            <div class="col-xs-12 col-sm-5 col-lg-5 text-center">
                <!-- Fighting -->
                <div class="fighting">
                    <img src="errorfolder/403.png" alt="Fighting">
                </div>
                <!-- end Fighting -->
            </div>

        </div>
        <!-- end container -->
    </div>
    <!-- end Content Wrapper -->
    <!-- Scripts -->
    <script src="errorfolder/jquery-2.js" type="text/javascript"></script>
    <script src="errorfolder/bootstrap.js" type="text/javascript"></script>
    <script src="errorfolder/modernizr.js" type="text/javascript"></script>
    <script src="errorfolder/jquery.js" type="text/javascript"></script>
    <script src="errorfolder/scripts.js" type="text/javascript"></script><div id="ascrail2000" class="nicescroll-rails" style="width: 2px; z-index: 999999; cursor: default; position: fixed; top: 0px; height: 100%; right: 0px; display: none; opacity: 0;"><div style="position: relative; top: 0px; float: right; width: 2px; height: 275px; background-color: rgb(255, 255, 255); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px;"></div></div>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</body>
</html>