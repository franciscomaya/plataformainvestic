﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using dl = ClassLibrary;

namespace INI.Controllers
{
    //[Authorize(Roles = "Administrator,Maestro,Editor")]
    [Authorize]
    public class DiseniosProyController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: DiseniosProy
        public ActionResult Index(int code = 0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            return View(db.tblDiseniosProy.ToList());
        }

        // GET: DiseniosProy/Details/5
        public ActionResult Details(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDiseniosProy tblDiseniosProy = db.tblDiseniosProy.Find(id);
            if (tblDiseniosProy == null)
            {
                return HttpNotFound();
            }
            return View(tblDiseniosProy);
        }

        // GET: DiseniosProy/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View();
        }

        // POST: DiseniosProy/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tblDiseniosProy_ID,disProy_nombre")] tblDiseniosProy tblDiseniosProy)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblDiseniosProy variablep = db.tblDiseniosProy.SingleOrDefault(model => model.disProy_nombre == tblDiseniosProy.disProy_nombre);
                if (variablep == null)
                {
                    db.tblDiseniosProy.Add(tblDiseniosProy);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }

            return View(tblDiseniosProy);
        }

        // GET: DiseniosProy/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDiseniosProy tblDiseniosProy = db.tblDiseniosProy.Find(id);
            if (tblDiseniosProy == null)
            {
                return HttpNotFound();
            }
            return View(tblDiseniosProy);
        }

        // POST: DiseniosProy/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tblDiseniosProy_ID,disProy_nombre")] tblDiseniosProy tblDiseniosProy)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblDiseniosProy variablep = db.tblDiseniosProy.SingleOrDefault(model => model.disProy_nombre == tblDiseniosProy.disProy_nombre && model.tblDiseniosProy_ID != tblDiseniosProy.tblDiseniosProy_ID);
                if (variablep == null)
                {
                    db.Entry(tblDiseniosProy).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }
            return View(tblDiseniosProy);
        }

        // GET: DiseniosProy/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDiseniosProy tblDiseniosProy = db.tblDiseniosProy.Find(id);
            if (tblDiseniosProy == null)
            {
                return HttpNotFound();
            }
            return View(tblDiseniosProy);
        }

        // POST: DiseniosProy/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            try
            {
                tblDiseniosProy tblDiseniosProy = db.tblDiseniosProy.Find(id);
                db.tblDiseniosProy.Remove(tblDiseniosProy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 1390305 });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
