﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using dl = ClassLibrary;

namespace INI.Controllers
{
    //[Authorize(Roles = "Administrator,Maestro,Estudiante")]
    [Authorize]
    public class HerramientasRecoleccionInformacionController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: HerramientasRecoleccionInformacion
        public ActionResult Index(int code=0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            return View(db.tblHerramientasRecoleccionInformacion.ToList());
        }

        // GET: HerramientasRecoleccionInformacion/Details/5
        public ActionResult Details(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHerramientasRecoleccionInformacion tblHerramientasRecoleccionInformacion = db.tblHerramientasRecoleccionInformacion.Find(id);
            if (tblHerramientasRecoleccionInformacion == null)
            {
                return HttpNotFound();
            }
            return View(tblHerramientasRecoleccionInformacion);
        }

        // GET: HerramientasRecoleccionInformacion/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View();
        }

        // POST: HerramientasRecoleccionInformacion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,HerramientaRecoleccion,Descripcion")] tblHerramientasRecoleccionInformacion tblHerramientasRecoleccionInformacion)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblHerramientasRecoleccionInformacion variablep = db.tblHerramientasRecoleccionInformacion.SingleOrDefault(model => model.HerramientaRecoleccion == tblHerramientasRecoleccionInformacion.HerramientaRecoleccion);
                if (variablep == null)
                {
                    db.tblHerramientasRecoleccionInformacion.Add(tblHerramientasRecoleccionInformacion);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }

            return View(tblHerramientasRecoleccionInformacion);
        }

        // GET: HerramientasRecoleccionInformacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHerramientasRecoleccionInformacion tblHerramientasRecoleccionInformacion = db.tblHerramientasRecoleccionInformacion.Find(id);
            if (tblHerramientasRecoleccionInformacion == null)
            {
                return HttpNotFound();
            }
            return View(tblHerramientasRecoleccionInformacion);
        }

        // POST: HerramientasRecoleccionInformacion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,HerramientaRecoleccion,Descripcion")] tblHerramientasRecoleccionInformacion tblHerramientasRecoleccionInformacion)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblHerramientasRecoleccionInformacion variablep = db.tblHerramientasRecoleccionInformacion.SingleOrDefault(model => model.HerramientaRecoleccion == tblHerramientasRecoleccionInformacion.HerramientaRecoleccion && model.id != tblHerramientasRecoleccionInformacion.id);
                if (variablep == null)
                {
                    db.Entry(tblHerramientasRecoleccionInformacion).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }
            return View(tblHerramientasRecoleccionInformacion);
        }

        // GET: HerramientasRecoleccionInformacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHerramientasRecoleccionInformacion tblHerramientasRecoleccionInformacion = db.tblHerramientasRecoleccionInformacion.Find(id);
            if (tblHerramientasRecoleccionInformacion == null)
            {
                return HttpNotFound();
            }
            return View(tblHerramientasRecoleccionInformacion);
        }

        // POST: HerramientasRecoleccionInformacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            try
            {
                tblHerramientasRecoleccionInformacion tblHerramientasRecoleccionInformacion = db.tblHerramientasRecoleccionInformacion.Find(id);
                db.tblHerramientasRecoleccionInformacion.Remove(tblHerramientasRecoleccionInformacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 1390307 });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
