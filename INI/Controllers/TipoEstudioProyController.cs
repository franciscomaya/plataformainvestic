﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using dl = ClassLibrary;

namespace INI.Controllers
{
    //[Authorize(Roles = "Administrator,Maestro")]
    [Authorize]
    public class TipoEstudioProyController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: TipoEstudioProy
        public ActionResult Index(int code=0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            return View(db.tblTipoEstudioProy.ToList());
        }

        // GET: TipoEstudioProy/Details/5
        public ActionResult Details(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTipoEstudioProy tblTipoEstudioProy = db.tblTipoEstudioProy.Find(id);
            if (tblTipoEstudioProy == null)
            {
                return HttpNotFound();
            }
            return View(tblTipoEstudioProy);
        }

        // GET: TipoEstudioProy/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View();
        }

        // POST: TipoEstudioProy/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tblTipoEstudioProy_ID,tipEst_nombre")] tblTipoEstudioProy tblTipoEstudioProy)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblTipoEstudioProy variablep = db.tblTipoEstudioProy.SingleOrDefault(model => model.tipEst_nombre == tblTipoEstudioProy.tipEst_nombre && model.tblTipoEstudioProy_ID != tblTipoEstudioProy.tblTipoEstudioProy_ID);
                if (variablep == null)
                {
                    db.tblTipoEstudioProy.Add(tblTipoEstudioProy);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else{
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }

            return View(tblTipoEstudioProy);
        }

        // GET: TipoEstudioProy/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTipoEstudioProy tblTipoEstudioProy = db.tblTipoEstudioProy.Find(id);
            if (tblTipoEstudioProy == null)
            {
                return HttpNotFound();
            }
            return View(tblTipoEstudioProy);
        }

        // POST: TipoEstudioProy/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tblTipoEstudioProy_ID,tipEst_nombre")] tblTipoEstudioProy tblTipoEstudioProy)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblTipoEstudioProy variablep = db.tblTipoEstudioProy.SingleOrDefault(model => model.tipEst_nombre == tblTipoEstudioProy.tipEst_nombre);
                if (variablep == null)
                {
                    db.Entry(tblTipoEstudioProy).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }
            return View(tblTipoEstudioProy);
        }

        // GET: TipoEstudioProy/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTipoEstudioProy tblTipoEstudioProy = db.tblTipoEstudioProy.Find(id);
            if (tblTipoEstudioProy == null)
            {
                return HttpNotFound();
            }
            return View(tblTipoEstudioProy);
        }

        // POST: TipoEstudioProy/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            try
            { 
                tblTipoEstudioProy tblTipoEstudioProy = db.tblTipoEstudioProy.Find(id);
                db.tblTipoEstudioProy.Remove(tblTipoEstudioProy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 1390303 });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
