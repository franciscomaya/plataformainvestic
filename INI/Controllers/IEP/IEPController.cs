﻿using INI.Models;
using INI.Models.DataBase;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using dl = ClassLibrary;
using System.Xml;
using System.Data.Entity.Infrastructure;
namespace INI.Controllers.IEP
{
    [Authorize]
    public class IEPController : Controller
    {
        investicEntities db = new investicEntities();
        string retorno = "";
        string respuesta = "";
        string falla_xml = "false";
         /// <summary>
        /// Constructor heredado 
        /// </summary>
        public IEPController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        /// <summary>
        /// Constructor con parametro
        /// </summary>
        /// <param name="userManager">UserManager</param>
        public IEPController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        /// <summary>
        /// Propiedad pública UserManager para el registro de usuarios
        /// </summary>
        public UserManager<ApplicationUser> UserManager { get; private set; }

        // GET: IEP
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult Index(int code = 0)
        {
            if (AspNetUsersRoles.IsUserInRole("Estudiante", User.Identity.Name))
            {
                return RedirectToAction("Estudiante");
            }            
            
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            if(!tblMaestroCoInvestigador.Exist(idUsuario))
            {
                return RedirectToAction("CrearMaestro");
            }

            List<MisGrupos> misGrupos = new List<MisGrupos>();

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            var listaGrupos = db.tblGrupoInvestigacion.Where(g => g.idUsuario.Equals(idUsuario));

            foreach (var item in listaGrupos)
            {
                MisGrupos m = new MisGrupos();
                InformacionGrupo i = new InformacionGrupo();
                i.Descripcion = "No disponible";
                if (item.tblProblemaInvestigacion.FirstOrDefault() != null)
                {
                    i.Descripcion = item.tblProblemaInvestigacion.First().Descripcion;
                }
                i.Institucion = item.tblInstitucion.Nombre;
                i.Municipio = item.tblInstitucion.tblMunicipios.NombreMunicipio;
                i.NombreGrupo = item.Nombre;
                i.Pregunta = "Pregunta no disponible";
                if (item.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).FirstOrDefault() != null)
                {
                    i.Pregunta = item.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }                
                m.Informacion = i;
                m.Avatar = item.Avatar;
                m.id = item.id;
                misGrupos.Add(m);
            }

            var invitaciones = db.tblInvitacionGrupo.Where(m => m.idUsuario == idUsuario)
                .Where(m => m.Aceptada)
                .Include(m => m.tblGrupoInvestigacion)
                .Distinct();

            foreach (var item in invitaciones)
            {
                MisGrupos m = new MisGrupos();
                InformacionGrupo i = new InformacionGrupo();                
                i.Descripcion = "No disponible";
                if (item.tblGrupoInvestigacion.tblProblemaInvestigacion.First() != null)
                {
                    i.Descripcion = item.tblGrupoInvestigacion.tblProblemaInvestigacion.First().Descripcion;
                }
                i.Institucion = item.tblGrupoInvestigacion.tblInstitucion.Nombre;
                i.Municipio = item.tblGrupoInvestigacion.tblInstitucion.tblMunicipios.NombreMunicipio;
                i.NombreGrupo = item.tblGrupoInvestigacion.Nombre;
                i.Pregunta = "Pregunta no disponible";
                if (item.tblGrupoInvestigacion.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
                {
                    i.Pregunta = item.tblGrupoInvestigacion.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }                
                m.Informacion = i;
                m.Avatar = item.tblGrupoInvestigacion.Avatar;
                m.id = item.tblGrupoInvestigacion.id;
                misGrupos.Add(m);
            }

            return View(misGrupos);
        }

        public ActionResult Estudiante(int code = 0)
        {
            List<MisGrupos> misGrupos = new List<MisGrupos>();
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            var listaGrupos = db.tblGrupoInvestigacion.Where(g => g.idUsuario.Equals(idUsuario));

            foreach (var item in listaGrupos)
            {
                MisGrupos m = new MisGrupos();
                InformacionGrupo i = new InformacionGrupo();
                i.Descripcion = "No disponible";
                if (item.tblProblemaInvestigacion.FirstOrDefault() != null)
                {
                    i.Descripcion = item.tblProblemaInvestigacion.First().Descripcion;
                }
                i.Institucion = item.tblInstitucion.Nombre;
                i.Municipio = item.tblInstitucion.tblMunicipios.NombreMunicipio;
                i.NombreGrupo = item.Nombre;
                i.Pregunta = "Pregunta no disponible";
                if (item.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
                {
                    i.Pregunta = item.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }
                m.Informacion = i;
                m.Avatar = item.Avatar;
                m.id = item.id;
                misGrupos.Add(m);
            }

            var invitaciones = db.tblInvitacionGrupo.Where(m => m.idUsuario == idUsuario)
                .Where(m => m.Aceptada)
                .Include(m => m.tblGrupoInvestigacion);

            foreach (var item in invitaciones)
            {
                MisGrupos m = new MisGrupos();
                InformacionGrupo i = new InformacionGrupo();
                i.Descripcion = "No disponible";
                if (item.tblGrupoInvestigacion.tblProblemaInvestigacion.First() != null)
                {
                    i.Descripcion = item.tblGrupoInvestigacion.tblProblemaInvestigacion.First().Descripcion;
                }
                i.Institucion = item.tblGrupoInvestigacion.tblInstitucion.Nombre;
                i.Municipio = item.tblGrupoInvestigacion.tblInstitucion.tblMunicipios.NombreMunicipio;
                i.NombreGrupo = item.tblGrupoInvestigacion.Nombre;
                i.Pregunta = "Pregunta no disponible";
                if (item.tblGrupoInvestigacion.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
                {
                    i.Pregunta = item.tblGrupoInvestigacion.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }
                m.Informacion = i;
                m.Avatar = item.tblGrupoInvestigacion.Avatar;
                m.id = item.tblGrupoInvestigacion.id;
                misGrupos.Add(m);
            }

            return View(misGrupos);
        }

        #region Hoja de Vida Maestro Coinvestigador
        public ActionResult HojaDeVida(int code = 0)
        {
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            tblMaestroCoInvestigador maestro = new tblMaestroCoInvestigador();
            ViewBag.idAreaConocimiento = new SelectList(db.tblAreaConocimiento, "id", "Nombre");
            maestro.idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            return View(maestro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HojaDeVida([Bind(Include = "id,idInstitucion,idUsuario,TiempoOndas,Pregrado,Postgrado,Otro,idAreaConocimiento,ExperienciaAreaConocimiento")] tblMaestroCoInvestigador tblmaestrocoinvvestigador)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.tblMaestroCoInvestigador.Add(tblmaestrocoinvvestigador);
                    db.SaveChanges();
                    return RedirectToAction("Index", new { code = 10 });
                }
                ViewBag.idAreaConocimiento = new SelectList(db.tblAreaConocimiento, "id", "Nombre");
                return View("HojaDeVida", tblmaestrocoinvvestigador);
            }
            catch (Exception)
            {
                return RedirectToAction("HojaDeVida", new { code = 20 });
            }
        }

        public ActionResult CrearMaestro()
        {
            tblMaestroCoInvestigador maestro = new tblMaestroCoInvestigador();
            ViewBag.idAreaConocimiento = new SelectList(db.tblAreaConocimiento, "id", "Nombre");
            maestro.idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            return View(maestro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearMaestro([Bind(Include = "id,idInstitucion,idUsuario,TiempoOndas,Pregrado,Postgrado,Otro,idAreaConocimiento,ExperienciaAreaConocimiento,Institution")] tblMaestroCoInvestigador tblmaestrocoinvvestigador)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //tblmaestrocoinvvestigador.Institution = invitacion.idRol;
                    db.tblMaestroCoInvestigador.Add(tblmaestrocoinvvestigador);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ;
                }
            }
            ViewBag.idAreaConocimiento = new SelectList(db.tblAreaConocimiento, "id", "Nombre");
            return View("CrearMaestro", tblmaestrocoinvvestigador);
        }
        #endregion

        #region Registrar y Modificar Grupo de Investigacion
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrarGrupo([Bind(Include = "id,Codigo,Nombre,FechaCreacion,TipoGrupo,Avatar,idInstitucion,idLineaInvestigacion,idUsuario")] tblGrupoInvestigacion tblgrupoinvestigacion)
        {
            /// Datos del grupo de investigación adicionales
            /// que se cargan en tiempo de ejecución no en el modelo
            /// 
            tblgrupoinvestigacion.FechaCreacion = DateTime.Now;
            tblgrupoinvestigacion.Codigo = dl.Codigos.ResearchGroupCode();
            tblgrupoinvestigacion.Nombre = tblgrupoinvestigacion.Nombre.Trim();
            if (ModelState.IsValid)
            {
                // Validación para no permitir duplicidad de nombres en grupos para una misma institución educativa
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE idInstitucion = {0}";
                var gruposeninstitucion = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblgrupoinvestigacion.idInstitucion);

                //var gruposeninstitucion = db.tblGrupoInvestigacion.Where(m => m.idInstitucion == tblgrupoinvestigacion.idInstitucion);
                foreach (var item in gruposeninstitucion)
                {
                    //if (gruposeninstitucion.Count() > 0)
                    //{
                    //    return RedirectToAction("Index", new { alerta = 100 });
                    //}
                    if (item.Nombre.ToUpper().Equals(tblgrupoinvestigacion.Nombre.ToUpper()))
                    {
                        return RedirectToAction("Index", new { code = 105 });
                    }
                }

                try
                {
                    string fileName = "imagen-no-disponible.jpg";
                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        if (hpf.ContentLength == 0)
                            continue;
                        string folderPath = Server.MapPath("~/Images/Avatars/");
                        Directory.CreateDirectory(folderPath);
                        string ext = Path.GetExtension(hpf.FileName);
                        fileName = string.Format("img-{0}{1}", tblgrupoinvestigacion.Codigo, ext);
                        string savedFileName = Server.MapPath("~/Images/Avatars/" + fileName);
                        hpf.SaveAs(savedFileName);
                        tblgrupoinvestigacion.Avatar = fileName;
                    }

                    //--------------------------------
                    //tblPreguntaInvestigacion pregunta = new tblPreguntaInvestigacion();
                    //pregunta.FechaCreacion = System.DateTime.Now;
                    //pregunta.FechaModificacion = System.DateTime.Now;
                    //pregunta.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                    //pregunta.Pregunta = "";
                    //pregunta.PreguntaPrincipal = true;
                    //pregunta.Consecutivo = 0;
                    //db.tblPreguntaInvestigacion.Add(pregunta);
                    //--------------------
                    db.tblGrupoInvestigacion.Add(tblgrupoinvestigacion);
                    db.SaveChanges();

                    /// Creación de la reflexion
                    /// 
                    tblReflexionProyectoInvestigacion tblreflexion = new tblReflexionProyectoInvestigacion();
                    tblreflexion.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                    tblreflexion.FechaInicio = DateTime.Now;
                    tblreflexion.Proceso = "Proceso";
                    tblreflexion.Motivacion = "Motivación";
                    tblreflexion.Reflexion = "Reflexión";
                    tblreflexion.ConceptoAsesor = "Espacio para el Asesor";                    
                    db.tblReflexionProyectoInvestigacion.Add(tblreflexion);
                    db.SaveChanges();


                    /// Creación de la pregunta
                    /// 
                    tblPreguntaInvestigacion tblpregunta;

                    if (tblgrupoinvestigacion.TipoGrupo == 2)// abierto
                    {
                        for (int i = 0; i <= 5; i++)
                        {
                            tblpregunta = new tblPreguntaInvestigacion();
                            tblpregunta.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                            tblpregunta.Consecutivo = i;
                            tblpregunta.FechaCreacion = DateTime.Now;
                            if (i == 0)
                            {
                                tblpregunta.Pregunta = "Escriba la Pregunta seleccionada";
                                tblpregunta.PreguntaPrincipal = true;
                            }
                            else
                            {
                                tblpregunta.Pregunta = string.Format("Escriba la Pregunta {0}.", i.ToString());
                                tblpregunta.PreguntaPrincipal = false;
                            }
                            db.tblPreguntaInvestigacion.Add(tblpregunta);
                            db.SaveChanges();
                        }

                        //tblPreguntaProyectoInvestigacion tblpreguntaproyecto = new tblPreguntaProyectoInvestigacion();
                        //tblpreguntaproyecto.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //tblpreguntaproyecto.Revision = false;
                        //db.tblPreguntaProyectoInvestigacion.Add(tblpreguntaproyecto);
                        //db.SaveChanges();
                        var RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblPreguntaProyectoInvestigacion (idGrupoInvestigacion, Revision) values ({0}, {1})", tblgrupoinvestigacion.id, 0);

                        //tblProblemaInvestigacion tblproblemainvestigacion = new tblProblemaInvestigacion();
                        //tblproblemainvestigacion.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //db.tblProblemaInvestigacion.Add(tblproblemainvestigacion);
                        //db.SaveChanges();
                        var RowsAffected2 = db.Database.ExecuteSqlCommand("INSERT INTO tblProblemaInvestigacion (idGrupoInvestigacion) values ({0})", tblgrupoinvestigacion.id);

                        //tblProblemaProyectoInvestigacion tblproblemaproyectoinvestigacion = new tblProblemaProyectoInvestigacion();
                        //tblproblemaproyectoinvestigacion.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //db.tblProblemaProyectoInvestigacion.Add(tblproblemaproyectoinvestigacion);
                        //db.SaveChanges();
                        var RowsAffected3 = db.Database.ExecuteSqlCommand("INSERT INTO tblProblemaProyectoInvestigacion (idGrupoInvestigacion, Revision) values ({0}, {1})", tblgrupoinvestigacion.id, 0);
                    }
                    else // pre-estructurado
                    {
                        for (int i = 0; i <= 5; i++)
                        {
                            tblpregunta = new tblPreguntaInvestigacion();
                            tblpregunta.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                            tblpregunta.Consecutivo = i;
                            tblpregunta.FechaCreacion = DateTime.Now;
                            if (i == 0)
                            {
                                tblpregunta.Pregunta = "Escriba la Pregunta seleccionada";
                                tblpregunta.PreguntaPrincipal = true;
                            }
                            else
                            {
                                tblpregunta.Pregunta = string.Format("Escriba la Pregunta {0}", i.ToString());
                                tblpregunta.PreguntaPrincipal = false;
                            }
                            db.tblPreguntaInvestigacion.Add(tblpregunta);
                            db.SaveChanges();
                        }

                        //tblPreguntaProyectoInvestigacion tblpreguntaproyecto = new tblPreguntaProyectoInvestigacion();
                        //tblpreguntaproyecto.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //db.tblPreguntaProyectoInvestigacion.Add(tblpreguntaproyecto);
                        //db.SaveChanges();
                        var RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblPreguntaProyectoInvestigacion (idGrupoInvestigacion, Revision) values ({0}, {1})", tblgrupoinvestigacion.id, 0);

                        //tblProblemaInvestigacion tblproblemainvestigacion = new tblProblemaInvestigacion();
                        //tblproblemainvestigacion.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //db.tblProblemaInvestigacion.Add(tblproblemainvestigacion);
                        //db.SaveChanges();
                        var RowsAffected2 = db.Database.ExecuteSqlCommand("INSERT INTO tblProblemaInvestigacion (idGrupoInvestigacion) values ({0})", tblgrupoinvestigacion.id);

                        //tblProblemaProyectoInvestigacion tblproblemaproyectoinvestigacion = new tblProblemaProyectoInvestigacion();
                        //tblproblemaproyectoinvestigacion.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //db.tblProblemaProyectoInvestigacion.Add(tblproblemaproyectoinvestigacion);
                        //db.SaveChanges();
                        var RowsAffected3 = db.Database.ExecuteSqlCommand("INSERT INTO tblProblemaProyectoInvestigacion (idGrupoInvestigacion, Revision) values ({0}, {1})", tblgrupoinvestigacion.id, 0);

                        //tblDocumentosSoporte tbldocumentos = new tblDocumentosSoporte();
                        //tbldocumentos.idGrupoInvestigacion = tblgrupoinvestigacion.id;
                        //db.tblDocumentosSoporte.Add(tbldocumentos);
                        //db.SaveChanges();
                        var RowsAffected4 = db.Database.ExecuteSqlCommand("INSERT INTO tblDocumentosSoporte (idGrupoInvestigacion) values ({0})", tblgrupoinvestigacion.id);
                    }

                    var RowsAffectedMiembro = db.Database.ExecuteSqlCommand("INSERT INTO tblMiembroGrupo (idGrupoInvestigacion, idUsuario, idRol, Grado) values ({0}, {1}, {2}, {3})", tblgrupoinvestigacion.id, tblgrupoinvestigacion.idUsuario, 1, 0);
                    /// Grupo creado satisfactoriamente
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.idLineaInvestigacion = new SelectList(db.tblLineaInvestigacion, "id", "Nombre");
            tblgrupoinvestigacion.idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            //return View(tblgrupoinvestigacion);

            return RedirectToAction("Index");
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult ModificarGrupo([Bind(Include = "id,Codigo,Nombre,FechaCreacion,TipoGrupo,Avatar,idInstitucion,idLineaInvestigacion,idUsuario")] tblGrupoInvestigacion tblgrupoinvestigacion)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    tblgrupoinvestigacion.Nombre = tblgrupoinvestigacion.Nombre.Trim();

                    // Validación para no permitir duplicidad de nombres en grupos para una misma institución educativa
                    string query = "SELECT * FROM tblGrupoInvestigacion WHERE idInstitucion = {0} AND id != {1}";
                    var gruposeninstitucion = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblgrupoinvestigacion.idInstitucion, tblgrupoinvestigacion.id);

                    //var gruposeninstitucion = db.tblGrupoInvestigacion.Where(m => m.idInstitucion == tblgrupoinvestigacion.idInstitucion);
                    foreach (var item in gruposeninstitucion)
                    {
                        //if (gruposeninstitucion.Count() > 0)
                        //{
                        //    return RedirectToAction("Index", new { alerta = 100 });
                        //}
                        if (item.Nombre.Trim().ToUpper().Equals(tblgrupoinvestigacion.Nombre.Trim().ToUpper()))
                        {
                            return RedirectToAction("Index", new { code = 105 });
                        }
                    }

                    string fileName = tblGrupoInvestigacion.Find(tblgrupoinvestigacion.id).Avatar;
                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        if (hpf.ContentLength == 0)
                            continue;
                        string folderPath = Server.MapPath("~/Images/Avatars/");
                        Directory.CreateDirectory(folderPath);
                        string ext = Path.GetExtension(hpf.FileName);
                        fileName = string.Format("img-{0}{1}", tblgrupoinvestigacion.Codigo, ext);
                        string savedFileName = Server.MapPath("~/Images/Avatars/" + fileName);
                        hpf.SaveAs(savedFileName);
                    }
                    tblgrupoinvestigacion.Avatar = fileName;
                    db.Entry(tblgrupoinvestigacion).State = EntityState.Modified;
                    db.SaveChanges();
                    //return RedirectToAction("Index", "IEP");
                    return Index();
                }
                return RedirectToAction("Index", "IEP");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "IEP");
            }
        }
        #endregion

        #region Invitaciones Grupos de Investigacion
        public ActionResult Invitaciones(int code = 0)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            var invitaciones = db.tblInvitacionGrupo.Where(m => m.idUsuario.Equals(userId))
                .Include(m => m.tblGrupoInvestigacion);

            return View(invitaciones.ToList());
        }

        public ActionResult Aceptar(int id)
        {
            var invitacion = db.tblInvitacionGrupo.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            if(invitacion.idUsuario != idUsuario){
                return RedirectToAction("Invitaciones", new { code = 103 });
            }

            tblMiembroGrupo miembro = new tblMiembroGrupo();
            miembro.Grado = "0";
            miembro.idGrupoInvestigacion = invitacion.idGrupo;
            miembro.idUsuario = invitacion.idUsuario;
            miembro.idRol = invitacion.idRol;
            db.tblMiembroGrupo.Add(miembro);
            db.SaveChanges();

            ViewBag.RowsAffected = db.Database.ExecuteSqlCommand("UPDATE tblInvitacionGrupo SET Aceptada=1 WHERE id={0}", id);
            
            return RedirectToAction("Invitaciones");
        }

        public ActionResult Rechazar(int id)
        {
            var invitacion = db.tblInvitacionGrupo.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            try
            {
                if (invitacion.idUsuario != idUsuario)
                {
                    return RedirectToAction("Invitaciones", new { code = 103 });
                }

                ViewBag.RowsAffected = db.Database.ExecuteSqlCommand("DELETE FROM tblInvitacionGrupo WHERE id={0}", id);

                return RedirectToAction("Invitaciones");
            }
            catch (Exception)
            {
                return RedirectToAction("Invitaciones", new { code = 103 });
            }
        }
        #endregion        

        public ActionResult IrAlProyecto(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }
            // -----------------------------------------------------------------------------

            InformacionGrupo infogrupo = new InformacionGrupo();            
            infogrupo.idGrupo = id;

            List<tblMiembroGrupo> Miembros = db.tblMiembroGrupo.ToList();
            List<AspNetUsers> Usuarios = db.AspNetUsers.ToList();

            //string[] members = { "Jani", "Hege", "Kai", "Jim" };
            List<string> members = new List<string>();
            //members.Add("Hello");

            var variableprueba = from pd in Miembros
                                 join p in Usuarios on pd.idUsuario equals p.Id
                                 where (pd.idGrupoInvestigacion == id) && (pd.idRol == 4)
                                 select new { Nombre = p.Name, Apellido = p.SureName, Correo = p.Email, Telefono = p.PhoneNumber };
            
            variableprueba = variableprueba.ToList();
            foreach (var item in variableprueba)
            {
                members.Add(item.Nombre + "," + item.Apellido + "," + item.Correo + "," + item.Telefono);
            }
            
            if (tblgrupo.tblPreguntaInvestigacion.Count>0)
            {
                infogrupo.Descripcion = tblgrupo.tblPreguntaInvestigacion
                    .Where(m => m.idGrupoInvestigacion == id)
                    .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
                infogrupo.Institucion = tblgrupo.tblInstitucion.Nombre;
                infogrupo.Municipio = tblgrupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = tblgrupo.Nombre;
            }

            if (tblgrupo.idUsuario.Equals(idUsuario))
            {
                infogrupo.EsCreador = true;
                ViewBag.InfoGrupo = infogrupo;
                ViewBag.members = members;
                return View("MiGrupo", tblgrupo);
            }
            else
            {
                // Comprueba si el usuario está asociado de alguna manera al grupo
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }
                // -----------------------------------

                infogrupo.EsCreador = false;
                ViewBag.InfoGrupo = infogrupo;
                ViewBag.members = members;
                return View("Grupo", tblgrupo);
            }            
        }


        [AcceptVerbs(HttpVerbs.Get)]
        [AllowAnonymous]
        public ActionResult OffLineMetodo(int id)
        {
            investicEntities db = new investicEntities();
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            tblPreguntaInvestigacion tblpreg = (from q in db.tblPreguntaInvestigacion where (q.idGrupoInvestigacion == id) && (q.PreguntaPrincipal==true) select q).FirstOrDefault();
            string nombreproyecto = tblpreg.Pregunta;
            string nombrearchivo = tblgrupo.Nombre + " - " + nombreproyecto + ".xml";
            try
            {
                if (!string.IsNullOrEmpty(id.ToString()))
                {

                    //investicEntities db = new investicEntities();
                    var cadenaxml = db.GeneradorConsultaSQLAXML(id.ToString()).ToList();
                    var arvhivo = cadenaxml[0].ToString();
                    //byte[] fileBytes = Encoding.ASCII.GetBytes(arvhivo);
                    byte[] fileBytes = Encoding.UTF8.GetBytes(arvhivo);
                    string fileName = nombrearchivo;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                return Json(new { d_rta = "Archivo generado correctamente." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { d_rta = "Error! al generar el archivo xml:" + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }





        public ActionResult IrABitacoras(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }
            // -----------------------------------------------------------------------------

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            InformacionGrupo infogrupo = new InformacionGrupo();
            infogrupo.idGrupo = id;
            if (tblgrupo.tblPreguntaInvestigacion.Count > 0)
            {
                infogrupo.Descripcion = tblgrupo.tblPreguntaInvestigacion
                    .Where(m => m.idGrupoInvestigacion == id)
                    .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
                infogrupo.Institucion = tblgrupo.tblInstitucion.Nombre;
                infogrupo.Municipio = tblgrupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = tblgrupo.Nombre;
            }

            if (tblgrupo.idUsuario.Equals(idUsuario))
            {
                infogrupo.EsCreador = true;
                ViewBag.InfoGrupo = infogrupo;
                return View("BitacoraCreador", tblgrupo);
            }
            else
            {
                // Comprueba si el usuario está asociado de alguna manera al grupo
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
                // -----------------------------------

                infogrupo.EsCreador = false;
                ViewBag.InfoGrupo = infogrupo;                
                return View("BitacoraColaborador", tblgrupo);
            }            
        }

        #region Foros del proyecto
        public ActionResult ForoProyecto(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            // Consulta todos los temas de determinado grupo de investigacion
            //string queryforos = "SELECT tblForoProyectoInvestigacion.* FROM tblForoProyectoInvestigacion WHERE idGrupoInvestigacion = {0}";
            //DbRawSqlQuery<tblForoProyectoInvestigacion> forosengrupo = db.Database.SqlQuery<tblForoProyectoInvestigacion>(queryforos, id);
            var forosengrupo = db.tblForoProyectoInvestigacion.Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.idGrupoInvestigacion == id);
            ViewBag.forostodos = forosengrupo;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            //var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            var tbforo = db.tblForoProyectoInvestigacion.Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.idForo == null)
                .Include(m => m.tblForoProyectoInvestigacion1);
            ViewBag.idGrupoinvestigacion = id;
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            infogrupo.EsCreador = false;
            if (tblGrupoInvestigacion.Find(id).idUsuario.Equals(idUsuario))
            {
                infogrupo.EsCreador = true;
            }
            ViewBag.InfoGrupo = infogrupo;

            return View(tbforo.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarForo([Bind(Include = "id,idUser,idGrupoInvestigacion,Titulo,Mensaje,Fecha,Respuestas,idForo,FechaUltimaRespuesta")] tblForoProyectoInvestigacion tblforo)
        {
            tblforo.Fecha = DateTime.Now;
            
            int id = tblforo.idGrupoInvestigacion;
            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;

            if (ModelState.IsValid)
            {
                // Validación para permitir no duplicidad de títuos y mensaje del foro en determinado grupo
                string query = "SELECT * FROM tblForoProyectoInvestigacion WHERE idGrupoInvestigacion = {0} AND Titulo = {1} AND Mensaje =  {2}";
                var fororepetido = db.Database.SqlQuery<tblForoProyectoInvestigacion>(query, tblforo.idGrupoInvestigacion, tblforo.Titulo, tblforo.Mensaje);

                //if (idUsuario != dueno.FirstOrDefault().idGrupoInvestigacion)
                if (fororepetido.Count() > 0)
                {
                    return RedirectToAction("ForoProyecto", new { id = tblforo.idGrupoInvestigacion, code = 511 });
                }

                db.tblForoProyectoInvestigacion.Add(tblforo);
                db.SaveChanges();
                if (tblforo.idForo.HasValue)
                {
                    tblForoProyectoInvestigacion tblForoPrincipal = db.tblForoProyectoInvestigacion.Find(tblforo.idForo.Value);
                    int respuestas = db.tblForoProyectoInvestigacion.Where(m => m.idForo == tblforo.idForo.Value).Count();
                    tblForoPrincipal.Respuestas = respuestas;
                    db.Entry(tblForoPrincipal).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("ForoProyecto", new { id = tblforo.idGrupoInvestigacion });
            }
            return RedirectToAction("ForoProyecto", new { id = tblforo.idGrupoInvestigacion });
        }
        #endregion

        #region Comentarios del Proyecto
        public ActionResult ComentarioProyecto(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            var tbcomentario = db.tblComentarioGrupo.Where(m => m.idGrupo == id)
                .Include(m => m.tblGrupoInvestigacion);
            ViewBag.idGrupoinvestigacion = id;
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;

            return View(tbcomentario.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarComentario([Bind(Include = "id,idGrupo,UserId,Comentario,Aprobado")] tblComentarioGrupo tblcomentario)
        {
            if (ModelState.IsValid)
            {
                db.tblComentarioGrupo.Add(tblcomentario);
                db.SaveChanges();
                return RedirectToAction("ComentarioProyecto", new { id = tblcomentario.idGrupo });
            }
            return RedirectToAction("ComentarioProyecto", new { id = tblcomentario.idGrupo });
        }

        public ActionResult Comentarios(int id)
        {
            var listaComentarios = db.tblComentarioGrupo.Where(m => m.idGrupo == id).Where(m => m.Aprobado.Value);
            return PartialView(listaComentarios.ToList());
        }

        public ActionResult AprobarComentario(int id)
        {
            tblComentarioGrupo tblcomentario = db.tblComentarioGrupo.Find(id);
            tblcomentario.Aprobado = true;
            db.Entry(tblcomentario).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("ComentarioProyecto", new { id = tblcomentario.idGrupo });
        }

        public ActionResult EliminarComentario(int id)
        {
            try {
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblComentarioGrupo tblcomentario = db.tblComentarioGrupo.Where(m => m.id == id).SingleOrDefault();

                if(idUsuario != tblcomentario.UserId){
                    return RedirectToAction("ComentarioProyecto", new { id = tblcomentario.idGrupo, code = 999 });
                }

                db.tblComentarioGrupo.Remove(tblcomentario);
                db.SaveChanges();
                return RedirectToAction("ComentarioProyecto", new { id = tblcomentario.idGrupo });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }
        #endregion

        public ActionResult EditarFeria(int id)
        {
            try
            {
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblPropagacionGrupo tblrec = db.tblPropagacionGrupo.Where(m => m.id == id).SingleOrDefault();

                // Validación para permitir solo al dueño eliminar conceptos
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblrec.idGrupoInvestigacion);

                if (idUsuario != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }

                tblGrupoInvestigacion grupo = db.tblGrupoInvestigacion.Find(tblrec.idGrupoInvestigacion);

                InformacionGrupo infogrupo = new InformacionGrupo();
                //var grupo = tblGrupoInvestigacion.Find(tblgrupo.id);
                infogrupo.idGrupo = grupo.id;
                infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                    .Where(m => m.idGrupoInvestigacion == grupo.id)
                    .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
                infogrupo.Institucion = grupo.tblInstitucion.Nombre;
                infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = grupo.Nombre;
                infogrupo.EsCreador = true;
                ViewBag.InfoGrupo = infogrupo;

                ViewBag.idTipoFeria = new SelectList(db.tblTipoFeria, "id", "TipoFeria");

                return View(tblrec);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EditarFeriaAjax(int id)
        {
            tblPropagacionGrupo model = db.tblPropagacionGrupo.Find(id);

            ViewBag.idTipoFeria = new SelectList(db.tblTipoFeria, "id", "TipoFeria");
            
            return PartialView("_EditarFeria", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarFeria([Bind(Include = "id,idGrupoInvestigacion,idTipoFeria,Archivo,Descripcion")] tblPropagacionGrupo tblrec)
        {
            try
            {
                string userId = AspNetUsers.GetUserId(User.Identity.Name);

                // Validación para permitir solo al dueño eliminar conceptos
                string query2 = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query2, tblrec.idGrupoInvestigacion);

                if (userId != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }

                int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tblrec.idGrupoInvestigacion);
                if (idRol != 1)
                {
                    return RedirectToAction("Index", new { code = 999, id = tblrec.idGrupoInvestigacion });
                }

                if (ModelState.IsValid)
                {
                    if(tblrec.Archivo!=null){
                        foreach (string file in Request.Files)
                        {
                            var grupo = db.tblGrupoInvestigacion.Find(tblrec.idGrupoInvestigacion);

                            HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                            if (hpf.ContentLength == 0)
                                continue;
                            string folderPath = string.Format("{0}{1}/", Server.MapPath("~/Upload/"), grupo.Nombre.Replace(" ", ""));
                            Directory.CreateDirectory(folderPath);
                            string ext = Path.GetExtension(hpf.FileName);

                            string fileName = "fer" + grupo.id + Convert.ToString(DateTime.Now.Ticks);

                            string savedFileName = Server.MapPath("~/Upload/" + grupo.Nombre.Replace(" ", "") + "/" + fileName + ext);
                            hpf.SaveAs(savedFileName);
                            tblrec.Archivo = string.Format("../../Upload/{0}/{1}", grupo.Nombre.Replace(" ", ""), fileName + ext);
                        }
                    }
                    else
                    {
                        string querye = "SELECT * FROM tblPropagacionGrupo WHERE id = {0}";
                        var evidenciae = db.Database.SqlQuery<tblPropagacionGrupo>(querye, tblrec.id).FirstOrDefault().Archivo;
                        tblrec.Archivo = evidenciae;
                    }

                    db.Entry(tblrec).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("IrABitacoras", new { id = tblrec.idGrupoInvestigacion, code = 606, posEle = 6 });
                }

                return View(tblrec);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EliminarFeria(int id)
        {
            try
            {
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblPropagacionGrupo tblpropagacion = db.tblPropagacionGrupo.Where(m => m.id == id).SingleOrDefault();

                // Validación para permitir solo al dueño eliminar ferias
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblpropagacion.idGrupoInvestigacion);

                if (idUsuario != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("IrABitacoras", new { id = tblpropagacion.idGrupoInvestigacion, code = 999 });
                }

                db.tblPropagacionGrupo.Remove(tblpropagacion);
                db.SaveChanges();
                return RedirectToAction("IrABitacoras", new { id = tblpropagacion.idGrupoInvestigacion, posEle = 6 });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EditarConcepto(int id)
        {
            try { 
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblConceptosEstadoArte tblconceptos = db.tblConceptosEstadoArte.Where(m => m.id == id).SingleOrDefault();

                // Validación para permitir solo al dueño eliminar conceptos
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblconceptos.tblEstadoArteProyectoInvestigacion.idGrupoInvestigacion);

                if (idUsuario != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }

                tblConceptosEstadoArte tblconcepto = db.tblConceptosEstadoArte.Find(id);
                //tblconcepto.idGrupoInvestigacion = idGrupoInvestigacion;

                tblGrupoInvestigacion grupo = db.tblGrupoInvestigacion.Find(tblconcepto.tblEstadoArteProyectoInvestigacion.idGrupoInvestigacion);

                InformacionGrupo infogrupo = new InformacionGrupo();
                //var grupo = tblGrupoInvestigacion.Find(tblgrupo.id);
                infogrupo.idGrupo = grupo.id;
                infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                    .Where(m => m.idGrupoInvestigacion == grupo.id)
                    .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
                infogrupo.Institucion = grupo.tblInstitucion.Nombre;
                infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = grupo.Nombre;
                infogrupo.EsCreador = true;
                ViewBag.InfoGrupo = infogrupo;

                return View(tblconcepto);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EditarConceptoAjax(int id)
        {
            tblConceptosEstadoArte model = db.tblConceptosEstadoArte.Find(id);

            ViewBag.idInstrumento = new SelectList(db.tblHerramientasRecoleccionInformacion, "id", "HerramientaRecoleccion");

            return PartialView("_EditarConcepto", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarConcepto([Bind(Include = "id,idEstadoArte,Autor,Publicacion,Texto")] tblConceptosEstadoArte tbconceptos)
        {
            try
            { 
                string userId = AspNetUsers.GetUserId(User.Identity.Name);

                string query = "SELECT * FROM tblEstadoArteProyectoInvestigacion WHERE id = {0}";
                var grupo = db.Database.SqlQuery<tblEstadoArteProyectoInvestigacion>(query, tbconceptos.idEstadoArte).FirstOrDefault();

                // Validación para permitir solo al dueño eliminar conceptos
                string query2 = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query2, grupo.idGrupoInvestigacion);

                if (userId != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }

                int idRol = tblMiembroGrupo.GetRoleMiembro(userId, grupo.idGrupoInvestigacion);
                if (idRol != 1)
                {
                    return RedirectToAction("Index", new { code = 999, id = grupo.idGrupoInvestigacion });
                }

                if (ModelState.IsValid)
                {
                    db.Entry(tbconceptos).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("IrABitacoras", new { id = grupo.idGrupoInvestigacion, code = 604, posEle = 4 });
                }

                return View(tbconceptos);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EditarRecoleccionInformacion(int id)
        {
            try
            {
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblRecoleccionInformacionProyectoInvestigacion tblrec = db.tblRecoleccionInformacionProyectoInvestigacion.Where(m => m.id == id).SingleOrDefault();

                // Validación para permitir solo al dueño eliminar conceptos
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblrec.idGrupoInvestigacion);

                if (idUsuario != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }

                tblGrupoInvestigacion grupo = db.tblGrupoInvestigacion.Find(tblrec.idGrupoInvestigacion);

                InformacionGrupo infogrupo = new InformacionGrupo();
                //var grupo = tblGrupoInvestigacion.Find(tblgrupo.id);
                infogrupo.idGrupo = grupo.id;
                infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                    .Where(m => m.idGrupoInvestigacion == grupo.id)
                    .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
                infogrupo.Institucion = grupo.tblInstitucion.Nombre;
                infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = grupo.Nombre;
                infogrupo.EsCreador = true;
                ViewBag.InfoGrupo = infogrupo;

                ViewBag.idInstrumento = new SelectList(db.tblHerramientasRecoleccionInformacion, "id", "HerramientaRecoleccion");

                return View(tblrec);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EditarRecoleccionInformacionAjax(int id)
        {
            tblRecoleccionInformacionProyectoInvestigacion model = db.tblRecoleccionInformacionProyectoInvestigacion.Find(id);

            ViewBag.idInstrumento = new SelectList(db.tblHerramientasRecoleccionInformacion, "id", "HerramientaRecoleccion");

            return PartialView("_EditarRecoleccionInformacion", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarRecoleccionInformacion([Bind(Include = "id,idGrupoInvestigacion,idInstrumento,Evidencia,Descripcion")] tblRecoleccionInformacionProyectoInvestigacion tblrec)
        {
            try
            {
                string userId = AspNetUsers.GetUserId(User.Identity.Name);

                // Validación para permitir solo al dueño eliminar conceptos
                string query2 = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query2, tblrec.idGrupoInvestigacion);

                if (userId != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("Index", new { code = 999 });
                }

                int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tblrec.idGrupoInvestigacion);
                if (idRol != 1)
                {
                    return RedirectToAction("Index", new { code = 999, id = tblrec.idGrupoInvestigacion });
                }

                if (ModelState.IsValid)
                {
                    if (tblrec.Evidencia != null) { 
                        foreach (string file in Request.Files) {
                            var grupo = db.tblGrupoInvestigacion.Find(tblrec.idGrupoInvestigacion);
                        
                            HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                            if (hpf.ContentLength == 0)
                                continue;
                            string folderPath = string.Format("{0}", Server.MapPath("~/Upload/"));
                            Directory.CreateDirectory(folderPath);
                            string ext = Path.GetExtension(hpf.FileName);
                        
                            string fileName = "recInf" + grupo.id + Convert.ToString(DateTime.Now.Ticks);
                        
                            string savedFileName = Server.MapPath("~/Upload/" + fileName + ext);
                            hpf.SaveAs(savedFileName);
                            tblrec.Evidencia = string.Format("{0}", fileName + ext);
                        }
                    }
                    else
                    {
                        string querye = "SELECT * FROM tblRecoleccionInformacionProyectoInvestigacion WHERE id = {0}";
                        var evidenciae = db.Database.SqlQuery<tblRecoleccionInformacionProyectoInvestigacion>(querye, tblrec.id).FirstOrDefault().Evidencia;
                        tblrec.Evidencia = evidenciae;
                    }

                    db.Entry(tblrec).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("IrABitacoras", new { id = tblrec.idGrupoInvestigacion, code = 605, posEle = 4 });
                }

                return View(tblrec);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EliminarConcepto(int id)
        {
            try
            {
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblConceptosEstadoArte tblconceptos = db.tblConceptosEstadoArte.Where(m => m.id == id).SingleOrDefault();

                // Validación para permitir solo al dueño eliminar conceptos
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblconceptos.tblEstadoArteProyectoInvestigacion.idGrupoInvestigacion);

                if (idUsuario != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("IrABitacoras", new { id = tblconceptos.tblEstadoArteProyectoInvestigacion.idGrupoInvestigacion, code = 999 });
                }

                string query2 = "DELETE FROM tblConceptosEstadoArte WHERE id = {0}";
                ViewBag.RowsAffected = db.Database.ExecuteSqlCommand(query2, id);
                //db.tblConceptosEstadoArte.Remove(tblconceptos);
                //db.SaveChanges();
                return RedirectToAction("IrABitacoras", new { id = tblconceptos.tblEstadoArteProyectoInvestigacion.idGrupoInvestigacion, posEle = 4 });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        public ActionResult EliminarRecoleccionInformacion(int id)
        {
            try
            {
                var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
                tblRecoleccionInformacionProyectoInvestigacion tblrecinformacion = db.tblRecoleccionInformacionProyectoInvestigacion.Where(m => m.id == id).SingleOrDefault();

                // Validación para permitir solo al dueño eliminar ferias
                string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
                var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblrecinformacion.idGrupoInvestigacion);

                if (idUsuario != dueno.FirstOrDefault().idUsuario)
                {
                    return RedirectToAction("IrABitacoras", new { id = tblrecinformacion.idGrupoInvestigacion, code = 999 });
                }

                db.tblRecoleccionInformacionProyectoInvestigacion.Remove(tblrecinformacion);
                db.SaveChanges();
                return RedirectToAction("IrABitacoras", new { id = tblrecinformacion.idGrupoInvestigacion, posEle = 4 });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 999 });
            }
        }

        #region Bitacora 1 Estar en la onda
        /// <summary>
        /// Maestros asesores o coinvestigadores
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult MaestroCoinvestigador(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            string userId = AspNetUsers.GetUserId(User.Identity.Name);           

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = "No disponible";
            if (grupo.tblProblemaInvestigacion.First() != null)
            {
                infogrupo.Descripcion = grupo.tblProblemaInvestigacion.First().Descripcion;
            }
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            infogrupo.Pregunta = "Pregunta no disponible";
            if (grupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
            {
                infogrupo.Pregunta = grupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
            }
            infogrupo.EsCreador = grupo.idUsuario.Equals(userId);
            ViewBag.InfoGrupo = infogrupo;

            var tbcoresearchteacher = db.tblMiembroGrupo
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.tblRol.id == 1);
            if (tbcoresearchteacher.ToList().Count == 0)
            {
                if (infogrupo.EsCreador)
                {
                    var tbgroupmember = new tblMiembroGrupo();
                    tbgroupmember.idGrupoInvestigacion = id;
                    tbgroupmember.idUsuario = userId;
                    tbgroupmember.idRol = 1;
                    tbgroupmember.Grado = "0";
                    try
                    {
                        db.tblMiembroGrupo.Add(tbgroupmember);
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("IrABitacoras", new { id = id, code = 160 });
                    }
                }
            }
            else
            {
                tbcoresearchteacher = db.tblMiembroGrupo
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.tblRol.id != 2);
            }           
            return View(tbcoresearchteacher.ToList());
        }

        /// <summary>
        /// Invitar usuario como Asesor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idGroup"></param>
        /// <returns></returns>
        public ActionResult InvitarAsesor(string id, int idGroup)
        {
            try
            {
                var tblinvitacion = new tblInvitacionGrupo();
                tblinvitacion.idGrupo = idGroup;
                tblinvitacion.idUsuario = id;
                tblinvitacion.idRol = 3;
                db.tblInvitacionGrupo.Add(tblinvitacion);
                db.SaveChanges();

                tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGroup);
                InformacionGrupo infogrupo = new InformacionGrupo();
                infogrupo.idGrupo = idGroup;
                infogrupo.Descripcion = "No disponible";
                if (tblgrupo.tblProblemaInvestigacion.First() != null)
                {
                    infogrupo.Descripcion = tblgrupo.tblProblemaInvestigacion.First().Descripcion;
                }
                infogrupo.Institucion = tblgrupo.tblInstitucion.Nombre;
                infogrupo.Municipio = tblgrupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = tblgrupo.Nombre;
                infogrupo.Pregunta = "Pregunta no disponible";
                if (tblgrupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
                {
                    infogrupo.Pregunta = tblgrupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }
                infogrupo.EsCreador = true;

                return RedirectToAction("MaestroCoinvestigador", new { id = idGroup, code = 101 });

            }
            catch (Exception)
            {
                return RedirectToAction("IrABitacoras", new { id = idGroup });
            }
        }

        /// <summary>
        /// Invitar usuarios como Colaborador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idGroup"></param>
        /// <returns></returns>
        public ActionResult InvitarColaborador(string id, int idGroup)
        {
            try
            {
                // Validación para no permitir múltiples invitaciones al mismo usuario
                string query = "SELECT * FROM tblInvitacionGrupo WHERE idGrupo = {0} AND idUsuario = {1}";
                var miembrosinvitados = db.Database.SqlQuery<tblInvitacionGrupo>(query, idGroup, id);

                if (miembrosinvitados.Count() > 0)
                {
                    return RedirectToAction("MiembroGrupo", new { id = idGroup, code = 106 });
                }

                ViewBag.RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblInvitacionGrupo (idUsuario, idGrupo, idRol, Aceptada) values ({0}, {1}, '4', '0')", id, idGroup);

                //var tblinvitacion = new tblInvitacionGrupo();
                //tblinvitacion.idGrupo = idGroup;
                //tblinvitacion.idUsuario = id;
                //tblinvitacion.idRol = 4;
                //db.tblInvitacionGrupo.Add(tblinvitacion);
                //db.SaveChanges();

                tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGroup);
                InformacionGrupo infogrupo = new InformacionGrupo();
                infogrupo.idGrupo = idGroup;
                infogrupo.Descripcion = "No disponible";
                if (tblgrupo.tblProblemaInvestigacion.First() != null)
                {
                    infogrupo.Descripcion = tblgrupo.tblProblemaInvestigacion.First().Descripcion;
                }
                infogrupo.Institucion = tblgrupo.tblInstitucion.Nombre;
                infogrupo.Municipio = tblgrupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = tblgrupo.Nombre;
                infogrupo.Pregunta = "Pregunta no disponible";
                if (tblgrupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
                {
                    infogrupo.Pregunta = tblgrupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }
                infogrupo.EsCreador = true;

                return RedirectToAction("MaestroCoinvestigador", new { id = idGroup, code = 101 });

            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { id = idGroup });
            }
        }

        public ActionResult EliminarMiembroColaborador(int id)
        {
            // Identificador del usuario logueado
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Datos a eliminar
            tblMiembroGrupo tblmiembrogrupo = db.tblMiembroGrupo.Find(id);
            int idGrupo = tblmiembrogrupo.idGrupoInvestigacion;

            var haciadonde = "";
            switch(tblmiembrogrupo.idRol){
                case 1:
                    haciadonde = "MaestroCoinvestigador";
                    break;
                case 2:
                    haciadonde = "MiembroGrupo";
                    break;
                case 4:
                    haciadonde = "MaestroCoinvestigador";
                    break;
            }
            try
            {
                // Identificador del creador del grupo
                tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGrupo);
                AspNetUsers creadorGrupo = db.AspNetUsers.Find(tblgrupo.idUsuario);

                // Comparo si el usuario logueado es el creador del grupo para poder eliminar
                if (idUsuario == creadorGrupo.Id)
                {
                    if (tblmiembrogrupo.idRol != 1)
                    {
                        db.tblMiembroGrupo.Remove(tblmiembrogrupo);
                        db.SaveChanges();
                        return RedirectToAction(haciadonde, new { id = idGrupo, code = 13901 });
                    }
                    else
                    {
                        return RedirectToAction(haciadonde, new { id = idGrupo, code = 13902 });
                    }
                }

                return RedirectToAction(haciadonde, new { id = idGrupo, code = 999 });
                
            }
            catch (Exception)
            {
                return RedirectToAction(haciadonde, new { id = idGrupo, code = 999 });
            }
        }

        public ActionResult DocumentosSoporte(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            ViewBag.Id = id;
            var tbsupportdocument = db.tblDocumentosSoporte.Where(m => m.idGrupoInvestigacion == id).SingleOrDefault();

            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            AspNetUsers creadorGrupo = db.AspNetUsers.Find(tblgrupo.idUsuario);
            if (idUsuario == creadorGrupo.Id)
            {
                ViewBag.esCreadorGrupo = true;
            }

            return View(tbsupportdocument);
        }

        public ActionResult UploadFileCommitment(int id)
        {
            var uploadfile = db.tblDocumentosSoporte.Find(id);
            return PartialView("_UploadFile", new UploadFile { Id = uploadfile.id, ResearchGroupId = uploadfile.idGrupoInvestigacion, FileToUpload = string.Empty, Acceptance = false });
        }

        public ActionResult UploadFileAceptance(int id)
        {
            var uploadfile = db.tblDocumentosSoporte.Find(id);
            return PartialView("_UploadFile", new UploadFile { Id = uploadfile.id, ResearchGroupId = uploadfile.idGrupoInvestigacion, FileToUpload = string.Empty, Acceptance = true });
        }

        [HttpPost]
        public ActionResult UploadFile(UploadFile model)
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    var tbsupportdocuments = db.tblDocumentosSoporte.Find(model.Id);

                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = Server.MapPath("~/Upload/");
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);
                    string fileName = "";

                    if (model.Acceptance)
                    {
                        fileName = dl.Codigos.Acceptance(db.tblGrupoInvestigacion.Find(model.ResearchGroupId).Nombre, ext);
                        tbsupportdocuments.CartaAceptacion = fileName;
                    }
                    else
                    {
                        fileName = dl.Codigos.Commitment(db.tblGrupoInvestigacion.Find(model.ResearchGroupId).Nombre, ext);
                        tbsupportdocuments.CartaCompromiso = fileName;
                    }
                    string savedFileName = Server.MapPath("~/Upload/" + fileName);
                    hpf.SaveAs(savedFileName);
                    db.Entry(tbsupportdocuments).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 135 });
            }
            
            return RedirectToAction("Index", new { code = 130 });
        }

        /// <summary>
        /// Ingreso de información de la bitacora
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ParaElMaestro(int idGrupo)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGrupo);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, idGrupo);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            int id = idGrupo;
            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            infogrupo.EsCreador = true;
            ViewBag.InfoGrupo = infogrupo;

            AspNetUsers creadorGrupo = db.AspNetUsers.Find(tblgrupo.idUsuario);
            if (idUsuario == creadorGrupo.Id)
            {
                ViewBag.esCreadorGrupo = true;
            }

            //var tbresearchprojectreflexion = db.tblReflexionProyectoInvestigacion.Where(m => m.idGrupoInvestigacion == id).SingleOrDefault();
            return View(grupo.tblReflexionProyectoInvestigacion.OrderByDescending(m=>m.id).First());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ParaElMaestro([Bind(Include = "id,FechaInicio,UltimaModificacion,Proceso,Motivacion,Reflexion,ConceptoAsesor,Revisado,idGrupoInvestigacion")] tblReflexionProyectoInvestigacion tbresearchprojectreflexion)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);

            int id = tbresearchprojectreflexion.idGrupoInvestigacion;
            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);            
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            infogrupo.EsCreador = true;
            ViewBag.InfoGrupo = infogrupo;

            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbresearchprojectreflexion.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("IrABitacoras", new { id = tbresearchprojectreflexion.idGrupoInvestigacion, code = 999 });
            }

            tbresearchprojectreflexion.UltimaModificacion = DateTime.Now;
            
            if (ModelState.IsValid)
            {
                db.Entry(tbresearchprojectreflexion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IrABitacoras", new { id = tbresearchprojectreflexion.idGrupoInvestigacion, code = 110 });
            }

            return View(tbresearchprojectreflexion);
        }


        /// <summary>
        /// Miembros del grupo
        /// </summary>
        /// <returns></returns>
        public ActionResult MiembroGrupo(int id = 0, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            if (id == 0)
            {
                id = tblGrupoInvestigacion.ResearchGroupIdByUser(userId);
            }
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);            
            InformacionGrupo infogrupo = new InformacionGrupo();            
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            infogrupo.EsCreador = grupo.idUsuario.Equals(userId);
            ViewBag.InfoGrupo = infogrupo;
            
            var tbgroupmember = db.tblMiembroGrupo.Where(m => m.idGrupoInvestigacion == id).Where(m => m.idRol == 2);
            ViewBag.idGrupoInvestigacion = id;
            return View(tbgroupmember);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegistrarMiembroGrupo(MiembroGrupo model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    UserName = model.User.PersonalID,
                    Name = model.User.Name,
                    SureName = model.User.SureName,
                    PersonalID = model.User.PersonalID,
                    Genre = model.User.Genre,
                    Email = model.User.Mail,
                    PhoneNumber = model.User.Phone,
                    Address = model.User.Address,
                    BirthDay = model.User.BirthDay
                };
                var result = await UserManager.CreateAsync(user, model.User.Password);
                if (result.Succeeded)
                {
                    model.Informacion.idUsuario = user.Id;
                    db.tblMiembroGrupo.Add(model.Informacion);
                    db.SaveChanges();
                    RedirectToAction("MiembroGrupo");
                }
                else
                {
                    AddErrors(result);
                }
            }
            return RedirectToAction("MiembroGrupo");
        }

        /// <summary>
        /// Invitar usuarios como Colaborador
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idGroup"></param>
        /// <returns></returns>
        public ActionResult InvitarEstudiante(string id, int idGroup)
        {
            try
            {
                // Validación para no permitir múltiples invitaciones al mismo usuario
                string query = "SELECT * FROM tblInvitacionGrupo WHERE idGrupo = {0} AND idUsuario = {1}";
                var miembrosinvitados = db.Database.SqlQuery<tblInvitacionGrupo>(query, idGroup, id);

                if (miembrosinvitados.Count() > 0)
                {
                    return RedirectToAction("MiembroGrupo", new { id = idGroup, code = 106 });
                }

                ViewBag.RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblInvitacionGrupo (idUsuario, idGrupo, idRol, Aceptada) values ({0}, {1}, '2', '0')", id, idGroup);

                //tblInvitacionGrupo tblinvitacion = new tblInvitacionGrupo();
                //tblinvitacion.idGrupo = idGroup;
                //tblinvitacion.idUsuario = id;
                //tblinvitacion.idRol = 2;
                //tblinvitacion.Aceptada = false;
                //db.tblInvitacionGrupo.Add(tblinvitacion);
                //db.SaveChanges();

                tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGroup);
                InformacionGrupo infogrupo = new InformacionGrupo();
                infogrupo.idGrupo = idGroup;
                infogrupo.Descripcion = "No disponible";
                if (tblgrupo.tblProblemaInvestigacion.First() != null)
                {
                    infogrupo.Descripcion = tblgrupo.tblProblemaInvestigacion.First().Descripcion;
                }
                infogrupo.Institucion = tblgrupo.tblInstitucion.Nombre;
                infogrupo.Municipio = tblgrupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = tblgrupo.Nombre;
                infogrupo.Pregunta = "Pregunta no disponible";
                if (tblgrupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First() != null)
                {
                    infogrupo.Pregunta = tblgrupo.tblPreguntaInvestigacion.Where(g => g.PreguntaPrincipal).First().Pregunta;
                }
                infogrupo.EsCreador = true;

                return RedirectToAction("MiembroGrupo", new { id = idGroup, code = 102 });
                //return RedirectToAction("Index", new { id = idGroup, code = id, identificador = idGroup });

            }
            catch (Exception)
            {
                //return View("Error");
                return RedirectToAction("Index", new { id = idGroup });
            }
        }

        /// <summary>
        /// Eliminar Miembro grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteGroupMember(int id)
        {
            tblMiembroGrupo tbgroupmember = db.tblMiembroGrupo.Find(id);
            int idGroup = tbgroupmember.idGrupoInvestigacion;
            db.tblMiembroGrupo.Remove(tbgroupmember);
            db.SaveChanges();
            return RedirectToAction("MiembroGrupo");
        }
        
        #endregion

        #region Bitacora 2 Perturbacion de las Ondas
        /// <summary>
        /// Pregunta para el grupo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Pregunta(int id)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            ViewBag.ResearchId = id;
            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;
            var tbresearchquestion = db.tblPreguntaInvestigacion.Where(m => m.idGrupoInvestigacion == id);
            return View(tbresearchquestion);
        }

        /// <summary>
        /// Modificar pregunta
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditarPreguntaInvestigacion(int id = 0)
        {
            tblPreguntaInvestigacion model = db.tblPreguntaInvestigacion.Find(id);
            return PartialView("_EditarPreguntaInvestigacion", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarPreguntaInvestigacion([Bind(Include = "id,idGrupoInvestigacion,Consecutivo,Pregunta,FechaCreacion,FechaModificacion,PreguntaPrincipal")] tblPreguntaInvestigacion tbresearchquestion)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbresearchquestion.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tbresearchquestion.idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                tbresearchquestion.FechaModificacion = DateTime.Now;
                db.Entry(tbresearchquestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IrABitacoras", new { id = tbresearchquestion.idGrupoInvestigacion, code = 120, posEle = 1 });
            }
            return View(tbresearchquestion);
        }


        /// <summary>
        /// Pregunta para el maestro
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PreguntaParaElMaestro(int idGrupoInvestigacion)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGrupoInvestigacion);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, idGrupoInvestigacion);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            ViewBag.ResearchId = idGrupoInvestigacion;
            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(idGrupoInvestigacion);
            infogrupo.idGrupo = idGrupoInvestigacion;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == idGrupoInvestigacion)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;

            AspNetUsers creadorGrupo = db.AspNetUsers.Find(tblgrupo.idUsuario);
            if (idUsuario == creadorGrupo.Id)
            {
                ViewBag.esCreadorGrupo = true;
            }

            var tbresearchprojectquestion = db.tblPreguntaProyectoInvestigacion.Where(m => m.idGrupoInvestigacion == idGrupoInvestigacion).SingleOrDefault();
            return View(tbresearchprojectquestion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreguntaParaElMaestro([Bind(Include = "id,InformacionUno,FuenteUno,InformacionDos,FuenteDos,InformacionTres,FuenteTres,Reflexion,ConceptoAsesor,Revision,idGrupoInvestigacion")] tblPreguntaProyectoInvestigacion tbresearchprojectquestion)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);

            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbresearchprojectquestion.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tbresearchprojectquestion.idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                db.Entry(tbresearchprojectquestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IrAbitacoras", new { id = tbresearchprojectquestion.idGrupoInvestigacion, code = 120, posEle = 1 });
            }
            return View(tbresearchprojectquestion);
        }

        #endregion

        #region Bitacora 3 Superposición de las ondas
        /// <summary>
        /// El problema
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ElProblema(int idGrupoInvestigacion)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGrupoInvestigacion);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, idGrupoInvestigacion);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(idGrupoInvestigacion);
            infogrupo.idGrupo = idGrupoInvestigacion;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == idGrupoInvestigacion)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;
            var tbresearchproblem = db.tblProblemaInvestigacion.Where(m => m.idGrupoInvestigacion == idGrupoInvestigacion).SingleOrDefault();
            return View(tbresearchproblem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ElProblema([Bind(Include = "id,Descripcion,Justificacion,idGrupoInvestigacion")] tblProblemaInvestigacion tbresearchproblem)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);

            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbresearchproblem.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tbresearchproblem.idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                db.Entry(tbresearchproblem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IrAbitacoras", new { id = tbresearchproblem.idGrupoInvestigacion, code = 120, posEle = 2 });
            }
            return View(tbresearchproblem);
        }

        /// <summary>
        /// Problema de investigación para el profesor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ElProblemaParaElProfesor(int idGrupoInvestigacion)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(idGrupoInvestigacion);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, idGrupoInvestigacion);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            ViewBag.ResearchId = idGrupoInvestigacion;
            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(idGrupoInvestigacion);
            infogrupo.idGrupo = idGrupoInvestigacion;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == idGrupoInvestigacion)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;

            AspNetUsers creadorGrupo = db.AspNetUsers.Find(tblgrupo.idUsuario);
            if (idUsuario == creadorGrupo.Id)
            {
                ViewBag.esCreadorGrupo = true;
            }

            var tbresearchprojectproblem = db.tblProblemaProyectoInvestigacion.Where(m => m.idGrupoInvestigacion == idGrupoInvestigacion).SingleOrDefault();
            return View(tbresearchprojectproblem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ElProblemaParaElProfesor([Bind(Include = "id,Como,Reflexion,ConceptoAsesor,Revision,idGrupoInvestigacion")] tblProblemaProyectoInvestigacion tbresearchprojectproblem)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbresearchprojectproblem.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tbresearchprojectproblem.idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                db.Entry(tbresearchprojectproblem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IrAbitacoras", new { id = tbresearchprojectproblem.idGrupoInvestigacion, code = 120, posEle = 2 }); 
            }
            return View(tbresearchprojectproblem);
        }        

        #endregion

        #region Bitacora 4 Presupuesto
        public ActionResult AgregarPresupuesto(int idGrupoInvestigacion)
        {
            tblPresupuestoProyectoInvestigacion tblpresupuesto = new tblPresupuestoProyectoInvestigacion();
            tblpresupuesto.idGrupoInvestigacion = idGrupoInvestigacion;
            ViewBag.idRubro = new SelectList(db.tblRubro, "id", "Rubro");
            return View(tblpresupuesto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarPresupuesto([Bind(Include = "id,idGrupoinvestigacion,idRubro,DescripcionGasto,ValorRubro,ValorUnitario,Total")] tblPresupuestoProyectoInvestigacion tbpresupuesto)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbpresupuesto.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tbpresupuesto.idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                tbpresupuesto.Total = tbpresupuesto.ValorRubro * tbpresupuesto.ValorUnitario;
                db.tblPresupuestoProyectoInvestigacion.Add(tbpresupuesto);
                db.SaveChanges();
                return RedirectToAction("IrAbitacoras", new { id = tbpresupuesto.idGrupoInvestigacion, code = 400, posEle = 3 });
            }
            ViewBag.idRubro = new SelectList(db.tblRubro, "id", "Rubro");
            return View(tbpresupuesto);
        }

        public ActionResult PresupuestoDetallado(int id, int code = 0)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            // Validación para no permitir usuarios que no pertenezcan a determinado grupo
            string query = "SELECT * FROM tblMiembroGrupo WHERE idGrupoInvestigacion = {0}";
            var miembrosengrupo = db.Database.SqlQuery<tblMiembroGrupo>(query, id);
            int miembroencontrado = 0;

            foreach (var item in miembrosengrupo)
            {
                if (item.idUsuario.Equals(idUsuario))
                {
                    miembroencontrado = 1;
                    break;
                }
            }

            if (!tblgrupo.idUsuario.Equals(idUsuario))
            {
                if (miembroencontrado == 0)
                {
                    return RedirectToAction("Index");
                }
            }
            // -----------------------------------------------------------------------------

            InformacionGrupo infogrupo = new InformacionGrupo();
            var grupo = tblGrupoInvestigacion.Find(id);
            infogrupo.idGrupo = id;
            infogrupo.Descripcion = grupo.tblPreguntaInvestigacion
                .Where(m => m.idGrupoInvestigacion == id)
                .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
            infogrupo.Institucion = grupo.tblInstitucion.Nombre;
            infogrupo.Municipio = grupo.tblInstitucion.tblMunicipios.NombreMunicipio;
            infogrupo.NombreGrupo = grupo.Nombre;
            ViewBag.InfoGrupo = infogrupo;

            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            var presupuesto = db.tblPresupuestoProyectoInvestigacion.Where(m => m.idGrupoInvestigacion == id).ToList();
            if (presupuesto.Count == 0)
            {
                return RedirectToAction("Index", new { code = 411, id = id });
            }
            return View(presupuesto);
        }

        public ActionResult EliminarPresupuesto(int id)
        {
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            tblPresupuestoProyectoInvestigacion tblconceptos = db.tblPresupuestoProyectoInvestigacion.Where(m => m.id == id).SingleOrDefault();

            // Validación para permitir solo al dueño eliminar presupuestos
            string query = "SELECT * FROM tblGrupoInvestigacion WHERE id = {0}";
            var dueno = db.Database.SqlQuery<tblGrupoInvestigacion>(query, tblconceptos.idGrupoInvestigacion);

            if (idUsuario != dueno.FirstOrDefault().idUsuario)
            {
                return RedirectToAction("IrABitacoras", new { id = tblconceptos.idGrupoInvestigacion, code = 999 });
            }

            tblPresupuestoProyectoInvestigacion tblpresupuesto = db.tblPresupuestoProyectoInvestigacion.Find(id);
            int idGrupo = tblpresupuesto.idGrupoInvestigacion;
            db.tblPresupuestoProyectoInvestigacion.Remove(tblpresupuesto);
            db.SaveChanges();
            return RedirectToAction("PresupuestoDetallado", new { id = idGrupo, code = 410, posEle = 3 });
        }
        #endregion

        #region Bitacora 5 Recorrido
        public ActionResult AgregarEstadoArte(int idGrupoInvestigacion)
        {
            tblEstadoArteProyectoInvestigacion tblestado = new tblEstadoArteProyectoInvestigacion();
            tblestado.idGrupoInvestigacion = idGrupoInvestigacion;
            return View(tblestado);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarEstadoArte([Bind(Include = "id,idGrupoInvestigacion,TemaInvestigacion,MapaConceptual")] tblEstadoArteProyectoInvestigacion tblestado)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tblestado.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tblestado.idGrupoInvestigacion });
            }
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = Server.MapPath("~/Upload/");
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);
                    string fileName = dl.Codigos.CMap(db.tblGrupoInvestigacion.Find(tblestado.idGrupoInvestigacion).Nombre, ext);
                    tblestado.MapaConceptual = fileName;
                    string savedFileName = Server.MapPath("~/Upload/" + fileName);
                    hpf.SaveAs(savedFileName);
                    db.tblEstadoArteProyectoInvestigacion.Add(tblestado);
                    db.SaveChanges();
                    return RedirectToAction("IrAbitacoras", new { id = tblestado.idGrupoInvestigacion, code = 600 });
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 135, id = tblestado.idGrupoInvestigacion });
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarConcepto([Bind(Include = "id,idEstadoArte,Autor,Publicacion,Texto")]tblConceptosEstadoArte tblconcepto)
        {
            string query = "SELECT TOP 1 * FROM tblConceptosEstadoArte ORDER BY id DESC";
            var dueno = db.Database.SqlQuery<tblConceptosEstadoArte>(query);
            tblconcepto.id = dueno.SingleOrDefault().id + 1;

            string userId = AspNetUsers.GetUserId(User.Identity.Name);            
            int idGrupoInvestigacion = db.tblEstadoArteProyectoInvestigacion.Find(tblconcepto.idEstadoArte).idGrupoInvestigacion;
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                db.tblConceptosEstadoArte.Add(tblconcepto);
                db.SaveChanges();
                return RedirectToAction("IrAbitacoras", new { id = idGrupoInvestigacion, code = 601, posEle = 4 });
            }
            return View(tblconcepto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarInformacion([Bind(Include = "id,idGrupoInvestigacion,idInstrumento,Evidencia,Descripcion")] tblRecoleccionInformacionProyectoInvestigacion tblinformacion)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tblinformacion.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tblinformacion.idGrupoInvestigacion });
            }
            try
            {
                foreach (string file in Request.Files)
                {
                    //HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    //if (hpf.ContentLength == 0)
                    //    continue;
                    //string folderPath = Server.MapPath("~/Upload/");
                    //Directory.CreateDirectory(folderPath);
                    //string ext = Path.GetExtension(hpf.FileName);
                    //var grupo = db.tblGrupoInvestigacion.Find(tblinformacion.idGrupoInvestigacion);
                    //string fileName = dl.Codigos.Evidence(grupo.Nombre, grupo.tblRecoleccionInformacionProyectoInvestigacion.Count(), ext);
                    //tblinformacion.Evidencia = fileName;
                    //string savedFileName = Server.MapPath("~/Upload/" + fileName);
                    //hpf.SaveAs(savedFileName);
                    var grupo = db.tblGrupoInvestigacion.Find(tblinformacion.idGrupoInvestigacion);

                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = string.Format("{0}", Server.MapPath("~/Upload/"));
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);

                    string fileName = "recInf" + grupo.id + Convert.ToString(DateTime.Now.Ticks);

                    string savedFileName = Server.MapPath("~/Upload/" + fileName + ext);
                    hpf.SaveAs(savedFileName);
                    tblinformacion.Evidencia = string.Format("{0}", fileName + ext);

                    db.tblRecoleccionInformacionProyectoInvestigacion.Add(tblinformacion);
                    db.SaveChanges();
                    return RedirectToAction("IrAbitacoras", new { id = tblinformacion.idGrupoInvestigacion, code = 602, posEle = 4 });
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 601, id = tblinformacion.idGrupoInvestigacion });
            }
            return View();
        }
        #endregion

        #region Bitacora 6 Recorrido

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarReflexionDeLaOnda(tblReflexionOnda reflexionDeLaOnda)
        {
            reflexionDeLaOnda.FechaInicio = System.DateTime.Now;
            reflexionDeLaOnda.FechaFin = System.DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {                    
                    db.tblReflexionOnda.Add(reflexionDeLaOnda);
                    db.SaveChanges();
                    return RedirectToAction("IrABitacoras", new { code = 603, id = reflexionDeLaOnda.idGrupoInvestigacion, posEle = 5 });
                }
                catch
                {
                    return RedirectToAction("IrABitacoras", new { code = 999, id = reflexionDeLaOnda.idGrupoInvestigacion });
                }
                

            }
            return View(reflexionDeLaOnda);
            
        }

            
        #endregion

        #region Bitacora 7 Propagacion de la onda        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NuevaPropagacion([Bind(Include = "id,idGrupoInvestigacion,idTipoFeria,Archivo,Descripcion")] tblPropagacionGrupo model)
        {
            int id = model.idGrupoInvestigacion;
            try
            {
                foreach (string file in Request.Files)
                {
                    var grupo = db.tblGrupoInvestigacion.Find(model.idGrupoInvestigacion);
                    int index = db.tblPropagacionGrupo.Where(m => m.idGrupoInvestigacion == id).Count();
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = string.Format("{0}{1}/", Server.MapPath("~/Upload/"), grupo.Nombre.Replace(" ", ""));
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);
                    string fileName = "";
                    fileName = dl.Codigos.ImgInstitucional(grupo.Nombre.Replace(" ", ""), ext, index);
                    string savedFileName = Server.MapPath("~/Upload/" + grupo.Nombre.Replace(" ", "") + "/" + fileName);
                    hpf.SaveAs(savedFileName);
                    model.Archivo = string.Format("../../Upload/{0}/{1}", grupo.Nombre.Replace(" ", ""), fileName);
                    db.tblPropagacionGrupo.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return RedirectToAction("IrABitacoras", new { code = 122, id = id });
            }
            return RedirectToAction("IrABitacoras", new { code = 121, id = id, posEle = 6 });
        }

        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);

            // SELECT investic.dbo.AspNetUsers.* FROM investic.dbo.AspNetUsers, investic.dbo.AspNetUserRoles, investic.dbo.AspNetRoles WHERE AspNetUsers.Id=AspNetUserRoles.UserId AND AspNetUserRoles.RoleId=AspNetRoles.Id AND AspNetRoles.Name='Maestro'
            string eninvita = "select tblInvitacionGrupo.idUsuario from tblInvitacionGrupo, tblGrupoInvestigacion where tblInvitacionGrupo.idGrupo = tblGrupoInvestigacion.id and tblGrupoInvestigacion.idUsuario = '" + userId + "'";
            string query = "SELECT AspNetUsers.* FROM AspNetUsers, AspNetUserRoles, AspNetRoles WHERE AspNetUsers.id NOT IN (" + eninvita + ") AND AspNetUsers.id <> '" + userId + "' AND AspNetUsers.Id=AspNetUserRoles.UserId AND AspNetUserRoles.RoleId=AspNetRoles.Id AND AspNetRoles.Name='Maestro' AND (AspNetUsers.Name LIKE '%" + param.sSearch + "%' OR AspNetUsers.SureName LIKE '%" + param.sSearch + "%' OR AspNetUsers.Email LIKE '%" + param.sSearch + "%' OR AspNetUsers.PersonalID LIKE '%" + param.sSearch + "%') ORDER BY AspNetUsers.Id OFFSET " + param.iDisplayStart + " ROWS FETCH NEXT " + param.iDisplayLength + " ROWS ONLY";
            IEnumerable<AspNetUsers> allCompanies = db.Database.SqlQuery<AspNetUsers>(query);

            string querycount = "SELECT count(*) FROM AspNetUsers, AspNetUserRoles, AspNetRoles WHERE AspNetUsers.Id=AspNetUserRoles.UserId AND AspNetUserRoles.RoleId=AspNetRoles.Id AND AspNetRoles.Name='Maestro' AND (AspNetUsers.Name LIKE '%" + param.sSearch + "%' OR AspNetUsers.SureName LIKE '%" + param.sSearch + "%' OR AspNetUsers.Email LIKE '%" + param.sSearch + "%' OR AspNetUsers.PersonalID LIKE '%" + param.sSearch + "%')";
            var cuan = db.Database.SqlQuery<int>(querycount).First();

            var result = from c in allCompanies
                         select new[] { c.Name, c.SureName, c.UserName, c.Email, "<a href='../InvitarColaborador/" + c.Id + "?idGroup=" + HttpContext.Request.Params.Get("idGroup") + "'>Invitar Colaborador</a>" };

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = cuan,
                iTotalDisplayRecords = cuan,
                iDisplayStart = param.iDisplayStart,
                iDisplayLength = param.iDisplayLength,
                sSearch = param.sSearch,
                aaData = (result).ToArray()
            },
                            JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxHandlerEstudiante(jQueryDataTableParamModel param)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);

            // SELECT investic.dbo.AspNetUsers.* FROM investic.dbo.AspNetUsers, investic.dbo.AspNetUserRoles, investic.dbo.AspNetRoles WHERE AspNetUsers.Id=AspNetUserRoles.UserId AND AspNetUserRoles.RoleId=AspNetRoles.Id AND AspNetRoles.Name='Maestro'
            string eninvita = "select tblInvitacionGrupo.idUsuario from tblInvitacionGrupo, tblGrupoInvestigacion where tblInvitacionGrupo.idGrupo = tblGrupoInvestigacion.id and tblGrupoInvestigacion.idUsuario = '" + userId + "'";
            string query = "SELECT AspNetUsers.* FROM AspNetUsers, AspNetUserRoles, AspNetRoles WHERE AspNetUsers.id NOT IN (" + eninvita + ") AND AspNetUsers.Id=AspNetUserRoles.UserId AND AspNetUserRoles.RoleId=AspNetRoles.Id AND AspNetRoles.Name='Estudiante' AND (AspNetUsers.Name LIKE '%" + param.sSearch + "%' OR AspNetUsers.SureName LIKE '%" + param.sSearch + "%' OR AspNetUsers.Email LIKE '%" + param.sSearch + "%' OR AspNetUsers.PersonalID LIKE '%" + param.sSearch + "%') ORDER BY AspNetUsers.Id OFFSET " + param.iDisplayStart + " ROWS FETCH NEXT " + param.iDisplayLength + " ROWS ONLY";
            IEnumerable<AspNetUsers> allCompanies = db.Database.SqlQuery<AspNetUsers>(query);

            string querycount = "SELECT count(*) FROM AspNetUsers, AspNetUserRoles, AspNetRoles WHERE AspNetUsers.Id=AspNetUserRoles.UserId AND AspNetUserRoles.RoleId=AspNetRoles.Id AND AspNetRoles.Name='Estudiante' AND (AspNetUsers.Name LIKE '%" + param.sSearch + "%' OR AspNetUsers.SureName LIKE '%" + param.sSearch + "%' OR AspNetUsers.Email LIKE '%" + param.sSearch + "%' OR AspNetUsers.PersonalID LIKE '%" + param.sSearch + "%')";
            var cuan = db.Database.SqlQuery<int>(querycount).First();

            var result = from c in allCompanies
                         select new[] { c.Name, c.SureName, c.UserName, c.Email, "<a href='../InvitarEstudiante/" + c.Id + "?idGroup=" + HttpContext.Request.Params.Get("idGroup") + "'>Invitar Estudiante</a>" };

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = cuan,
                iTotalDisplayRecords = cuan,
                iDisplayStart = param.iDisplayStart,
                iDisplayLength = param.iDisplayLength,
                sSearch = param.sSearch,
                aaData = (result).ToArray()
            },
                            JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditarPresupuesto( int id )
        {
            tblPresupuestoProyectoInvestigacion model = db.tblPresupuestoProyectoInvestigacion.Find(id);

            ViewBag.idRubro = new SelectList(db.tblRubro, "id", "Rubro");

            return PartialView( "_EditarPresupuesto", model );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarPresupuesto([Bind(Include = "id,idGrupoinvestigacion,idRubro,DescripcionGasto,ValorRubro,ValorUnitario,Total")] tblPresupuestoProyectoInvestigacion tbpresupuesto)
        {
            string userId = AspNetUsers.GetUserId(User.Identity.Name);
            int idRol = tblMiembroGrupo.GetRoleMiembro(userId, tbpresupuesto.idGrupoInvestigacion);
            if (idRol != 1)
            {
                return RedirectToAction("Index", new { code = 999, id = tbpresupuesto.idGrupoInvestigacion });
            }
            if (ModelState.IsValid)
            {
                tbpresupuesto.Total = tbpresupuesto.ValorRubro * tbpresupuesto.ValorUnitario;
                db.Entry(tbpresupuesto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IrABitacoras", new { id = tbpresupuesto.idGrupoInvestigacion, code = 400, posEle = 3 });
            }
            return View(tbpresupuesto);
        }

        [HttpPost]
        public ActionResult CargarImagenFeria(tblPropagacionGrupo model)
        {
            int id = model.idGrupoInvestigacion;
            try
            {
                foreach (string file in Request.Files)
                {
                    var grupo = db.tblGrupoInvestigacion.Find(model.idGrupoInvestigacion);
                    int index = db.tblPropagacionGrupo.Where(m => m.idGrupoInvestigacion == id).Count();
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = string.Format("{0}{1}/", Server.MapPath("~/Upload/"), grupo.Nombre.Replace(" ", ""));
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);
                    string fileName = "";
                    fileName = dl.Codigos.ImgInstitucional(grupo.Nombre.Replace(" ", ""), ext, index);
                    string savedFileName = Server.MapPath("~/Upload/" + grupo.Nombre.Replace(" ", "") + "/" + fileName);
                    hpf.SaveAs(savedFileName);
                    model.Archivo = string.Format("../../Upload/{0}/{1}", grupo.Nombre.Replace(" ", ""), fileName);
                    db.tblPropagacionGrupo.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { id = id, code = 135 });
            }
            return RedirectToAction("Index", new { id = id, code = 130 });
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CargarValoracionBitacora(tblValoracionBitacora model)
        {
            int id = model.idGrupoInvestigacion;
            try
            {
                foreach (string file in Request.Files)
                {
                    var grupo = db.tblGrupoInvestigacion.Find(model.idGrupoInvestigacion);
                    //int index = nuevaValoracion.Where(m => m.idGrupoInvestigacion == idValor).Count();
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = string.Format("{0}{1}/", Server.MapPath("~/Upload/"), grupo.id);
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);
                    //string fileName = "valoracionBitacora"+grupo.id;
                    string fileName = "vB" + grupo.id + Convert.ToString(DateTime.Now.Ticks);
                    //string fileName = Convert.ToString(model.idValoracion);
                    //fileName = dl.Codigos.ImgInstitucional(grupo.Nombre.Replace(" ", ""), ext, index);
                    string savedFileName = Server.MapPath("~/Upload/" + grupo.id + "/" + fileName + ext);
                    hpf.SaveAs(savedFileName);
                    model.archivoValoracion = string.Format("{0}", fileName + ext);

                    var RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblValoracionBitacora (idGrupoInvestigacion, descripcionValoracion, archivoValoracion) values ({0}, {1}, {2})", grupo.id, model.descripcionValoracion, model.archivoValoracion);
                }
            }
            catch (Exception)
            {
                return RedirectToAction("IrABitacoras", new { code = 135, id = id });
            }
            return RedirectToAction("IrABitacoras", new { code = 130, id = id, posEle = 7 });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CargarParticipacionGrupo(tblParticipacionGrupo model)
        {
            int id = model.idGrupoInvestigacion;
            try
            {
                foreach (string file in Request.Files)
                {
                    var grupo = db.tblGrupoInvestigacion.Find(model.idGrupoInvestigacion);
                    
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    string folderPath = string.Format("{0}{1}/", Server.MapPath("~/Upload/"), grupo.id);
                    Directory.CreateDirectory(folderPath);
                    string ext = Path.GetExtension(hpf.FileName);
                    
                    string fileName = "pG" + grupo.id + Convert.ToString(DateTime.Now.Ticks);
                    
                    string savedFileName = Server.MapPath("~/Upload/" + grupo.id + "/" + fileName + ext);
                    hpf.SaveAs(savedFileName);
                    model.archivoParticipacion = string.Format("{0}", fileName + ext);

                    var RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblParticipacionGrupo (idGrupoInvestigacion, descripcionParticipacion, archivoParticipacion) values ({0}, {1}, {2})", grupo.id, model.descripcionParticipacion, model.archivoParticipacion);
                }
            }
            catch (Exception)
            {
                return RedirectToAction("IrABitacoras", new { code = 135, id = id });
            }
            return RedirectToAction("IrABitacoras", new { code = 130, id = id });
        }

        #region subir xml offline
        public ActionResult subirOffline(int id)
        {
            tblGrupoInvestigacion tblgrupo = db.tblGrupoInvestigacion.Find(id);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);

            InformacionGrupo infogrupo = new InformacionGrupo();
            infogrupo.idGrupo = id;
            
            if (tblgrupo.tblPreguntaInvestigacion.Count > 0)
            {
                infogrupo.Descripcion = tblgrupo.tblPreguntaInvestigacion
                    .Where(m => m.idGrupoInvestigacion == id)
                    .Where(m => m.PreguntaPrincipal).Select(m => m.Pregunta).First();
                infogrupo.Institucion = tblgrupo.tblInstitucion.Nombre;
                infogrupo.Municipio = tblgrupo.tblInstitucion.tblMunicipios.NombreMunicipio;
                infogrupo.NombreGrupo = tblgrupo.Nombre;
            }
            
            return View("subirOffline", tblgrupo);
            
            
            
        }
        [HttpPost]
        public ActionResult leerXml()
        {            
            string folderPath = "";
            string filename = "";
            string savedfileName;

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase archivo = Request.Files[file] as HttpPostedFileBase;

                folderPath = Server.MapPath("~/Archivos/iep/");

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                filename = Path.GetFileName(archivo.FileName);

                savedfileName = folderPath + filename;
                archivo.SaveAs(savedfileName);

                string direccion = savedfileName;
                cargarXml(direccion);

            }
            int cod = 0;
            if (falla_xml == "true")
            {
                cod = 13908;                
            }
            else
            {
                if (respuesta == "true")
                {
                    cod = 13906;
                }
                else if (respuesta == "nousuario")
                {
                    cod = 13907;
                }
                else if (respuesta == "false")
                {
                    cod = 13908;
                }
            }
            return RedirectToAction(retorno, new { code = cod });
        }

        public ActionResult cargarXml(string datos)
        {
            //conectar a la base de datos
            Conexion_bd con = new Conexion_bd();
            string sql_final="";
            string sql="";
            int cod = 0;
            
            XmlDocument documentoXml = new XmlDocument();
            try
            {
                documentoXml.Load(datos);
                XmlNodeList proyecto = documentoXml.GetElementsByTagName("Proyecto");
                if (proyecto.Count == 0)
                {
                    falla_xml = "true";
                    retorno = "Index";
                    return RedirectToAction(retorno, new { code = cod });
                }
                string idGrupo = "";
                string idenUsu = "";
                //verificar usuario
                var usuId = AspNetUsers.GetUserId(User.Identity.Name);
                AspNetUsers tblUsu = db.AspNetUsers.Find(usuId);
                string numDocUsu = tblUsu.PersonalID;

                foreach (XmlElement nodoProyecto in proyecto)
                {
                    XmlNodeList informacion = ((XmlElement)nodoProyecto).GetElementsByTagName("InFormacion");
                    foreach (XmlElement nodoInformacion in informacion)
                    {
                        XmlNodeList gru_inv = nodoInformacion.GetElementsByTagName("investic.dbo.tblGrupoInvestigacion");
                        foreach (XmlElement nodoGruInv in gru_inv)
                        {
                            XmlNodeList IdGurpo = nodoGruInv.GetElementsByTagName("IdGrupo");
                            idGrupo = IdGurpo[0].InnerText;

                            XmlNodeList institucion = nodoGruInv.GetElementsByTagName("investic.dbo.tblInstitucion");
                            foreach (XmlElement nodoInstitucion in institucion)
                            {
                                XmlNodeList lineaInv = nodoInstitucion.GetElementsByTagName("investic.dbo.tblLineaInvestigacion");
                                foreach (XmlElement nodoInv in lineaInv)
                                {
                                    XmlNodeList user = nodoInv.GetElementsByTagName("investic.dbo.AspNetUsers");
                                    foreach (XmlElement nodoUser in user)
                                    {
                                        XmlNodeList Identificacion = nodoUser.GetElementsByTagName("Identificacion");
                                        idenUsu = Identificacion[0].InnerText;
                                    }
                                }
                            }
                        }
                    }
                }
                
                if (!numDocUsu.Equals(idenUsu))
                {
                    retorno = "IrAlProyecto/" + idGrupo;
                    //return RedirectToAction(retorno, new { code = 13907 });
                    cod = 13907;
                    respuesta = "nousuario";
                }
                else
                {
                    foreach (XmlElement nodoProyecto in proyecto)
                    {
                        //XmlNodeList informacion = ((XmlElement)nodoProyecto).GetElementsByTagName("InFormacion");

                        XmlNodeList grupos = ((XmlElement)nodoProyecto).GetElementsByTagName("Grupos");
                        foreach (XmlElement nodoGrupos in grupos)
                        {
                            XmlNodeList gru_inv = nodoGrupos.GetElementsByTagName("investic.dbo.tblGrupoInvestigacion");
                            foreach (XmlElement nodoGruInv in gru_inv)
                            {
                                XmlNodeList id = nodoGruInv.GetElementsByTagName("id");
                                XmlNodeList Codigo = nodoGruInv.GetElementsByTagName("Codigo");
                                XmlNodeList Nombre = nodoGruInv.GetElementsByTagName("Nombre");
                                XmlNodeList FechaCreacion = nodoGruInv.GetElementsByTagName("FechaCreacion");
                                XmlNodeList TipoGrupo = nodoGruInv.GetElementsByTagName("TipoGrupo");
                                XmlNodeList Avatar = nodoGruInv.GetElementsByTagName("Avatar");
                                XmlNodeList idInstitucion = nodoGruInv.GetElementsByTagName("idInstitucion");
                                XmlNodeList idLineaInvestigacion = nodoGruInv.GetElementsByTagName("idLineaInvestigacion");
                                XmlNodeList idUsuario = nodoGruInv.GetElementsByTagName("idUsuario");
                                string cadena = "";
                                if (Codigo.Count == 1)
                                {
                                    cadena = cadena + "Codigo='" + Codigo[0].InnerText + "'";
                                }
                                if (Nombre.Count == 1)
                                {
                                    cadena = cadena + ",Nombre='" + Nombre[0].InnerText + "'";
                                }
                                if (FechaCreacion.Count == 1)
                                {
                                    cadena = cadena + ",FechaCreacion='" + FechaCreacion[0].InnerText + "'";
                                }
                                if (TipoGrupo.Count == 1)
                                {
                                    cadena = cadena + ",TipoGrupo='" + TipoGrupo[0].InnerText + "'";
                                }
                                if (Avatar.Count == 1)
                                {
                                    cadena = cadena + ",Avatar='" + Avatar[0].InnerText + "'";
                                }
                                if (idInstitucion.Count == 1)
                                {
                                    cadena = cadena + ",idInstitucion='" + idInstitucion[0].InnerText + "'";
                                }
                                if (idLineaInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idLineaInvestigacion='" + idLineaInvestigacion[0].InnerText + "'";
                                }
                                if (idUsuario.Count == 1)
                                {
                                    cadena = cadena + ",idUsuario='" + idUsuario[0].InnerText + "'";
                                }
                                //sql = "SELECT * FROM tblGrupoInvestigacion WHERE id='" + Convert.ToInt16(id[0].InnerText) + "'";

                                sql = "UPDATE tblGrupoInvestigacion SET " + cadena + "  WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";
                                /*
                                //campos para  actualizar en la base de datos con entity framework
                                tblGrupoInvestigacion tblGrupoInvestigacion = db.tblGrupoInvestigacion.Find(Convert.ToInt16(id[0].InnerText));
                                //tblGrupoInvestigacion.id = Convert.ToInt16(id[0].InnerText);                        
                                tblGrupoInvestigacion.Codigo = Codigo[0].InnerText;
                                tblGrupoInvestigacion.Nombre = Nombre[0].InnerText;
                                tblGrupoInvestigacion.FechaCreacion = Convert.ToDateTime(FechaCreacion[0].InnerText);
                                tblGrupoInvestigacion.TipoGrupo = Convert.ToInt16(TipoGrupo[0].InnerText);
                                tblGrupoInvestigacion.Avatar = Avatar[0].InnerText;
                                tblGrupoInvestigacion.idInstitucion = Convert.ToInt16(idInstitucion[0].InnerText);
                                tblGrupoInvestigacion.idLineaInvestigacion = Convert.ToInt16(idLineaInvestigacion[0].InnerText);
                                tblGrupoInvestigacion.idUsuario = idUsuario[0].InnerText;

                                db.Entry(tblGrupoInvestigacion).State = EntityState.Modified;
                                db.SaveChanges();*/
                                // para  utlizar en retorno 
                                idGrupo = id[0].InnerText;
                            }

                        }
                        XmlNodeList RegistroInv = ((XmlElement)nodoProyecto).GetElementsByTagName("RegistroInv");
                        foreach (XmlElement nodoRegistroInv in RegistroInv)
                        {
                            XmlNodeList miembros = nodoRegistroInv.GetElementsByTagName("investic.dbo.tblMiembroGrupo");
                            foreach (XmlElement nodoMiembros in miembros)
                            {
                                XmlNodeList id = nodoMiembros.GetElementsByTagName("id");
                                XmlNodeList idGrupoInvestigacion = nodoMiembros.GetElementsByTagName("idGrupoInvestigacion");
                                XmlNodeList idUsuario = nodoMiembros.GetElementsByTagName("idUsuario");
                                XmlNodeList idRol = nodoMiembros.GetElementsByTagName("idRol");
                                XmlNodeList Grado = nodoMiembros.GetElementsByTagName("Grado");
                                string cadena = "";
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + "idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                if (idUsuario.Count == 1)
                                {
                                    cadena = cadena + ",idUsuario='" + idUsuario[0].InnerText + "'";
                                }
                                if (idRol.Count == 1)
                                {
                                    cadena = cadena + ",idRol='" + idRol[0].InnerText + "'";
                                }
                                if (Grado.Count == 1)
                                {
                                    cadena = cadena + ",Grado='" + Grado[0].InnerText + "'";
                                }
                                sql = "UPDATE tblMiembroGrupo SET " + cadena + "  WHERE id='" + id[0].InnerText + "'";

                                sql_final = sql_final + sql + ";";
                                //con.Ejecutar(sql_final);

                            }

                        }
                        XmlNodeList BitacoraMaestro = ((XmlElement)nodoProyecto).GetElementsByTagName("BitacoraMaestro");
                        foreach (XmlElement nodoBitacoraMaestro in BitacoraMaestro)
                        {
                            XmlNodeList reflexion = nodoBitacoraMaestro.GetElementsByTagName("investic.dbo.tblReflexionProyectoInvestigacion");
                            foreach (XmlElement nodoReflexion in reflexion)
                            {
                                XmlNodeList id = nodoReflexion.GetElementsByTagName("id");
                                XmlNodeList FechaInicio = nodoReflexion.GetElementsByTagName("FechaInicio");
                                XmlNodeList UltimaModificacion = nodoReflexion.GetElementsByTagName("UltimaModificacion");
                                XmlNodeList Proceso = nodoReflexion.GetElementsByTagName("Proceso");
                                XmlNodeList Motivacion = nodoReflexion.GetElementsByTagName("Motivacion");
                                XmlNodeList Reflexion = nodoReflexion.GetElementsByTagName("Reflexion");
                                XmlNodeList ConceptoAsesor = nodoReflexion.GetElementsByTagName("ConceptoAsesor");
                                XmlNodeList Revisado = nodoReflexion.GetElementsByTagName("Revisado");
                                XmlNodeList idGrupoInvestigacion = nodoReflexion.GetElementsByTagName("idGrupoInvestigacion");

                                string cadena = "";
                                if (FechaInicio.Count == 1)
                                {
                                    cadena = cadena + "FechaInicio='" + FechaInicio[0].InnerText + "'";
                                }
                                if (UltimaModificacion.Count == 1)
                                {
                                    cadena = cadena + ",UltimaModificacion='" + UltimaModificacion[0].InnerText + "'";
                                }
                                if (Proceso.Count == 1)
                                {
                                    cadena = cadena + ",Proceso='" + Proceso[0].InnerText + "'";
                                }
                                if (Motivacion.Count == 1)
                                {
                                    cadena = cadena + ",Motivacion='" + Motivacion[0].InnerText + "'";
                                }
                                if (Reflexion.Count == 1)
                                {
                                    cadena = cadena + ",Reflexion='" + Reflexion[0].InnerText + "'";
                                }
                                if (ConceptoAsesor.Count == 1)
                                {
                                    cadena = cadena + ",ConceptoAsesor='" + ConceptoAsesor[0].InnerText + "'";
                                }
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblReflexionProyectoInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList PerturbacionGrupoInv = ((XmlElement)nodoProyecto).GetElementsByTagName("PerturbacionGrupoInv");
                        foreach (XmlElement nodoPerturbacionGrupoInv in PerturbacionGrupoInv)
                        {
                            XmlNodeList pregunta = nodoPerturbacionGrupoInv.GetElementsByTagName("investic.dbo.tblPreguntaInvestigacion");
                            foreach (XmlElement nodoPregunta in pregunta)
                            {
                                XmlNodeList id = nodoPregunta.GetElementsByTagName("id");
                                XmlNodeList Consecutivo = nodoPregunta.GetElementsByTagName("Consecutivo");
                                XmlNodeList Pregunta = nodoPregunta.GetElementsByTagName("Pregunta");
                                XmlNodeList FechaCreacion = nodoPregunta.GetElementsByTagName("FechaCreacion");
                                XmlNodeList FechaModificacion = nodoPregunta.GetElementsByTagName("FechaModificacion");
                                XmlNodeList PreguntaPrincipal = nodoPregunta.GetElementsByTagName("PreguntaPrincipal");
                                XmlNodeList idGrupoInvestigacion = nodoPregunta.GetElementsByTagName("idGrupoInvestigacion");
                                string cadena = "";
                                if (Consecutivo.Count == 1)
                                {
                                    cadena = cadena + "Consecutivo='" + Consecutivo[0].InnerText + "'";
                                }
                                if (Pregunta.Count == 1)
                                {
                                    cadena = cadena + ",Pregunta='" + Pregunta[0].InnerText + "'";
                                }
                                if (FechaCreacion.Count == 1)
                                {
                                    cadena = cadena + ",FechaCreacion='" + FechaCreacion[0].InnerText + "'";
                                }
                                if (FechaModificacion.Count == 1)
                                {
                                    cadena = cadena + ",FechaModificacion='" + FechaModificacion[0].InnerText + "'";
                                }
                                if (PreguntaPrincipal.Count == 1)
                                {
                                    cadena = cadena + ",PreguntaPrincipal='" + PreguntaPrincipal[0].InnerText + "'";
                                }
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblPreguntaInvestigacion SET " + cadena + "  WHERE id='" + id[0].InnerText + "'";

                                sql_final = sql_final + sql + ";";

                                //con.Ejecutar(sql);
                            }


                        }
                        XmlNodeList PerturbacionMaestro = ((XmlElement)nodoProyecto).GetElementsByTagName("PerturbacionMaestro");
                        foreach (XmlElement nodoPerturbacionMaestro in PerturbacionMaestro)
                        {
                            XmlNodeList preguntaProyectos = nodoPerturbacionMaestro.GetElementsByTagName("investic.dbo.tblPreguntaProyectoInvestigacion");
                            foreach (XmlElement nodoPreguntaProyectos in preguntaProyectos)
                            {
                                XmlNodeList id = nodoPreguntaProyectos.GetElementsByTagName("id");
                                XmlNodeList InformacionUno = nodoPreguntaProyectos.GetElementsByTagName("InformacionUno");
                                XmlNodeList FuenteUno = nodoPreguntaProyectos.GetElementsByTagName("FuenteUno");
                                XmlNodeList InformacionDos = nodoPreguntaProyectos.GetElementsByTagName("InformacionDos");
                                XmlNodeList FuenteDos = nodoPreguntaProyectos.GetElementsByTagName("FuenteDos");
                                XmlNodeList InformacionTres = nodoPreguntaProyectos.GetElementsByTagName("InformacionTres");
                                XmlNodeList FuenteTres = nodoPreguntaProyectos.GetElementsByTagName("FuenteTres");
                                XmlNodeList Reflexion = nodoPreguntaProyectos.GetElementsByTagName("Reflexion");
                                XmlNodeList ConceptoAsesor = nodoPreguntaProyectos.GetElementsByTagName("ConceptoAsesor");
                                XmlNodeList Revision = nodoPreguntaProyectos.GetElementsByTagName("Revision");
                                XmlNodeList idGrupoInvestigacion = nodoPreguntaProyectos.GetElementsByTagName("idGrupoInvestigacion");
                                string cadena = "";
                                if (InformacionUno.Count == 1)
                                {
                                    cadena = cadena + "InformacionUno='" + InformacionUno[0].InnerText + "'";
                                }
                                if (FuenteUno.Count == 1)
                                {
                                    cadena = cadena + ",FuenteUno='" + FuenteUno[0].InnerText + "'";
                                }
                                if (InformacionDos.Count == 1)
                                {
                                    cadena = cadena + ",InformacionDos='" + InformacionDos[0].InnerText + "'";
                                }
                                if (FuenteDos.Count == 1)
                                {
                                    cadena = cadena + ",FuenteDos='" + FuenteDos[0].InnerText + "'";
                                }
                                if (InformacionTres.Count == 1)
                                {
                                    cadena = cadena + ",InformacionTres='" + InformacionTres[0].InnerText + "'";
                                }
                                if (FuenteTres.Count == 1)
                                {
                                    cadena = cadena + ",FuenteTres='" + FuenteTres[0].InnerText + "'";
                                }
                                if (Reflexion.Count == 1)
                                {
                                    cadena = cadena + ",Reflexion='" + Reflexion[0].InnerText + "'";
                                }
                                if (ConceptoAsesor.Count == 1)
                                {
                                    cadena = cadena + ",ConceptoAsesor='" + ConceptoAsesor[0].InnerText + "'";
                                }
                                if (Revision.Count == 1)
                                {
                                    cadena = cadena + ",Revision='" + Revision[0].InnerText + "'";
                                }
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblPreguntaProyectoInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList SuperposicionGrupo = ((XmlElement)nodoProyecto).GetElementsByTagName("SuperposicionGrupo");
                        foreach (XmlElement nodoSuperposicionGrupo in SuperposicionGrupo)
                        {
                            XmlNodeList problema = nodoSuperposicionGrupo.GetElementsByTagName("investic.dbo.tblProblemaInvestigacion");
                            foreach (XmlElement nodoProblema in problema)
                            {
                                XmlNodeList id = nodoProblema.GetElementsByTagName("id");
                                XmlNodeList Descripcion = nodoProblema.GetElementsByTagName("Descripcion");
                                XmlNodeList Justificacion = nodoProblema.GetElementsByTagName("Justificacion");
                                XmlNodeList idGrupoInvestigacion = nodoProblema.GetElementsByTagName("idGrupoInvestigacion");
                                string cadena = "";
                                if (Descripcion.Count == 1)
                                {
                                    cadena = cadena + "Descripcion='" + Descripcion[0].InnerText + "'";
                                }
                                if (Justificacion.Count == 1)
                                {
                                    cadena = cadena + ",Justificacion='" + Justificacion[0].InnerText + "'";
                                }
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblProblemaInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList BitacoraMaestroPro = ((XmlElement)nodoProyecto).GetElementsByTagName("BitacoraMaestro");
                        foreach (XmlElement nodoBitacoraMaestroPro in BitacoraMaestroPro)
                        {
                            XmlNodeList problemaProyecto = nodoBitacoraMaestroPro.GetElementsByTagName("investic.dbo.tblProblemaProyectoInvestigacion");
                            foreach (XmlElement nodoProblemaProyecto in problemaProyecto)
                            {
                                XmlNodeList id = nodoProblemaProyecto.GetElementsByTagName("id");
                                XmlNodeList Como = nodoProblemaProyecto.GetElementsByTagName("Como");
                                XmlNodeList Reflexion = nodoProblemaProyecto.GetElementsByTagName("Reflexion");
                                XmlNodeList ConceptoAsesor = nodoProblemaProyecto.GetElementsByTagName("ConceptoAsesor");
                                XmlNodeList Revision = nodoProblemaProyecto.GetElementsByTagName("Revision");
                                XmlNodeList idGrupoInvestigacion = nodoProblemaProyecto.GetElementsByTagName("idGrupoInvestigacion");
                                string cadena = "";
                                if (Como.Count == 1)
                                {
                                    cadena = cadena + "Como='" + Como[0].InnerText + "'";
                                }
                                if (Reflexion.Count == 1)
                                {
                                    cadena = cadena + ",Reflexion='" + Reflexion[0].InnerText + "'";
                                }
                                if (ConceptoAsesor.Count == 1)
                                {
                                    cadena = cadena + ",ConceptoAsesor='" + ConceptoAsesor[0].InnerText + "'";
                                }
                                if (Revision.Count == 1)
                                {
                                    cadena = cadena + ",Revision='" + Revision[0].InnerText + "'";
                                }
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblProblemaProyectoInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList BitacoraMaestroPre = ((XmlElement)nodoProyecto).GetElementsByTagName("BitacoraMaestro");
                        foreach (XmlElement nodoBitacoraMaestroPre in BitacoraMaestroPre)
                        {
                            XmlNodeList presupuesto = nodoBitacoraMaestroPre.GetElementsByTagName("investic.dbo.tblPresupuestoProyectoInvestigacion");
                            foreach (XmlElement nodoPresupuesto in presupuesto)
                            {
                                XmlNodeList id = nodoPresupuesto.GetElementsByTagName("id");
                                XmlNodeList idGrupoInvestigacion = nodoPresupuesto.GetElementsByTagName("idGrupoInvestigacion");
                                XmlNodeList idRubro = nodoPresupuesto.GetElementsByTagName("idRubro");
                                XmlNodeList DescripcionGasto = nodoPresupuesto.GetElementsByTagName("DescripcionGasto");
                                XmlNodeList ValorRubro = nodoPresupuesto.GetElementsByTagName("ValorRubro");
                                XmlNodeList ValorUnitario = nodoPresupuesto.GetElementsByTagName("ValorUnitario");
                                XmlNodeList Total = nodoPresupuesto.GetElementsByTagName("Total");
                                string cadena = "";
                                string valores = "";
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + "idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                    valores = valores + "'" + idGrupoInvestigacion[0].InnerText + "',";
                                }
                                if (idRubro.Count == 1)
                                {
                                    cadena = cadena + ",idRubro='" + idRubro[0].InnerText + "'";
                                    valores = valores + "'" + idRubro[0].InnerText + "',";
                                }
                                if (DescripcionGasto.Count == 1)
                                {
                                    cadena = cadena + ",DescripcionGasto='" + DescripcionGasto[0].InnerText + "'";
                                    valores = valores + "'" + DescripcionGasto[0].InnerText + "',";
                                }
                                if (ValorRubro.Count == 1)
                                {
                                    cadena = cadena + ",ValorRubro='" + ValorRubro[0].InnerText + "'";
                                    valores = valores + "'" + ValorRubro[0].InnerText + "',";
                                }
                                if (ValorUnitario.Count == 1)
                                {
                                    cadena = cadena + ",ValorUnitario='" + ValorUnitario[0].InnerText + "'";
                                    valores = valores + "'" + ValorUnitario[0].InnerText + "',";
                                }
                                if (Total.Count == 1)
                                {
                                    cadena = cadena + ",Total='" + Total[0].InnerText + "'";
                                    valores = valores + "'" + Total[0].InnerText + "'";
                                }
                                String sql_add = "SELECT COUNT(*) FROM tblPresupuestoProyectoInvestigacion WHERE id='" + id[0].InnerText + "'";
                                int cont = con.contador(sql_add);
                                if (cont == 1)
                                {
                                    sql = "UPDATE tblPresupuestoProyectoInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                }
                                else
                                {
                                    sql = "INSERT tblPresupuestoProyectoInvestigacion (idGrupoInvestigacion,idRubro,DescripcionGasto,ValorRubro,ValorUnitario,Total) VALUES (" + valores + ")";
                                }
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList RecorridoTrayectoria = ((XmlElement)nodoProyecto).GetElementsByTagName("RecorridoTrayectoria");
                        foreach (XmlElement nodoRecorridoTrayectoria in RecorridoTrayectoria)
                        {
                            XmlNodeList estadoArte = nodoRecorridoTrayectoria.GetElementsByTagName("investic.dbo.tblEstadoArteProyectoInvestigacion");
                            foreach (XmlElement nodoEstadoArte in estadoArte)
                            {
                                XmlNodeList id = nodoEstadoArte.GetElementsByTagName("id");
                                XmlNodeList idGrupoInvestigacion = nodoEstadoArte.GetElementsByTagName("idGrupoInvestigacion");
                                XmlNodeList TemaInvestigacion = nodoEstadoArte.GetElementsByTagName("TemaInvestigacion");
                                XmlNodeList MapaConceptual = nodoEstadoArte.GetElementsByTagName("MapaConceptual");
                                string cadena = "";
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + "idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                if (TemaInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",TemaInvestigacion='" + TemaInvestigacion[0].InnerText + "'";
                                }
                                if (MapaConceptual.Count == 1)
                                {
                                    cadena = cadena + ",MapaConceptual='" + MapaConceptual[0].InnerText + "'";
                                }
                                sql = "UPDATE tblEstadoArteProyectoInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList RecorridoTrayectoriaConcepto = ((XmlElement)nodoProyecto).GetElementsByTagName("RecorridoTrayectoriaConcepto");
                        foreach (XmlElement nodoRecorridoTrayectoriaConcepto in RecorridoTrayectoriaConcepto)
                        {
                            XmlNodeList conceptoEstadoArte = nodoRecorridoTrayectoriaConcepto.GetElementsByTagName("investic.dbo.tblConceptosEstadoArte");
                            foreach (XmlElement nodoConceptoEstadoArte in conceptoEstadoArte)
                            {
                                XmlNodeList id = nodoConceptoEstadoArte.GetElementsByTagName("id");
                                XmlNodeList idEstadoArte = nodoConceptoEstadoArte.GetElementsByTagName("idEstadoArte");
                                XmlNodeList Autor = nodoConceptoEstadoArte.GetElementsByTagName("Autor");
                                XmlNodeList Publicacion = nodoConceptoEstadoArte.GetElementsByTagName("Publicacion");
                                XmlNodeList Texto = nodoConceptoEstadoArte.GetElementsByTagName("Texto");
                                string cadena = "";
                                if (idEstadoArte.Count == 1)
                                {
                                    cadena = cadena + "idEstadoArte='" + idEstadoArte[0].InnerText + "'";
                                }
                                if (Autor.Count == 1)
                                {
                                    cadena = cadena + ",Autor='" + Autor[0].InnerText + "'";
                                }
                                if (Publicacion.Count == 1)
                                {
                                    cadena = cadena + ",Publicacion='" + Publicacion[0].InnerText + "'";
                                }
                                if (Texto.Count == 1)
                                {
                                    cadena = cadena + ",Texto='" + Texto[0].InnerText + "'";
                                }
                                sql = "UPDATE tblConceptosEstadoArte SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList RecoleccionInformacionProyectoInvestigacion = ((XmlElement)nodoProyecto).GetElementsByTagName("RecoleccionInformacionProyectoInvestigacion");
                        foreach (XmlElement nodoRecoleccionInformacionProyectoInvestigacion in RecoleccionInformacionProyectoInvestigacion)
                        {
                            XmlNodeList recoleccionInfo = nodoRecoleccionInformacionProyectoInvestigacion.GetElementsByTagName("investic.dbo.tblRecoleccionInformacionProyectoInvestigacion");
                            foreach (XmlElement nodoRecoleccionInfo in recoleccionInfo)
                            {
                                XmlNodeList id = nodoRecoleccionInfo.GetElementsByTagName("id");
                                XmlNodeList idGrupoInvestigacion = nodoRecoleccionInfo.GetElementsByTagName("idGrupoInvestigacion");
                                XmlNodeList idInstrumento = nodoRecoleccionInfo.GetElementsByTagName("idInstrumento");
                                XmlNodeList Evidencia = nodoRecoleccionInfo.GetElementsByTagName("Evidencia");
                                XmlNodeList Descripcion = nodoRecoleccionInfo.GetElementsByTagName("Descripcion");
                                string cadena = "";
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + "idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                if (idInstrumento.Count == 1)
                                {
                                    cadena = cadena + ",idInstrumento='" + idInstrumento[0].InnerText + "'";
                                }
                                if (Evidencia.Count == 1)
                                {
                                    cadena = cadena + ",Evidencia='" + Evidencia[0].InnerText + "'";
                                }
                                if (Descripcion.Count == 1)
                                {
                                    cadena = cadena + ",Descripcion='" + Descripcion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblRecoleccionInformacionProyectoInvestigacion SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList Reflecion = ((XmlElement)nodoProyecto).GetElementsByTagName("Reflecion");
                        foreach (XmlElement nodoReflecion in Reflecion)
                        {
                            XmlNodeList reflexionOnda = nodoReflecion.GetElementsByTagName("investic.dbo.tblReflexionOnda");
                            foreach (XmlElement nodoReflexionOnda in reflexionOnda)
                            {
                                XmlNodeList id = nodoReflexionOnda.GetElementsByTagName("id");
                                XmlNodeList Introduccion = nodoReflexionOnda.GetElementsByTagName("Introduccion");
                                XmlNodeList ConformacionGrupo = nodoReflexionOnda.GetElementsByTagName("ConformacionGrupo");
                                XmlNodeList ObjetivoInvestigacion = nodoReflexionOnda.GetElementsByTagName("ObjetivoInvestigacion");
                                XmlNodeList ActividadasRealizadas = nodoReflexionOnda.GetElementsByTagName("ActividadasRealizadas");
                                XmlNodeList ConceptosPrincipales = nodoReflexionOnda.GetElementsByTagName("ConceptosPrincipales");
                                XmlNodeList EspaciosParticipacion = nodoReflexionOnda.GetElementsByTagName("EspaciosParticipacion");
                                XmlNodeList Conclusiones = nodoReflexionOnda.GetElementsByTagName("Conclusiones");
                                XmlNodeList idGrupoInvestigacion = nodoReflexionOnda.GetElementsByTagName("idGrupoInvestigacion");
                                XmlNodeList FechaInicio = nodoReflexionOnda.GetElementsByTagName("FechaInicio");
                                XmlNodeList FechaFin = nodoReflexionOnda.GetElementsByTagName("FechaFin");
                                string cadena = "";
                                if (Introduccion.Count == 1)
                                {
                                    cadena = cadena + "Introduccion='" + Introduccion[0].InnerText + "'";
                                }
                                if (ConformacionGrupo.Count == 1)
                                {
                                    cadena = cadena + ",ConformacionGrupo='" + ConformacionGrupo[0].InnerText + "'";
                                }
                                if (ObjetivoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",ObjetivoInvestigacion='" + ObjetivoInvestigacion[0].InnerText + "'";
                                }
                                if (ActividadasRealizadas.Count == 1)
                                {
                                    cadena = cadena + ",ActividadasRealizadas='" + ActividadasRealizadas[0].InnerText + "'";
                                }
                                if (ConceptosPrincipales.Count == 1)
                                {
                                    cadena = cadena + ",ConceptosPrincipales='" + ConceptosPrincipales[0].InnerText + "'";
                                }
                                if (EspaciosParticipacion.Count == 1)
                                {
                                    cadena = cadena + ",EspaciosParticipacion='" + EspaciosParticipacion[0].InnerText + "'";
                                }
                                if (Conclusiones.Count == 1)
                                {
                                    cadena = cadena + ",Conclusiones='" + Conclusiones[0].InnerText + "'";
                                }
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + ",idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                if (FechaInicio.Count == 1)
                                {
                                    cadena = cadena + ",FechaInicio='" + FechaInicio[0].InnerText + "'";
                                }
                                if (FechaFin.Count == 1)
                                {
                                    cadena = cadena + ",FechaFin='" + FechaFin[0].InnerText + "'";
                                }
                                sql = "UPDATE tblReflexionOnda SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList TipoFeria = ((XmlElement)nodoProyecto).GetElementsByTagName("TipoFeria");
                        foreach (XmlElement nodoTipoFeria in TipoFeria)
                        {
                            XmlNodeList tipoFeria = nodoTipoFeria.GetElementsByTagName("investic.dbo.tblTipoFeria");
                            foreach (XmlElement nodoTipoFerias in tipoFeria)
                            {
                                XmlNodeList id = nodoTipoFerias.GetElementsByTagName("id");
                                XmlNodeList TipoFerias = nodoTipoFerias.GetElementsByTagName("TipoFeria");
                                string cadena = "";
                                if (TipoFerias.Count == 1)
                                {
                                    cadena = cadena + "TipoFeria='" + TipoFerias[0].InnerText + "'";
                                }
                                sql = "UPDATE tblTipoFeria SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                        XmlNodeList PropagacionGrupo = ((XmlElement)nodoProyecto).GetElementsByTagName("PropagacionGrupo");
                        foreach (XmlElement nodoPropagacionGrupo in PropagacionGrupo)
                        {
                            XmlNodeList propagacionGrupo = nodoPropagacionGrupo.GetElementsByTagName("investic.dbo.tblPropagacionGrupo");
                            foreach (XmlElement nodoPropagacionGrupos in propagacionGrupo)
                            {
                                XmlNodeList id = nodoPropagacionGrupos.GetElementsByTagName("id");
                                XmlNodeList idGrupoInvestigacion = nodoPropagacionGrupos.GetElementsByTagName("idGrupoInvestigacion");
                                XmlNodeList idTipoFeria = nodoPropagacionGrupos.GetElementsByTagName("idTipoFeria");
                                XmlNodeList Archivo = nodoPropagacionGrupos.GetElementsByTagName("Archivo");
                                XmlNodeList Descripcion = nodoPropagacionGrupos.GetElementsByTagName("Descripcion");
                                string cadena = "";
                                if (idGrupoInvestigacion.Count == 1)
                                {
                                    cadena = cadena + "idGrupoInvestigacion='" + idGrupoInvestigacion[0].InnerText + "'";
                                }
                                if (idTipoFeria.Count == 1)
                                {
                                    cadena = cadena + ",idTipoFeria='" + idTipoFeria[0].InnerText + "'";
                                }
                                if (Archivo.Count == 1)
                                {
                                    cadena = cadena + ",Archivo='" + Archivo[0].InnerText + "'";
                                }
                                if (Descripcion.Count == 1)
                                {
                                    cadena = cadena + ",Descripcion='" + Descripcion[0].InnerText + "'";
                                }
                                sql = "UPDATE tblPropagacionGrupo SET " + cadena + "   WHERE id='" + id[0].InnerText + "'";
                                //con.Ejecutar(sql);
                                sql_final = sql_final + sql + ";";

                            }

                        }
                    }
                    if (con.Ejecutar(sql_final))
                    {
                        respuesta = "true";
                        cod = 13906;
                    }
                    else
                    {
                        respuesta = "false";
                        cod = 13908;
                    }

                }
            
            retorno = "IrAlProyecto/" + idGrupo;
            //return RedirectToAction(retorno, new { code = cod });
            }
            catch
            {
                falla_xml = "true";
                retorno = "Index";
            }
            return RedirectToAction(retorno, new { code = cod });
        }
        #endregion

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}