﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using dl = ClassLibrary;

namespace INI.Controllers
{
    //[Authorize(Roles = "Administrator,Maestro")]
    [Authorize]
    public class ParadigmaEpistemologicoController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: ParadigmaEpistemologico
        public ActionResult Index(int code=0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            return View(db.tblParadigmaEpistemologico.ToList());
        }

        // GET: ParadigmaEpistemologico/Details/5
        public ActionResult Details(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblParadigmaEpistemologico tblParadigmaEpistemologico = db.tblParadigmaEpistemologico.Find(id);
            if (tblParadigmaEpistemologico == null)
            {
                return HttpNotFound();
            }
            return View(tblParadigmaEpistemologico);
        }

        // GET: ParadigmaEpistemologico/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View();
        }

        // POST: ParadigmaEpistemologico/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tblParadigmaEpistemologico_ID,parEpi_nombre")] tblParadigmaEpistemologico tblParadigmaEpistemologico)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblParadigmaEpistemologico tblParEpistemologico = db.tblParadigmaEpistemologico.SingleOrDefault(model => model.parEpi_nombre == tblParadigmaEpistemologico.parEpi_nombre);
                if (tblParEpistemologico == null)
                {
                    db.tblParadigmaEpistemologico.Add(tblParadigmaEpistemologico);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }

            return View(tblParadigmaEpistemologico);
        }

        // GET: ParadigmaEpistemologico/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblParadigmaEpistemologico tblParadigmaEpistemologico = db.tblParadigmaEpistemologico.Find(id);
            if (tblParadigmaEpistemologico == null)
            {
                return HttpNotFound();
            }
            return View(tblParadigmaEpistemologico);
        }

        // POST: ParadigmaEpistemologico/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tblParadigmaEpistemologico_ID,parEpi_nombre")] tblParadigmaEpistemologico tblParadigmaEpistemologico)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblParadigmaEpistemologico tblParEpistemologico = db.tblParadigmaEpistemologico.SingleOrDefault(model => model.parEpi_nombre == tblParadigmaEpistemologico.parEpi_nombre && model.tblParadigmaEpistemologico_ID != tblParadigmaEpistemologico.tblParadigmaEpistemologico_ID);
                if (tblParEpistemologico == null)
                {
                    db.Entry(tblParadigmaEpistemologico).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }
            return View(tblParadigmaEpistemologico);
        }

        // GET: ParadigmaEpistemologico/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblParadigmaEpistemologico tblParadigmaEpistemologico = db.tblParadigmaEpistemologico.Find(id);
            if (tblParadigmaEpistemologico == null)
            {
                return HttpNotFound();
            }
            return View(tblParadigmaEpistemologico);
        }

        // POST: ParadigmaEpistemologico/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            try
            {
                tblParadigmaEpistemologico tblParadigmaEpistemologico = db.tblParadigmaEpistemologico.Find(id);
                db.tblParadigmaEpistemologico.Remove(tblParadigmaEpistemologico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 1390306 });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
