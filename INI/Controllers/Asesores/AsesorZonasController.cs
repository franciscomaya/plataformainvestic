﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;

namespace INI.Controllers.Asesores
{
    [Authorize]
    public class AsesorZonasController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: AsesorZonas
        public ActionResult Index()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //return View(db.tblAsesorZona.Where(estaActivo=> estaActivo.estaActivo==true).ToList());
            return View(db.tblAsesorZona.ToList());
        }

        // GET: AsesorZonas/Details/5
        public ActionResult Details(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsesorZona tblAsesorZona = db.tblAsesorZona.Find(id);
            if (tblAsesorZona == null)
            {
                return HttpNotFound();
            }
            return View(tblAsesorZona);
        }

        // GET: AsesorZonas/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.tblMunicipio_ID = new SelectList(db.tblMunicipios.Where(m => m.idDepartamento == "52"), "idMunicipio", "NombreMunicipio");
            return View();
        }

        // POST: AsesorZonas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tblAsesorZona_ID,aseZon_nombre,aseZon_apellido,aseZon_correo,aseZon_telefono,tblEquipo_ID,tblMunicipio_ID,estaActivo")] tblAsesorZona tblAsesorZona)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.tblMunicipio_ID = new SelectList(db.tblMunicipios.Where(m => m.idDepartamento == "52"), "idMunicipio", "NombreMunicipio",tblAsesorZona.tblMunicipio_ID);
            tblAsesorZona.tblEquipo_ID = 10010; //Este Id debe estar en la tabla tblEquipos
            if (ModelState.IsValid)
            {                
                tblAsesorZona.estaActivo = true;
                db.tblAsesorZona.Add(tblAsesorZona);
                try {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {

                    if (ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key"))
                    {
                        ViewBag.SqlError = "El asesor de zona ya existe";
                    }
                    else
                    {
                        ViewBag.SqlError = ex.InnerException.InnerException.Message;
                    }

                    return View(tblAsesorZona);
                }
            }

            return View(tblAsesorZona);
        }

        // GET: AsesorZonas/Edit/5
  
        public ActionResult Edit(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsesorZona tblAsesorZona = db.tblAsesorZona.Find(id);
            if (tblAsesorZona == null)
            {
                return HttpNotFound();
            }
            ViewBag.tblMunicipio_ID = new SelectList(db.tblMunicipios.Where(m => m.idDepartamento == "52"), "idMunicipio", "NombreMunicipio",tblAsesorZona.tblMunicipio_ID);
            return View(tblAsesorZona);
        }

        // POST: AsesorZonas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tblAsesorZona_ID,aseZon_nombre,aseZon_apellido,aseZon_correo,aseZon_telefono,tblEquipo_ID,tblMunicipio_ID,estaActivo")] tblAsesorZona tblAsesorZona)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblAsesorZona.tblEquipo_ID = 10010; //Este Id debe estar en la tabla tblEquipos
            ViewBag.tblMunicipio_ID = new SelectList(db.tblMunicipios.Where(m => m.idDepartamento == "52"), "idMunicipio", "NombreMunicipio", tblAsesorZona.tblMunicipio_ID);
            if (ModelState.IsValid)
            {                
                db.Entry(tblAsesorZona).State = EntityState.Modified;
                try {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key"))
                    {
                        ViewBag.SqlError = "El asesor de zona ya existe";
                    }
                    else
                    {
                        ViewBag.SqlError = ex.InnerException.InnerException.Message;
                    }                    
                    return View(tblAsesorZona);
                }
            }
            return View(tblAsesorZona);
        }

        // GET: AsesorZonas/Delete/5
        public ActionResult Delete(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsesorZona tblAsesorZona = db.tblAsesorZona.Find(id);
            if (tblAsesorZona == null)
            {
                return HttpNotFound();
            }
            return View(tblAsesorZona);
        }

        // POST: AsesorZonas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblAsesorZona tblAsesorZona = db.tblAsesorZona.Find(id);
            db.tblAsesorZona.Remove(tblAsesorZona);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
