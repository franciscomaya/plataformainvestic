﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using System.IO;
using Microsoft.AspNet.Identity;
using dl = ClassLibrary;

namespace INI.Controllers.RecursosEducativos
{
    //[Authorize(Roles = "Administrator,Editor")]
    [Authorize]
    public class RecursosEducativosController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: /RecursosEducativos/
        public ActionResult Index(int code = 0)
        {
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            var userId= User.Identity.GetUserId();
            var tblrecursoseducativos = db.tblRecursosEducativos.Include(t => t.tblColecciones).Where(t => t.id_user.Equals(userId));
            //var tblrecursoseducativos = db.tblRecursosEducativos.ToList();
            return View(tblrecursoseducativos.ToList());
        }

        public ActionResult getRecByCollections(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tblrecursoseducativos = db.tblRecursosEducativos.Where(t => t.id_coleccion == id);

            if (tblrecursoseducativos == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Views/RecursosEducativos/_RecursosEducativos.cshtml", tblrecursoseducativos);

        }

        public ActionResult getResourcesDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tblrecursoseducativos = db.tblRecursosEducativos.Find(id);

            if (tblrecursoseducativos == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/RecursosEducativos/Details.cshtml", tblrecursoseducativos);

        }

        // GET: /RecursosEducativos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRecursosEducativos tblrecursoseducativos = db.tblRecursosEducativos.Find(id);
            if (tblrecursoseducativos == null)
            {
                return HttpNotFound();
            }
            return View("~/Views/RecursosEducativos/Detalles.cshtml", tblrecursoseducativos);
        }
        // Item value 0 para validacion select colecciones
        private SelectList AddFirstItemCol(SelectList list)
        {
            List<SelectListItem> _list = list.ToList();
            _list.Insert(0, new SelectListItem() { Value = "0", Text = ".:Colección:." });
            return new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text", list.SelectedValue);
        }
        // Item value 0 para validacion select idiomas
        private SelectList AddFirstItemIdioma(SelectList list)
        {
            List<SelectListItem> _list = list.ToList();
            _list.Insert(0, new SelectListItem() { Value = "0", Text = ".:Idioma:." });
            return new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text", list.SelectedValue);
        }
        // GET: /RecursosEducativos/Create
        public ActionResult Create()
        {
            ViewBag.id_coleccion = AddFirstItemCol(new SelectList(db.tblColecciones, "id_coleccion", "nom_coleccion"));
            ViewBag.tblIdiomas_ID = AddFirstItemIdioma(new SelectList(db.tblIdiomas, "tblIdiomas_ID", "idi_nombre"));
            return View();
        }

        // POST: /RecursosEducativos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_rec_educativo,nom_rec_educativo,desc_rec_educativo,icono_rec_educativo,archivo_rec_educativo,id_coleccion,autor_rec_educativo,id_user,palabra_clave,tblIdiomas_ID")] tblRecursosEducativos tblrecursoseducativos)
        {
            if (ModelState.IsValid)
            {
            
           
                string folderPath = "";
                string PathforDB = "";
                string filename = "";
                string savedfileName;

                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;

                    folderPath = Server.MapPath("~/Archivos/RecursosEducativos/");
                    PathforDB = "/Archivos/RecursosEducativos/";

                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);

                    filename = string.Format("{0}-{1}",
                          DateTime.Now.ToString("ddMMyyyyHHmmss"),
                          Path.GetFileName(hpf.FileName));

                    savedfileName = folderPath + filename;

                   hpf.SaveAs(savedfileName);

                    tblrecursoseducativos.archivo_rec_educativo = PathforDB + filename;
                    tblrecursoseducativos.icono_rec_educativo = "/images/icono_agenda.png";
                    //tblrecursoseducativos.autor_rec_educativo = User.Identity.GetUserName();
                    //tblrecursoseducativos.id_user = User.Identity.GetUserId();

                }
                
                    db.tblRecursosEducativos.Add(tblrecursoseducativos);
                    db.SaveChanges();
               
                }
            return RedirectToAction("Index");
        }


        // GET: /RecursosEducativos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRecursosEducativos tblrecursoseducativos = db.tblRecursosEducativos.Find(id);
            if (tblrecursoseducativos == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_coleccion =  AddFirstItemCol(new SelectList(db.tblColecciones, "id_coleccion", "nom_coleccion", tblrecursoseducativos.id_coleccion));
            ViewBag.tblIdiomas_ID = AddFirstItemIdioma(new SelectList(db.tblIdiomas, "tblIdiomas_ID", "idi_nombre", tblrecursoseducativos.tblIdiomas_ID));
            return View(tblrecursoseducativos);
        }

        // POST: /RecursosEducativos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_rec_educativo,nom_rec_educativo,desc_rec_educativo,icono_rec_educativo,archivo_rec_educativo,id_coleccion,autor_rec_educativo,id_user,palabra_clave,tblIdiomas_ID,conservar_archivo")] tblRecursosEducativos tblrecursoseducativos)
        {
            string conservar_archivo = Request.Form["conservar_archivo"];
            //tblrecursoseducativos.archivo_rec_educativo = Request.Form["archivo_rec_educativo1"];           
        
            if (ModelState.IsValid)
            {

                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                    {
                        db.Entry(tblrecursoseducativos).State = EntityState.Modified;
                        db.SaveChanges();
                        continue;
                    }
                        

                    string folderPath = Server.MapPath("~/Archivos/RecursosEducativos/");
                    string PathforDB = "/Archivos/RecursosEducativos/";

                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);

                    string filename = string.Format("{0}-{1}",
                          DateTime.Now.ToString("ddMMyyyyHHmmss"),
                          Path.GetFileName(hpf.FileName));

                    string savedfileName = folderPath + filename;

                    hpf.SaveAs(savedfileName);

                    tblrecursoseducativos.archivo_rec_educativo = PathforDB + filename;
                    if (conservar_archivo == "no")
                    {
                        tblrecursoseducativos.archivo_rec_educativo = Request.Form["archivo_rec_educativo1"];
                    }
                    tblrecursoseducativos.icono_rec_educativo = "/images/icono_agenda.png";
                    tblrecursoseducativos.autor_rec_educativo = User.Identity.GetUserName();
                    tblrecursoseducativos.id_user = User.Identity.GetUserId();

                    db.Entry(tblrecursoseducativos).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }
            return RedirectToAction("Index");
        }

        // GET: /RecursosEducativos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRecursosEducativos tblrecursoseducativos = db.tblRecursosEducativos.Find(id);
            if (tblrecursoseducativos == null)
            {
                return HttpNotFound();
            }
            return View(tblrecursoseducativos);
        }

        // POST: /RecursosEducativos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblRecursosEducativos tblrecursoseducativos = db.tblRecursosEducativos.Find(id);
            int c= 13909;
            int ide = int.Parse(Request.Form["rec"]);
            string autor = Request.Form["autor"];
            if (id == ide && autor == tblrecursoseducativos.autor_rec_educativo)
            {
                db.tblRecursosEducativos.Remove(tblrecursoseducativos);
                db.SaveChanges();
                c = 13910;
            }
            //tblRecursosEducativos tblrecursoseducativos = db.tblRecursosEducativos.Find(id);
            //db.tblRecursosEducativos.Remove(tblrecursoseducativos);
            //db.SaveChanges();
            return RedirectToAction("Index", new { code =  c});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
