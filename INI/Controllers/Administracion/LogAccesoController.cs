﻿using INI.Models.DataBase;
using INI.Models.DataTableAjaxModels;
using INI.Models.DataTableAjaxModels.Binder;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using INI.Extensions.Controllers;
using INI.Models.RangeFechas;
using INI.Extensions.ActionResults;
using INI.Models.Admin;
using System.Net;
using ExcelReporLibrary;
using System.Collections.Generic;
using ExcelReporLibrary.Models;
using INI.Models.Mymodels;
using Newtonsoft.Json;

namespace INI.Controllers.Administracion
{
    //[Authorize(Roles = "Administrator")]
    [Authorize]
    public class LogAccesoController : NewtomSofController
    {
        private investicEntities db = new investicEntities();

        // GET: LogAcceso
        public ActionResult Index()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var statelog = db.tblConfiguration.Where(m => m.operation == 1 && m.element == 1).OrderByDescending(m => m.DateOperation).Select(m => m.IsEnabled).FirstOrDefault();
            ViewBag.statelog = statelog;

            return View();
        }

        public ActionResult Navigation(int id = 0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var navigation = db.tblLogAcceso.Where(m => m.id == id).FirstOrDefault();
            List<NavegationModel> navigationobj= JsonConvert.DeserializeObject<List<NavegationModel>>(navigation.Navegacion).OrderByDescending(m=>m.ImputDateUrl).ToList();
            String username=db.AspNetUsers.Where(model => model.UserName == navigation.Usuario).Select(model => new { Name = model.Name + " " + model.SureName }).Take(1).FirstOrDefault().Name;
            NavigationLog nl = new NavigationLog() { id = navigation.id, Rol = navigation.Rol, Usuario = username, IP = navigation.IP, Latitud = navigation.Latitud, Longitud = navigation.Longitud, Altitud = navigation.Longitud, FechaInicioSesion = navigation.FechaInicioSesion, FechaCierreSesion = navigation.FechaCierreSesion, navigations = navigationobj };
            return View(nl);
        }

        public ActionResult ChangeStateLog(bool state)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            db.tblConfiguration.Add(new tblConfiguration() { DateOperation = System.DateTime.Now, element = 1, operation = 1, IsEnabled = state });
            db.SaveChanges();
            String rta = "Ok";
            return Json(rta, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetReport([Bind(Include = "StartDate,EndDate,IsRole,IsAdvancedSearch,criterion,ReportType")]ReportModel model)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                switch (model.ReportType)
                {
                    case 1:
                        string filePath = Server.MapPath("/ExcelTemplates/ReportLog.xlsx");
                        ExcelReportModel rm = new ExcelReportModel();
                        //List<>

                        var datos = db.tblLogAcceso.Where(m => m.FechaInicioSesion >= model.StartDate && m.FechaInicioSesion <= model.EndDate).OrderByDescending(m => m.FechaInicioSesion).
                            Select(m =>
                            new LogRow() {
                                Rol = m.Rol,
                                Usuario = m.Usuario ,
                                Nombre= db.AspNetUsers.Where(model2 => model2.UserName == m.Usuario).Select(model2 => new { Name = model2.Name + " " + model2.SureName }).Take(1).FirstOrDefault().Name,
                                Altitud = m.Altitud,
                                Latitud = m.Latitud,
                                IP = m.IP,
                                FechaCierreSesion = m.FechaCierreSesion.Value,
                                FechaInicioSesion = m.FechaInicioSesion.Value
                            });


                        if (model.IsAdvancedSearch)
                        {
                            if (model.IsRole)
                            {
                                datos = db.tblLogAcceso.Where(m => m.FechaInicioSesion >= model.StartDate && m.FechaInicioSesion <= model.EndDate && m.Rol.Contains(model.criterion)).OrderByDescending(m => m.FechaInicioSesion).
                                    Select(m => 
                                    new LogRow() {
                                        Rol = m.Rol,
                                        Usuario = m.Usuario,
                                        Nombre = db.AspNetUsers.Where(model2 => model2.UserName == m.Usuario).Select(model2 => new { Name = model2.Name + " " + model2.SureName }).Take(1).FirstOrDefault().Name,
                                        Altitud = m.Altitud,
                                        Latitud = m.Latitud,
                                        IP = m.IP,
                                        FechaCierreSesion = m.FechaCierreSesion.Value,
                                        FechaInicioSesion = m.FechaInicioSesion.Value
                                    });
                            }
                            else
                            {
                                datos = db.tblLogAcceso.Where(m => m.FechaInicioSesion >= model.StartDate && m.FechaInicioSesion <= model.EndDate ).OrderByDescending(m => m.FechaInicioSesion).
                                    Select(m => 
                                    new LogRow() {
                                        Rol = m.Rol,
                                        Usuario = m.Usuario,
                                        Nombre = db.AspNetUsers.Where(model2 => model2.UserName == m.Usuario).Select(model2 => new { Name = model2.Name + " " + model2.SureName }).Take(1).FirstOrDefault().Name,
                                        Altitud = m.Altitud,
                                        Latitud = m.Latitud,
                                        IP = m.IP,
                                        FechaCierreSesion = m.FechaCierreSesion.Value,
                                        FechaInicioSesion = m.FechaInicioSesion.Value
                                    }).Where(m=>m.Nombre.Contains(model.criterion));
                            }
                        }
                        rm.LstLog = datos.ToList();
                        return new ExcelResult(ExcelReport.HistoricReport(filePath, rm), "Report " + System.DateTime.Now);
                    //return null;
                    case 2:
                        return new PDFResult(INI.LogReport.Reporte(model), "reporteLogAcceso");
                    default:
                        return RedirectToAction("Index");

                }

            }
            else return RedirectToAction("Index");
        }


        
        public ActionResult GetNavigateReport(int id, int reporttype=1)
        {
            switch (reporttype)
            {
                case 1:
                    string filePath = Server.MapPath("/ExcelTemplates/Navigate.xlsx");
                    var navigation = db.tblLogAcceso.Where(m => m.id == id).FirstOrDefault();
                    
                    List<NavegationModel_> navigationobj = JsonConvert.DeserializeObject<List<NavegationModel_>>(navigation.Navegacion).OrderByDescending(m => m.ImputDateUrl).ToList();
                    NavigationLog_ nl = new NavigationLog_() { id = navigation.id, Rol = navigation.Rol, Usuario = AspNetUsers.GetName(navigation.Usuario), IP = navigation.IP, Latitud = navigation.Latitud, Longitud = navigation.Longitud, Altitud = navigation.Longitud, FechaInicioSesion = navigation.FechaInicioSesion, FechaCierreSesion = navigation.FechaCierreSesion, navigations = navigationobj };
                    return new ExcelResult(ExcelReport.HistoricNavigateReport(filePath, nl), "Report " + System.DateTime.Now);
                case 2:
                    return null;
                default:
                    return null;
                    
            }

            
        }
        



        [HttpPost]
        public ActionResult getLogAcceso(String Dates, [ModelBinder(typeof(RqDatatableModelBinder))]RequestModel requestModel)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ResponseModel<LogAcceso> responseModel = new ResponseModel<LogAcceso>();
            Type dat = typeof(LogAcceso);

            //---Parametros de control
            string conditions = "";
            string orderbyparameters = "";

            string[] DatesString = Dates.Split('-');

            DateTime logenddate = DateTime.Now;
            DateTime logstartdate = DateTime.Now.AddMonths(-1);
            DateTime nowdate = DateTime.Now;

            if (DatesString.Length == 2) {
                string[] datestartparts = DatesString[0].Split('/');
                string[] datendparts = DatesString[1].Split('/');

                try
                {
                    logstartdate = new DateTime(int.Parse(datestartparts[2]),int.Parse(datestartparts[1]),int.Parse(datestartparts[0]),nowdate.Hour,nowdate.Minute,nowdate.Second);
                    logenddate = new DateTime(int.Parse(datendparts[2]), int.Parse(datendparts[1]), int.Parse(datendparts[0]),nowdate.Hour,nowdate.Minute,nowdate.Second);
                }
                catch (Exception)
                {

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }



            var properties = dat.GetProperties();
            //TimeSpan ts = logenddate - logstartdate;
            //if(ts.Days>31)return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //new (Name=model.Name+" "+model.LastName)  
            var q = db.tblLogAcceso.Where(m => m.FechaInicioSesion >= logstartdate && m.FechaInicioSesion <= logenddate).Select(m => new LogAcceso()
            {
                Rol = m.Rol,
                Usuario = m.Usuario,
                Nombre = db.AspNetUsers.Where(model => model.UserName == m.Usuario).Select(model => new {Name=model.Name+" "+model.SureName }).Take(1).FirstOrDefault().Name,
                Latitud = m.Latitud,
                Longitud = m.Longitud,
                Altitud = m.Altitud,
                IP = m.IP,
                FechaCierreSesion = m.FechaCierreSesion,
                FechaInicioSesion = m.FechaInicioSesion,
                Image = m.FechaInicioSesion == m.FechaCierreSesion ? "<img src=\"/images/true.png\" /><a href=\"LogAcceso/Navigation/" + m.id+"\">Navegación</a>" : "<img src=\"/images/false.png\" /><a href=\"LogAcceso/Navigation/" + m.id + "\">Navegación</a>"


            }
            );

            if (!String.IsNullOrEmpty(requestModel.Search.Value))
            {
                string searchPhrase = requestModel.Search.Value;
                int i = 0;
                int n = properties.Count();

                foreach (var property in properties)
                {
                    if (property.PropertyType.Name == "DateTime")
                    {
                        i++;
                        continue;
                    }
                    if (property.Name == "id" || property.Name == "Image")
                    {
                        i++;
                        continue;
                    }
                    if (property.PropertyType.Name == "String") conditions += string.Format("{0}.Contains(\"{1}\")", property.Name, searchPhrase);
                    if (i < n - 4) conditions += " || ";
                    i++;
                }
            }

            if (requestModel.Orders.ToList()[0].Column >= 0)
            {
                int column = requestModel.Orders.ToList()[0].Column;
                string key = requestModel.Columns.ToList()[column].Data;
                string val = requestModel.Orders.ToList()[0].Dir;
                string direction = val == "desc" ? "descending" : "";
                orderbyparameters = string.Format("{0} {1}", key, direction);
            }

            // Build Response 

            responseModel.recordsTotal = db.tblLogAcceso.Count();
            responseModel.draw = requestModel.Draw;

            if (conditions != "" && orderbyparameters != "")
            {
                //var q = db.AspNetUsers.Where(conditions).OrderBy(orderbyparameters).Skip(requestModel.Start).Take(requestModel.Length).
                //var q2 = q.Where(string.Format(conditions, requestModel.Search.Value)).OrderBy(orderbyparameters);
                var q2 = q.Where(conditions).OrderBy(orderbyparameters);
                responseModel.recordsFiltered = q2.Count();
                responseModel.data = q2.Skip(requestModel.Start).Take(requestModel.Length).ToList();
                return Json(responseModel, JsonRequestBehavior.AllowGet);
            }
            else if (conditions == "" && orderbyparameters != "")
            {
                var q2 = q.OrderBy(orderbyparameters).Skip(requestModel.Start).Take(requestModel.Length);
                responseModel.recordsFiltered = q.Count();
                responseModel.data = q2.ToList();
                return Json(responseModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                responseModel.recordsFiltered = responseModel.recordsTotal;
                responseModel.data = q.OrderByDescending(m => m.FechaInicioSesion).Skip(requestModel.Start).Take(requestModel.Length).ToList();

                return Json(responseModel, JsonRequestBehavior.AllowGet);
            }

        }
    }
}