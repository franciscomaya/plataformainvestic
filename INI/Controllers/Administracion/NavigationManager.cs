﻿using INI.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using INI.Models.Mymodels;
using Newtonsoft.Json;

namespace INI.Controllers.Administracion
{
    public static class NavigationManager
    {
        public static void registrerNewUrl(string UserName, string URLnavigation)
        {
            investicEntities db = new investicEntities();
            var idUser = UserName;
            var statelog = db.tblConfiguration.Where(m => m.operation == 1 && m.element == 1).OrderByDescending(m => m.DateOperation).Select(m => new { m.IsEnabled, m.DateOperation }).FirstOrDefault();

            tblLogAcceso tbllogAcceso = (from m in db.tblLogAcceso where m.Usuario == idUser orderby m.FechaInicioSesion descending select m).Take(1).FirstOrDefault();


            if (statelog.IsEnabled || (tbllogAcceso != null && !statelog.IsEnabled && tbllogAcceso.FechaInicioSesion <= statelog.DateOperation))
            {
                NavegationModel newnavegation = new NavegationModel() { ImputDateUrl = System.DateTime.Now, Url = URLnavigation };
                if (String.IsNullOrEmpty(tbllogAcceso.Navegacion))
                {
                    List<NavegationModel> navegationmodels = new List<NavegationModel>();
                    navegationmodels.Add(newnavegation);
                    tbllogAcceso.Navegacion = JsonConvert.SerializeObject(navegationmodels);

                    db.Entry(tbllogAcceso).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else if(tbllogAcceso!=null)
                {
                    List<NavegationModel> navegationmodelsupdate = JsonConvert.DeserializeObject<List<NavegationModel>>(tbllogAcceso.Navegacion);
                    navegationmodelsupdate.Add(newnavegation);
                    tbllogAcceso.Navegacion = JsonConvert.SerializeObject(navegationmodelsupdate);

                    db.Entry(tbllogAcceso).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                                
            }

        }
    }
}