﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using INI.Models;
using INI.Models.DataBase;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using dl = ClassLibrary;

namespace INI.Controllers.Administracion
{
    using System.Net.Mail;
    //[Authorize]
    public class InfoGeneralController : Controller
    {
        private tblInfoGeneral dbs = new tblInfoGeneral();
        private investicEntities db = new investicEntities();
        int cod = 0;
        int id;
        string nombre;
        string campo;
        string password;
        SmtpClient server = new SmtpClient("smtp.gmail.com", 587);
        // GET: /InfoGeneral/
        [Authorize]
        public ActionResult Index(int code = 0)
        {
            Conexion_bd con = new Conexion_bd();
            List<tblInfoGeneral> lista = new List<tblInfoGeneral>();
            lista = con.listar();
            foreach (var dt in lista)
            {
                ViewBag.id = dt.id;
                ViewBag.nombre = dt.nombre;
                ViewBag.campo = dt.campo;
                ViewBag.password = dt.password;
            }
            //ViewBag.lis = datos.nombre;
            ViewData["Array"] = lista;
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);

            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "id,nombre,campo,password")] tblInfoGeneral tblInfoGeneral)
        //public ActionResult Index()
        {
            cod = 13912;
            if (VerificarCorreo(Request.Form["campo"], Request.Form["password"]))
            {
                if (ModelState.IsValid)
                {
                    string sql = "UPDATE tblInfoGeneral SET campo = '" + Request.Form["campo"] + "' , password ='" + Request.Form["password"] + "' WHERE tblInfoGeneral.id=1";
                    Conexion_bd con = new Conexion_bd();
                    if (con.Ejecutar(sql))
                    {
                        cod = 13911;
                    }
                }
            }
            else
            {
                cod = 13915;
            }
            return RedirectToAction("Index", new { code = cod });
        }
        //retornar email de contactanos
        public string email()
        {
            Conexion_bd con = new Conexion_bd();
            List<tblInfoGeneral> lista = new List<tblInfoGeneral>();
            lista = con.listar();
            foreach (var dt in lista)
            {
                id = dt.id;
                nombre = dt.nombre;
                campo = dt.campo;
            }
            return campo;
        }
        //retornar password de contactanos
        public string contrasena()
        {
            Conexion_bd con = new Conexion_bd();
            List<tblInfoGeneral> lista = new List<tblInfoGeneral>();
            lista = con.listar();
            foreach (var dt in lista)
            {
                id = dt.id;
                nombre = dt.nombre;
                password = dt.password;
            }
            return password;
        }

        //FUNCION  ENVIAR CORREOS
        public void Correos(){
            server.Credentials = new System.Net.NetworkCredential(email(),contrasena());
            server.EnableSsl = true;
        }
        //FUNCION ENVIAR EL CORREO
        public void MandarCorreo (MailMessage mensaje){
            server.Send(mensaje);
        }
        //FUNCION VERIFICAR CORREO FUNCIONAL
        public Boolean VerificarCorreo (string emailV, string passV){
            bool retorno = false;
            server.Credentials = new System.Net.NetworkCredential(emailV,passV);
            server.EnableSsl = true;
            try
            {
                //Correos Cr = new Correos();
                MailMessage mnsj = new MailMessage();

                mnsj.Subject = "Email de configuración contactános, Plataforma Tecnologíca Investic ";

                mnsj.To.Add(new MailAddress("plataformainvestic@gmail.com"));

                mnsj.From = new MailAddress(emailV, "Contáctanos");
                //mnsj.From = new MailAddress("edwnar44@gmail.com", "Contáctanos");

                /* Si deseamos Adjuntar algún archivo*/
                //mnsj.Attachments.Add(new Attachment("C:\\archivo.pdf"));

                mnsj.Body = "Estas configurando esta cuenta para recepción de correos de Contáctanos de la Plataforma Tecnológica Investic.";

                /* Enviar */
                server.Send(mnsj);
                //Enviado = true;
                retorno = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                retorno = false;
            }
            return retorno;
        }
        [Authorize]
        public ActionResult litar_link_cvemp(int code = 0)
        {
            Conexion_bd con = new Conexion_bd();
            //List<tblLinkCVEMP> datos = new List<tblLinkCVEMP>();
            List<string> datos = new List<string>();
            datos = con.listarLinkCVEMP1();
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            ViewBag.datos = datos;
            return View();             
        }
        [Authorize]
        public ActionResult editar_link(int id = 0)
        {
            Conexion_bd con = new Conexion_bd();
            List<tblLinkCVEMP> lista = new List<tblLinkCVEMP>();
            lista = con.EditLista(id);
            foreach (var dt in lista)
            {
                ViewBag.id = dt.id;
                ViewBag.categoria = dt.categoria;
                ViewBag.elemento = dt.elemento;
                ViewBag.link = dt.link;
            }
            //ViewBag.lis = datos.nombre;
            ViewData["Array"] = lista;

            return View();
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult editar_link([Bind(Include = "id,categoria,elemento,link")] tblLinkCVEMP tblLinkCVEMP)
        //public ActionResult Index()
        {
            cod = 13919;
            
                if (ModelState.IsValid)
                {
                    string sql = "UPDATE tblLinkCVEMP SET link = '" + Request.Form["link"] + "' WHERE tblLinkCVEMP.id=" + Request.Form["id"];
                    Conexion_bd con = new Conexion_bd();
                    if (con.Ejecutar(sql))
                    {
                        cod = 13918;
                    }
                }
                return RedirectToAction("litar_link_cvemp", new { code = cod });
        }

        public ActionResult return_link(int id = 0)
        {
            Conexion_bd con = new Conexion_bd();
            List<tblLinkCVEMP> lista = new List<tblLinkCVEMP>();
            lista = con.ReturLista(id);
            foreach (var dt in lista)
            {
                ViewBag.link = dt.link;
            }

            return View();
        }
        //[HttpPost]
        public  ActionResult contactosMovil()
        {
            return View();
        }
        [HttpPost]
        public ActionResult enviarMail()
        {
            string nombre = Request.Form["name"];
            string emailContacto = Request.Form["mail"];
            string motivo = "Contáctanos movil - " + Request.Form["subject"];
            string mensaje = Request.Form["message"];
            string emailPlataforma = email();
            try
            {
                Correos();

                MailMessage mnsj = new MailMessage();

                mnsj.Subject = motivo;

                mnsj.To.Add(new MailAddress(emailPlataforma));

                mnsj.From = new MailAddress(emailContacto, nombre);

                
                mnsj.Body = "<B>Nombres:</B> " + nombre + "<br /> <B>Correo electrónico:</B> "  +  emailContacto + "<BR /><B>Motivo:</B> " + motivo +"<BR /><B> Mensaje:</B> " + mensaje;
                //mnsj.Body = string.Format("Nombres: " + nombre + "Correo electrónico: " + emailContacto + " Motivo: " + motivo + " Mensaje: " + mensaje);
                mnsj.IsBodyHtml = true;
                // Enviar 
                MandarCorreo(mnsj);

                //MessageBox.Show("El Mail se ha Enviado Correctamente", "Listo!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View();
        }
	}
}
/* desde cualkier lugar llamar la funcion enviar correo
 try
{
    Correos Cr = new Correos();
    MailMessage mnsj = new MailMessage();
 
    mnsj.Subject = "Hola Mundo";
 
    mnsj.To.Add(new MailAddress("amiguito@domain.com"));
 
    mnsj.From = new MailAddress("YO@MiDominio.com", "Nombre Apellido");
 
    // Si deseamos Adjuntar algún archivo
    mnsj.Attachments.Add(new Attachment("C:\\archivo.pdf"));
 
    mnsj.Body = "  Mensaje de Prueba \n\n Enviado desde C#\n\n *VER EL ARCHIVO ADJUNTO*";
 
    // Enviar 
    Cr.MandarCorreo(mnsj);
    Enviado = true;
 
    MessageBox.Show("El Mail se ha Enviado Correctamente", "Listo!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
}
catch (Exception ex)
{
    MessageBox.Show(ex.ToString());
}
 */