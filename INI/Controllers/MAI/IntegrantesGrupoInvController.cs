﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using INI.Models.DataTableAjaxModels.Binder;
using INI.Models.DataTableAjaxModels;
using INI.Models.Admin;
using System.Linq.Dynamic;

namespace INI.Controllers.Investigacion
{
    //[Authorize(Roles = "Administrator,Maestro")]
    [Authorize]
    public class IntegrantesGrupoInvController : Controller
    {
        private investicEntities db = new investicEntities();

        public ActionResult Index()//recibe el id del grupo de investigacion al que se van a vincular los integrantes
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            tblGruposInvestigacion miGrupo = (from q in db.tblGruposInvestigacion where q.tblUsuarioPlataforma_ID == idUsuario select q).FirstOrDefault();
            //tblGruposInvestigacion miGrupo = db.tblGruposInvestigacion.Find(id);
            if (miGrupo == null)
            {
                return RedirectToAction("CrearGrupoInv", "GruposInvestigacion");
            }
            return View(miGrupo);
        }

        public ActionResult Integrantes() //recibe el id del grupo de investigacion 
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            tblGruposInvestigacion miGrupo = (from q in db.tblGruposInvestigacion where q.tblUsuarioPlataforma_ID == idUsuario select q).FirstOrDefault();
            if (miGrupo == null)
            {
                return RedirectToAction("CrearGrupoInv", "GruposInvestigacion");
            }

            //var nuevosUsuarios = from t in db.tblUsuarioPlataforma
            //                     where !(from u in db.tblIntegrantesGrupoInv
            //                             where u.tblGruposInvestigacion_ID == id
            //                             select u.tblUsuarioPlataforma_ID).Contains(t.tblUsuarioPlataforma_ID)
            //                     select t;

            //ViewBag.tblGrupoInvestigacion_ID = id;
            //ViewBag.tblUsuarioPlataforma_ID = new SelectList(nuevosUsuarios, "tblUsuarioPlataforma_ID", "usuPla_identificacion", "usuPla_nombre");
            return View(miGrupo);
        }

        public ActionResult AgregarIntegranteGrupoInv(string idUsuario, long idGrupo)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblIntegrantesGrupoInv nuevoIntegrante = new tblIntegrantesGrupoInv();
            nuevoIntegrante.intGruInv_fechaVinculacion = DateTime.Now;
            nuevoIntegrante.tblGruposInvestigacion_ID = idGrupo;
            nuevoIntegrante.tblUsuarioPlataforma_ID = idUsuario;
            db.tblIntegrantesGrupoInv.Add(nuevoIntegrante);
            db.SaveChanges();
            return RedirectToAction("Integrantes");
        }

        public ActionResult getUsers(int id,[ModelBinder(typeof(RqDatatableModelBinder))]RequestModel requestModel)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ResponseModel<UserTeacher> responseModel = new ResponseModel<UserTeacher>();
            Type dat = typeof(UserTeacher);

            //---Parametros de control
            string conditions = "";
            string orderbyparameters = "";

            var properties = dat.GetProperties();

            var Maestros = db.AspNetUserRoles.Where(m => m.AspNetRoles.Name == "Maestro").Select(m => m.AspNetUsers);
            /*var Maestros = from m in db.AspNetUsers 
                           join ur in db.AspNetUserRoles on m.Id equals ur.UserId 
                           join r in db.AspNetRoles on ur.RoleId equals r.Id 
                           where r.Name == "Maestro" 
                           select m;*/
            var UsuariosEnGrupo = db.tblIntegrantesGrupoInv.Where(m => m.tblGruposInvestigacion_ID == id).Select(m => m.tblUsuarioPlataforma_ID);
            

            var Usuarios = from u in Maestros
                           where  !UsuariosEnGrupo.Contains(u.Id)
                           select u;

            //q = q.Where(m => m.Rol == "Maestro");

            //var q = db.AspNetUsers.Select(m => new UserTeacher()
            var q = Usuarios.Select(m => new UserTeacher()
            {
                Identification = m.UserName,
                Name = m.Name,
                LastName = m.SureName,
                Ctrl = "<a href=\"/IntegrantesGrupoInv/AgregarIntegranteGrupoInv?idUsuario=" + m.Id + "&idGrupo="+id+"\"class=\"button primary fg-white\">Agregar</a>"
                
            }
            );

            


            if (!String.IsNullOrEmpty(requestModel.Search.Value))
            {
                string searchPhrase = requestModel.Search.Value;
                int i = 0;
                int n = properties.Count();

                foreach (var property in properties)
                {
                    if (property.Name == "Ctrl")
                    {
                        i++;
                        continue;
                    }
                    
                    if (property.PropertyType.Name == "String") conditions += string.Format("{0}.Contains(\"{1}\")", property.Name, searchPhrase);
                    if (i < n - 2) conditions += " || ";
                    i++;
                }
            }

            if (requestModel.Orders.ToList()[0].Column >= 0)
            {
                int column = requestModel.Orders.ToList()[0].Column;
                string key = requestModel.Columns.ToList()[column].Data;
                string val = requestModel.Orders.ToList()[0].Dir;
                string direction = val == "desc" ? "descending" : "";
                orderbyparameters = string.Format("{0} {1}", key, direction);
            }

            // Build Response 

            responseModel.recordsTotal = db.AspNetUsers.Count();
            responseModel.draw = requestModel.Draw;

            if (conditions != "" && orderbyparameters != "")
            {
                //var q = db.AspNetUsers.Where(conditions).OrderBy(orderbyparameters).Skip(requestModel.Start).Take(requestModel.Length).
                var q2 = q.Where(string.Format(conditions, requestModel.Search.Value)).OrderBy(orderbyparameters);
                responseModel.recordsFiltered = q2.Count();
                responseModel.data = q2.Skip(requestModel.Start).Take(requestModel.Length).ToList();
                return Json(responseModel, JsonRequestBehavior.AllowGet);
            }
            else if (conditions == "" && orderbyparameters != "")
            {
                var q2 = q.OrderBy(orderbyparameters).Skip(requestModel.Start).Take(requestModel.Length);
                responseModel.recordsFiltered = q.Count();
                responseModel.data = q2.ToList();
                return Json(responseModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                responseModel.recordsFiltered = responseModel.recordsTotal;
                responseModel.data = q.OrderBy("Name").Skip(requestModel.Start).Take(requestModel.Length).ToList();

                return Json(responseModel, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult AgregarIntegrantes(long? id) //recibe el id del grupo de investigacion al que se van a vincular
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblGruposInvestigacion miGrupo = db.tblGruposInvestigacion.Find(id);
            if (miGrupo == null)
            {
                return HttpNotFound();
            }
            IQueryable<AspNetUsers> usuarios = from t in db.AspNetUsers
                                                        where !(from u in db.tblIntegrantesGrupoInv
                                                                where u.tblGruposInvestigacion_ID == miGrupo.tblGruposInvestigacion_ID
                                                                select u.tblUsuarioPlataforma_ID).Contains(t.Id)
                                                        select t;


            //IQueryable<tblUsuarioPlataforma> usuarios = from t in db.tblUsuarioPlataforma
            //                                            where !(from u in db.tblIntegrantesGrupoInv
            //                                                    where u.tblGruposInvestigacion_ID == miGrupo.tblGruposInvestigacion_ID
            //                                                    select u.tblUsuarioPlataforma_ID).Contains(t.tblUsuarioPlataforma_ID)
            //                                            select t;
            ViewBag.tblGruposInvestigacion_ID = id;
            return View(usuarios);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarIntegrantes([Bind(Include = "tblIntegrantesGrupoInv_ID,tblGruposInvestigacion_ID,tblUsuarioPlataforma_ID")] tblIntegrantesGrupoInv tblIntegrantesGrupoInv)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblIntegrantesGrupoInv.intGruInv_fechaVinculacion = DateTime.Now;
                db.tblIntegrantesGrupoInv.Add(tblIntegrantesGrupoInv);
                db.SaveChanges();
                tblGruposInvestigacion g = db.tblGruposInvestigacion.Find(tblIntegrantesGrupoInv.tblGruposInvestigacion_ID);
                return RedirectToAction("Integrantes");
                //return RedirectToAction("Integrantes", new { id = g.tblGruposInvestigacion_ID });
            }

            ViewBag.tblGruposInvestigacion_ID = new SelectList(db.tblGruposInvestigacion, "tblGruposInvestigacion_ID", "gruInv_nombreGrupo", tblIntegrantesGrupoInv.tblGruposInvestigacion_ID);
            return View(tblIntegrantesGrupoInv);
        }


        // GET: tblIntegrantesGrupoInvs/Delete/5
        public ActionResult Delete(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblIntegrantesGrupoInv tblIntegrantesGrupoInv = db.tblIntegrantesGrupoInv.Find(id);
            var idGrupo = tblIntegrantesGrupoInv.tblGruposInvestigacion_ID;
            db.tblIntegrantesGrupoInv.Remove(tblIntegrantesGrupoInv);
            db.SaveChanges();
            return RedirectToAction("Integrantes", "IntegrantesGrupoInv");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
