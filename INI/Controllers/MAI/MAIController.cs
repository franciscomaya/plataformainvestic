﻿//using DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;

namespace INI.Controllers.MAI
{
    //[Authorize(Roles = "Administrator,Maestro")]
    [Authorize]
    public class MAIController : Controller
    {
        private investicEntities db = new investicEntities();

        //------Acciones de la Vista Formacion y Experiencia------
        // GET: Obtiene hoja de vida y publica la información completa
        //[AllowAnonymous]
        public ActionResult FormacionYExperiencia(int tabactive=1)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            tblHojaVida hojaVida = (from q in db.tblHojaVida where q.tblUsuarioPlataforma_ID == idUsuario select q).FirstOrDefault();
            ViewData["Tab"] = tabactive;
            if (hojaVida == null)
            {
                var RowsAffected = db.Database.ExecuteSqlCommand("INSERT INTO tblHojaVida (tblUsuarioPlataforma_ID) values ({0})", idUsuario);

                hojaVida = (from q in db.tblHojaVida where q.tblUsuarioPlataforma_ID == idUsuario select q).FirstOrDefault();

                return View(hojaVida);

                //hojaVida = new tblHojaVida();
                //hojaVida.tblHojaVida_ID = 230104;
                //hojaVida.tblUsuarioPlataforma_ID = idUsuario;
                //db.tblHojaVida.Add(hojaVida);
                //db.SaveChanges();                
                
                //return RedirectToAction("EditarHojaVida", new { id = hojaVida.tblHojaVida_ID });
            }
            return View(hojaVida);
        }

        //--Acciones control de Hoja de Vida
        // GET: HojasVida/Edit/5
        public ActionResult EditarHojaVida(long? id) //Recibe id de la hoja de vida
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHojaVida tblhojavida = db.tblHojaVida.Find(id);
            if (tblhojavida == null)
            {
                return HttpNotFound();
            }
            else if (tblhojavida.AspNetUsers.UserName!=User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.FechaActual = DateTime.Now.ToString();
            return View(tblhojavida);
        }
        // POST: HojasVida/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarHojaVida([Bind(Include = "tblHojaVida_ID,hojVid_anoGradoSecundaria,hojVid_tituloSecundaria,tblUsuarioPlataforma_ID")] tblHojaVida tblHojaVida)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                long id = long.Parse(Request.Form["tblHojaVida_ID"]);
                tblHojaVida tblhojavida = db.tblHojaVida.Find(id);
                if (tblhojavida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblhojavida.hojVid_tituloSecundaria = Request.Form["hojVid_tituloSecundaria"];
                tblhojavida.hojVid_anoGradoSecundaria = DateTime.Parse(Request.Form["hojVid_anoGradoSecundaria"]);
                db.Entry(tblhojavida).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 1 });
            }
            ViewBag.tblUsuarioPlataforma_ID = AspNetUsers.GetUserId(User.Identity.Name);
            return View(tblHojaVida);
        }


        //--Acciones control de Titulos
        //GET: Adicionar Titulo
        public ActionResult AdicionarTitulo(long? id) //Recibe id de hoja de Vida
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == id select t).FirstOrDefault();
            if (hojaVida == null)
            {
                return HttpNotFound();
            }
            ViewBag.FechaActual = DateTime.Now.ToString();
            ViewBag.idHojaVida = id;
            ViewBag.idUsuarioPlataforma = hojaVida.tblUsuarioPlataforma_ID;
            ViewBag.tblNivelAcademicoEducacionSuperior_ID = new SelectList(db.tblNivelAcademicoEducacionSuperior, "tblNivelAcademicoEducacionSuperior_ID", "nivAcaEduSup_nombre");
            return View();
        }
        //POST: Adicionar Titulo
        // POST: TituloEducacionSuperiors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdicionarTitulo([Bind(Include = "tblTituloEducacionSuperior_ID,titEduSup_nombre,titEduSup_anoGraduacion,tblHojaVida_ID,tblNivelAcademicoEducacionSuperior_ID")] tblTituloEducacionSuperior tblTituloEducacionSuperior)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {

                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var titulos = db.tblTituloEducacionSuperior.Where(m => m.tblHojaVida.AspNetUsers.Id == userid).Select(m => m.titEduSup_nombre).ToList();
                if (titulos.Contains(tblTituloEducacionSuperior.titEduSup_nombre))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblTituloEducacionSuperior.tblHojaVida_ID);
                    ViewBag.tblNivelAcademicoEducacionSuperior_ID = new SelectList(db.tblNivelAcademicoEducacionSuperior, "tblNivelAcademicoEducacionSuperior_ID", "nivAcaEduSup_nombre", tblTituloEducacionSuperior.tblNivelAcademicoEducacionSuperior_ID);
                    ViewBag.SqlError = "El título ya existe";
                    return View(tblTituloEducacionSuperior);
                }
                db.tblTituloEducacionSuperior.Add(tblTituloEducacionSuperior);

                db.SaveChanges();

                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblTituloEducacionSuperior.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 2 });

            }

            ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblTituloEducacionSuperior.tblHojaVida_ID);
            ViewBag.tblNivelAcademicoEducacionSuperior_ID = new SelectList(db.tblNivelAcademicoEducacionSuperior, "tblNivelAcademicoEducacionSuperior_ID", "nivAcaEduSup_nombre", tblTituloEducacionSuperior.tblNivelAcademicoEducacionSuperior_ID);
            return View(tblTituloEducacionSuperior);
        }
        //GET: Editar Titulo
        public ActionResult ModificarTitulo(long? id) //Recibe id del titulo
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTituloEducacionSuperior tblTituloEducacionSuperior = db.tblTituloEducacionSuperior.Find(id);
            if (tblTituloEducacionSuperior == null)
            {
                return HttpNotFound();
            }
            if (tblTituloEducacionSuperior.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.idHojaVida = tblTituloEducacionSuperior.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblTituloEducacionSuperior.tblHojaVida.tblUsuarioPlataforma_ID;
            ViewBag.tblNivelAcademicoEducacionSuperior_ID = new SelectList(db.tblNivelAcademicoEducacionSuperior, "tblNivelAcademicoEducacionSuperior_ID", "nivAcaEduSup_nombre", tblTituloEducacionSuperior.tblNivelAcademicoEducacionSuperior_ID);
            return View(tblTituloEducacionSuperior);
        }
        // POST: TituloEducacionSuperiors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarTitulo([Bind(Include = "tblTituloEducacionSuperior_ID,titEduSup_nombre,titEduSup_anoGraduacion,tblHojaVida_ID,tblNivelAcademicoEducacionSuperior_ID")] tblTituloEducacionSuperior tblTituloEducacionSuperior)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var titulos = db.tblTituloEducacionSuperior.Where(m => m.tblHojaVida.AspNetUsers.Id == userid && m.tblTituloEducacionSuperior_ID!=tblTituloEducacionSuperior.tblTituloEducacionSuperior_ID).Select(m => m.titEduSup_nombre).ToList();
                if (titulos.Contains(tblTituloEducacionSuperior.titEduSup_nombre))
                {
                    ViewBag.idHojaVida = tblTituloEducacionSuperior.tblHojaVida_ID;
                    ViewBag.idUsuarioPlataforma = userid;
                    ViewBag.tblNivelAcademicoEducacionSuperior_ID = new SelectList(db.tblNivelAcademicoEducacionSuperior, "tblNivelAcademicoEducacionSuperior_ID", "nivAcaEduSup_nombre", tblTituloEducacionSuperior.tblNivelAcademicoEducacionSuperior_ID);
                    ViewBag.SqlError = "El título ya existe";
                    return View(tblTituloEducacionSuperior);
                }
                long id = long.Parse(Request.Form["tblTituloEducacionSuperior_ID"]);
                tblTituloEducacionSuperior tblTitEducSup = db.tblTituloEducacionSuperior.Find(id);
                
                if (tblTitEducSup.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblTitEducSup.titEduSup_nombre = Request.Form["titEduSup_nombre"];
                var fechaGrado = Request.Form["titEduSup_anoGraduacion"];
                if (fechaGrado != "")
                {
                    tblTitEducSup.titEduSup_anoGraduacion = DateTime.Parse(Request.Form["titEduSup_anoGraduacion"]);
                }
                tblTitEducSup.tblNivelAcademicoEducacionSuperior_ID = long.Parse(Request.Form["tblNivelAcademicoEducacionSuperior_ID"]);
                db.Entry(tblTitEducSup).State = EntityState.Modified;
                db.SaveChanges();

                tblHojaVida hojaVida = (from t in db.tblTituloEducacionSuperior where t.tblTituloEducacionSuperior_ID == tblTituloEducacionSuperior.tblTituloEducacionSuperior_ID select t.tblHojaVida).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 2 });
            }
            ViewBag.idHojaVida = tblTituloEducacionSuperior.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = AspNetUsers.GetUserId(User.Identity.Name);
            ViewBag.tblNivelAcademicoEducacionSuperior_ID = new SelectList(db.tblNivelAcademicoEducacionSuperior, "tblNivelAcademicoEducacionSuperior_ID", "nivAcaEduSup_nombre", tblTituloEducacionSuperior.tblNivelAcademicoEducacionSuperior_ID);
            return View(tblTituloEducacionSuperior);
        }

        public ActionResult BorrarTitulo(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblTituloEducacionSuperior miTitulo = db.tblTituloEducacionSuperior.Find(id);
            if (miTitulo.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.tblTituloEducacionSuperior.Remove(miTitulo);
            db.SaveChanges();
            return RedirectToAction("FormacionYExperiencia", new { tabactive = 2 });
        }


        //--Acciones control de Idiomas
        //GET: Adicionar Idioma
        public ActionResult AdicionarIdioma(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria");
            var listaIdiomas = from t in db.tblNivelIdioma where t.tblHojaVida_ID == id select t;
            List<tblIdiomas> tabla = new List<tblIdiomas>();
            foreach (var item in db.tblIdiomas)
            {
                bool esMiIdioma = true;
                foreach (var item2 in listaIdiomas)
                {
                    if (item.tblIdiomas_ID == item2.tblIdiomas_ID)
                    {
                        esMiIdioma = false;
                        break;
                    }
                }
                if (esMiIdioma) tabla.Add(item);
            }

            ViewBag.tblIdiomas_ID = new SelectList(tabla, "tblIdiomas_ID", "idi_nombre");
            ViewBag.idHojaVida = id;
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel");
            //ViewBag.idUsuarioPlataforma = from t in db.tblHojaVida where t.tblHojaVida_ID == id select t.tblUsuarioPlataforma_ID;

            tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == id select t).FirstOrDefault();
            if (hojaVida == null)
            {
                return HttpNotFound();
            }
            ViewBag.idUsuarioPlataforma = hojaVida.tblUsuarioPlataforma_ID;
            return View();
        }
        //POST: Adicionar Idioma
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdicionarIdioma([Bind(Include = "tblNivelIdioma_ID,tblHojaVida_ID,tblIdiomas_ID,tblNivel_ID")] tblNivelIdioma tblNivelIdioma)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                db.tblNivelIdioma.Add(tblNivelIdioma);
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblNivelIdioma.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 3 });
            }
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel");
            ViewBag.idHojaVida = tblNivelIdioma.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblNivelIdioma.tblHojaVida.tblUsuarioPlataforma_ID;
            //ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblNivelIdioma.tblHojaVida_ID);
            ViewBag.tblIdiomas_ID = new SelectList(db.tblIdiomas, "tblIdiomas_ID", "idi_nombre", tblNivelIdioma.tblIdiomas_ID);
            return View(tblNivelIdioma);
        }
        // GET: NivelIdiomas/Edit/5
        public ActionResult ModificarIdioma(long? id) //Recibe id del NivelIdioma
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }            
            tblNivelIdioma tblNivelIdioma = db.tblNivelIdioma.Find(id);
            if (tblNivelIdioma == null)
            {
                return HttpNotFound();
            }
            if (tblNivelIdioma.tblHojaVida.AspNetUsers.UserName!= User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.idHojaVida = tblNivelIdioma.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblNivelIdioma.tblHojaVida.tblUsuarioPlataforma_ID;
            //ViewBag.tblIdiomas_ID = new SelectList(db.tblIdiomas, "tblIdiomas_ID", "idi_nombre", tblNivelIdioma.tblIdiomas_ID);
            ViewBag.idIdioma = tblNivelIdioma.tblIdiomas_ID;
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel", tblNivelIdioma.tblNivel_ID);
            return View(tblNivelIdioma);
        }
        // POST: NivelIdiomas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarIdioma([Bind(Include = "tblNivelIdioma_ID,tblHojaVida_ID,tblIdiomas_ID,tblNivel_ID")] tblNivelIdioma tblNivelIdioma)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                long id = long.Parse(Request.Form["tblNivelIdioma_ID"]);
                tblNivelIdioma tblNivIdi = db.tblNivelIdioma.Find(id);
                if (tblNivIdi.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }                
                tblNivIdi.tblNivel_ID = long.Parse(Request.Form["tblNivel_ID"]);
                db.Entry(tblNivIdi).State = EntityState.Modified;
                db.SaveChanges();

                tblHojaVida hojaVida = (from t in db.tblNivelIdioma where t.tblNivelIdioma_ID == tblNivelIdioma.tblNivelIdioma_ID select t.tblHojaVida).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 3 });
            }
            ViewBag.idHojaVida = tblNivelIdioma.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblNivelIdioma.tblHojaVida.tblUsuarioPlataforma_ID;
            //ViewBag.tblIdiomas_ID = new SelectList(db.tblIdiomas, "tblIdiomas_ID", "idi_nombre", tblNivelIdioma.tblIdiomas_ID);
            ViewBag.idIdioma = tblNivelIdioma.tblIdiomas_ID;
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel");
            return View(tblNivelIdioma);
        }

        public ActionResult BorrarIdioma(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblNivelIdioma miIdioma = db.tblNivelIdioma.Find(id);
            if (miIdioma.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.tblNivelIdioma.Remove(miIdioma);
            db.SaveChanges();
            return RedirectToAction("FormacionYExperiencia", new { tabactive = 3 });
        }


        //--Acciones control de Lenguas
        //GET: Adicionar Lengua
        public ActionResult AdicionarLengua(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var listaLenguas = from t in db.tblNivelLengua where t.tblHojaVida_ID == id select t;
            List<tblLenguas> tabla = new List<tblLenguas>();
            foreach (var item in db.tblLenguas)
            {
                bool esMiLengua = true;
                foreach (var item2 in listaLenguas)
                {
                    if (item.tblLenguas_ID == item2.tblLenguas_ID)
                    {
                        esMiLengua = false;
                        break;
                    }
                }
                if (esMiLengua) tabla.Add(item);
            }

            ViewBag.tblLenguas_ID = new SelectList(tabla, "tblLenguas_ID", "len_nombre");
            ViewBag.idHojaVida = id;

            tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == id select t).FirstOrDefault();
            if (hojaVida == null)
            {
                return HttpNotFound();
            }
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel");
            ViewBag.idUsuarioPlataforma = hojaVida.tblUsuarioPlataforma_ID;
            return View();
        }
        //POST: Adicionar Lengua
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult AdicionarLengua([Bind(Include = "tblNivelLengua_ID,tblHojaVida_ID,tblLenguas_ID,tblNivel_ID")] tblNivelLengua tblNivelLengua)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {                
                db.tblNivelLengua.Add(tblNivelLengua);
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblNivelLengua.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 4 });
            }
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel");
            //ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblNivelIdioma.tblHojaVida_ID);
            ViewBag.tblLenguas_ID = new SelectList(db.tblLenguas, "tblLenguas_ID", "len_nombre", tblNivelLengua.tblLenguas_ID);
            return View(tblNivelLengua);
        }
        // GET: NivelIdiomas/Edit/5
        public ActionResult ModificarLengua(long? id) //Recibe id del NivelLengua
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblNivelLengua tblNivelLengua = db.tblNivelLengua.Find(id);
            if (tblNivelLengua == null)
            {
                return HttpNotFound();
            }
            if (tblNivelLengua.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel", tblNivelLengua.tblNivel_ID);
            ViewBag.idHojaVida = tblNivelLengua.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblNivelLengua.tblHojaVida.tblUsuarioPlataforma_ID;
            //ViewBag.tblLenguas_ID = new SelectList(db.tblLenguas, "tblLenguas_ID", "len_nombre", tblNivelLengua.tblLenguas_ID);
            ViewBag.idLengua = tblNivelLengua.tblLenguas_ID;
            return View(tblNivelLengua);
        }
        // POST: NivelIdiomas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarLengua([Bind(Include = "tblNivelLengua_ID,tblHojaVida_ID,tblLenguas_ID,tblNivel_ID")] tblNivelLengua tblNivelLengua)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                long id = long.Parse(Request.Form["tblNivelLengua_ID"]);
                tblNivelLengua tblNivLen= db.tblNivelLengua.Find(id);

                if (tblNivLen.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblNivLen.tblNivel_ID = long.Parse(Request.Form["tblNivel_ID"]);
                db.Entry(tblNivLen).State = EntityState.Modified;
                db.SaveChanges();

                tblHojaVida hojaVida = (from t in db.tblNivelLengua where t.tblNivelLengua_ID == tblNivelLengua.tblNivelLengua_ID select t.tblHojaVida).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("FormacionYExperiencia", new { tabactive = 4 });
            }
            ViewBag.tblNivel_ID = new SelectList(db.tblNivel, "tblNivel_ID", "tblNivel_nivel");
            ViewBag.idHojaVida = tblNivelLengua.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblNivelLengua.tblHojaVida.tblUsuarioPlataforma_ID;
            //ViewBag.tblIdiomas_ID = new SelectList(db.tblLenguas, "tblLenguas_ID", "len_nombre", tblNivelLengua.tblLenguas_ID);
            ViewBag.idLengua = tblNivelLengua.tblLenguas_ID;
            return View(tblNivelLengua);
        }

        public ActionResult BorrarLengua(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblNivelLengua miLengua = db.tblNivelLengua.Find(id);
            if (miLengua.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.tblNivelLengua.Remove(miLengua);
            db.SaveChanges();
            return RedirectToAction("FormacionYExperiencia", new { tabactive = 4 });
        }


        //----Acciones de la Vista Investigacion------
        public ActionResult Investigacion(int tabexpactive=1)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var idUsuario = AspNetUsers.GetUserId(User.Identity.Name);
            tblHojaVida hojaVida = (from q in db.tblHojaVida where q.tblUsuarioPlataforma_ID == idUsuario select q).FirstOrDefault();
            ViewData["Tab"] = tabexpactive;
            if (hojaVida == null)
            {
                hojaVida = new tblHojaVida();
                hojaVida.tblUsuarioPlataforma_ID = idUsuario;
                db.tblHojaVida.Add(hojaVida);
                db.SaveChanges();
            }

            return View(hojaVida);
        }
        //--Acciones control de Proyectos
        // GET: ExperienciaProyectos/Create
        public ActionResult AdicionarProyecto(long? id)// Recibe id de la hoja de vida
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == id select t).FirstOrDefault();
            if (hojaVida == null)
            {
                return HttpNotFound();
            }
            ViewBag.idHojaVida = id;
            ViewBag.idUsuarioPlataforma = hojaVida.tblUsuarioPlataforma_ID;
            return View();
        }
        // POST: ExperienciaProyectos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdicionarProyecto([Bind(Include = "tblExperienciaProyectos_ID,exppro_tituloProyecto,exppro_anoTerminacion,tblHojaVida_ID")] tblExperienciaProyectos tblExperienciaProyectos)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var proyecto = db.tblExperienciaProyectos.Where(m => m.tblHojaVida.AspNetUsers.Id == userid).Select(m => m.exppro_tituloProyecto).ToList();
                if (proyecto.Contains(tblExperienciaProyectos.exppro_tituloProyecto))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblExperienciaProyectos.tblHojaVida_ID);
                    ViewBag.tblUsuarioPlataforma_ID = new SelectList(db.tblExperienciaProyectos, "tblExperienciaProyectos_ID", "exppro_tituloProyecto", tblExperienciaProyectos.tblExperienciaProyectos_ID);
                    ViewBag.SqlError = "El proyecto ya existe";
                    return View(tblExperienciaProyectos);
                }

                db.tblExperienciaProyectos.Add(tblExperienciaProyectos);
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblExperienciaProyectos.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Investigacion", new { tabexpactive = 1 });
            }
            ViewBag.idHojaVida = tblExperienciaProyectos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblExperienciaProyectos.tblHojaVida.tblUsuarioPlataforma_ID;
            return View(tblExperienciaProyectos);
        }
        //GET: Modificar Proyecto
        // GET: ExperienciaProyectos/Edit/5
        public ActionResult ModificarProyecto(long? id) //Recibe id del Proyecto
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblExperienciaProyectos tblExperienciaProyectos = db.tblExperienciaProyectos.Find(id);
            if (tblExperienciaProyectos == null)
            {
                return HttpNotFound();
            }
            if (tblExperienciaProyectos.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.idHojaVida = tblExperienciaProyectos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblExperienciaProyectos.tblHojaVida.tblUsuarioPlataforma_ID;
            return View(tblExperienciaProyectos);
        }
        // POST: ExperienciaProyectos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarProyecto([Bind(Include = "tblExperienciaProyectos_ID,exppro_tituloProyecto,exppro_anoTerminacion,tblHojaVida_ID")] tblExperienciaProyectos tblExperienciaProyectos)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var proyecto = db.tblExperienciaProyectos.Where(m => m.tblHojaVida.AspNetUsers.Id == userid && m.tblExperienciaProyectos_ID!=tblExperienciaProyectos.tblExperienciaProyectos_ID).Select(m => m.exppro_tituloProyecto).ToList();
                if (proyecto.Contains(tblExperienciaProyectos.exppro_tituloProyecto))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblExperienciaProyectos.tblHojaVida_ID);
                    ViewBag.tblUsuarioPlataforma = userid;
                    ViewBag.SqlError = "El proyecto ya existe";
                    return View(tblExperienciaProyectos);
                }
                long id = long.Parse(Request.Form["tblExperienciaProyectos_ID"]);
                tblExperienciaProyectos tblExpPro = db.tblExperienciaProyectos.Find(id);
                if (tblExpPro.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblExpPro.exppro_tituloProyecto = Request.Form["exppro_tituloProyecto"];
                tblExpPro.exppro_anoTerminacion = DateTime.Parse(Request.Form["exppro_anoTerminacion"]);
                db.Entry(tblExpPro).State = EntityState.Modified;
                db.SaveChanges();

                tblHojaVida hojaVida = (from t in db.tblExperienciaProyectos where t.tblExperienciaProyectos_ID == tblExperienciaProyectos.tblExperienciaProyectos_ID select t.tblHojaVida).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Investigacion", new { tabexpactive = 1 });
            }
            ViewBag.idHojaVida = tblExperienciaProyectos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = AspNetUsers.GetUserId(User.Identity.Name);
            return View(tblExperienciaProyectos);
        }

        public ActionResult BorrarExperienciaProyecto(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblExperienciaProyectos miExperienciaProy = db.tblExperienciaProyectos.Find(id);
            if (miExperienciaProy.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.tblExperienciaProyectos.Remove(miExperienciaProy);
            db.SaveChanges();
            return RedirectToAction("Investigacion", new { tabexpactive = 1 });
        }


        //--Acciones control de Productos Academicos
        // GET: Productos Academicos/Create
        public ActionResult AdicionarProductoAcademico(long? id)// Recibe id de la hoja de vida
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == id select t).FirstOrDefault();
            if (hojaVida == null)
            {
                return HttpNotFound();
            }
            ViewBag.idHojaVida = id;
            ViewBag.idUsuarioPlataforma = hojaVida.tblUsuarioPlataforma_ID;
            ViewBag.tblCategoriaProductos_ID = new SelectList(db.tblCategoriaProductos, "tblCategoriaProductos_ID", "catpro_nombre");

            return View();
        }
        // POST: ExperienciaProyectos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdicionarProductoAcademico([Bind(Include = "tblProductosAcademicos_ID,proaca_tituloProducto,proaca_anoTerminacion,tblCategoriaProductos_ID,tblHojaVida_ID")] tblProductosAcademicos tblProductosAcademicos)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var productos = db.tblProductosAcademicos.Where(m => m.tblHojaVida.AspNetUsers.Id == userid).Select(m => m.proaca_tituloProducto).ToList();
                if (productos.Contains(tblProductosAcademicos.proaca_tituloProducto))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblProductosAcademicos.tblHojaVida_ID);
                    ViewBag.tblUsuarioPlataforma_ID = new SelectList(db.tblProductosAcademicos, "tblProductosAcademicos_ID", "proaca_tituloProducto", tblProductosAcademicos.tblProductosAcademicos_ID);
                    ViewBag.tblCategoriaProductos_ID = new SelectList(db.tblCategoriaProductos, "tblCategoriaProductos_ID", "catpro_nombre");
                    ViewBag.SqlError = "El producto ya existe";
                    return View(tblProductosAcademicos);
                }

                db.tblProductosAcademicos.Add(tblProductosAcademicos);
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblProductosAcademicos.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Investigacion", new { tabexpactive = 2 });
            }
            ViewBag.idHojaVida = tblProductosAcademicos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblProductosAcademicos.tblHojaVida.tblUsuarioPlataforma_ID;
            ViewBag.tblCategoriaProductos_ID = new SelectList(db.tblCategoriaProductos, "tblCategoriaProductos_ID", "catpro_nombre");

            return View(tblProductosAcademicos);
        }
        //GET: Modificar Proyecto
        // GET: ExperienciaProyectos/Edit/5
        public ActionResult ModificarProductoAcademico(long? id) //Recibe id del Producto Académico
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblProductosAcademicos tblProductosAcademicos = db.tblProductosAcademicos.Find(id);
            if (tblProductosAcademicos == null)
            {
                return HttpNotFound();
            }
            if (tblProductosAcademicos.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.idHojaVida = tblProductosAcademicos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblProductosAcademicos.tblHojaVida.tblUsuarioPlataforma_ID;
            ViewBag.tblCategoriaProductos_ID = new SelectList(db.tblCategoriaProductos, "tblCategoriaProductos_ID", "catpro_nombre", tblProductosAcademicos.tblCategoriaProductos_ID);
            return View(tblProductosAcademicos);
        }
        // POST: ExperienciaProyectos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarProductoAcademico([Bind(Include = "tblProductosAcademicos_ID,proaca_tituloProducto,proaca_anoTerminacion,tblCategoriaProductos_ID,tblHojaVida_ID")] tblProductosAcademicos tblProductosAcademicos)
        {
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var productos = db.tblProductosAcademicos.Where(m => m.tblHojaVida.AspNetUsers.Id == userid && m.tblProductosAcademicos_ID!= tblProductosAcademicos.tblProductosAcademicos_ID).Select(m => m.proaca_tituloProducto).ToList();
                if (productos.Contains(tblProductosAcademicos.proaca_tituloProducto))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblProductosAcademicos.tblHojaVida_ID);
                    ViewBag.tblUsuarioPlataforma = userid;
                    ViewBag.tblCategoriaProductos_ID = new SelectList(db.tblCategoriaProductos, "tblCategoriaProductos_ID", "catpro_nombre");
                    ViewBag.SqlError = "El producto ya existe";
                    return View(tblProductosAcademicos);
                }
                long id = long.Parse(Request.Form["tblProductosAcademicos_ID"]);
                tblProductosAcademicos tblProAca = db.tblProductosAcademicos.Find(id);
                if (tblProAca.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblProAca.proaca_tituloProducto = Request.Form["proaca_tituloProducto"];
                tblProAca.proaca_anoTerminacion = DateTime.Parse(Request.Form["proaca_anoTerminacion"]);
                tblProAca.tblCategoriaProductos_ID = long.Parse(Request.Form["tblCategoriaProductos_ID"]);
                db.Entry(tblProAca).State = EntityState.Modified;
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblProductosAcademicos.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Investigacion", new { tabexpactive = 2 });
            }
            ViewBag.idHojaVida = tblProductosAcademicos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = AspNetUsers.GetUserId(User.Identity.Name);
            ViewBag.tblCategoriaProductos_ID = new SelectList(db.tblCategoriaProductos, "tblCategoriaProductos_ID", "catpro_nombre");
            return View(tblProductosAcademicos);
        }

        public ActionResult BorrarProductosAcademicos(long? id)
        {
            tblProductosAcademicos miProductoAcademico = db.tblProductosAcademicos.Find(id);
            if (miProductoAcademico.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.tblProductosAcademicos.Remove(miProductoAcademico);
            db.SaveChanges();
            return RedirectToAction("Investigacion", new { tabexpactive = 2 });
        }


        //--Acciones control de Eventos Académicos
        public ActionResult AdicionarEventoAcademico(long? id)// Recibe id de la hoja de vida
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == id select t).FirstOrDefault();
            if (hojaVida == null)
            {
                return HttpNotFound();
            }
            ViewBag.idHojaVida = id;
            ViewBag.idUsuarioPlataforma = hojaVida.tblUsuarioPlataforma_ID;
            return View();
        }
        // POST: ExperienciaProyectos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdicionarEventoAcademico([Bind(Include = "tblEventosAcademicos_ID,eveaca_tituloEvento,eveaca_evento,eveaca_lugarEvento,eveaca_anoTerminacion,tblHojaVida_ID")] tblEventosAcademicos tblEventosAcademicos)
        {
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var eventos = db.tblEventosAcademicos.Where(m => m.tblHojaVida.AspNetUsers.Id == userid).Select(m => m.eveaca_tituloEvento).ToList();
                if (eventos.Contains(tblEventosAcademicos.eveaca_tituloEvento))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblEventosAcademicos.tblHojaVida_ID);
                    ViewBag.tblUsuarioPlataforma_ID = new SelectList(db.tblEventosAcademicos, "tblProductosAcademicos_ID", "proaca_tituloProducto", tblEventosAcademicos.tblEventosAcademicos_ID);                    
                    ViewBag.SqlError = "El evento ya existe";
                    return View(tblEventosAcademicos);
                }

                db.tblEventosAcademicos.Add(tblEventosAcademicos);
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblEventosAcademicos.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Investigacion", new { tabexpactive = 3 });
            }
            ViewBag.idHojaVida = tblEventosAcademicos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblEventosAcademicos.tblHojaVida.tblUsuarioPlataforma_ID;
            return View(tblEventosAcademicos);
        }
        //GET: Modificar Proyecto
        // GET: ExperienciaProyectos/Edit/5
        public ActionResult ModificarEventoAcademico(long? id) //Recibe id del Evento Académico
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblEventosAcademicos tblEventosAcademicos = db.tblEventosAcademicos.Find(id);
            if (tblEventosAcademicos == null)
            {
                return HttpNotFound();
            }
            if (tblEventosAcademicos.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.idHojaVida = tblEventosAcademicos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = tblEventosAcademicos.tblHojaVida.tblUsuarioPlataforma_ID;
            return View(tblEventosAcademicos);
        }
        // POST: ExperienciaProyectos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificarEventoAcademico([Bind(Include = "tblEventosAcademicos_ID,eveaca_tituloEvento,eveaca_evento,eveaca_lugarEvento,eveaca_anoTerminacion,tblHojaVida_ID")] tblEventosAcademicos tblEventosAcademicos)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                string userid = AspNetUsers.GetUserId(User.Identity.Name);
                var eventos = db.tblEventosAcademicos.Where(m => m.tblHojaVida.AspNetUsers.Id == userid && m.tblEventosAcademicos_ID!= tblEventosAcademicos.tblEventosAcademicos_ID).Select(m => m.eveaca_tituloEvento).ToList();
                if (eventos.Contains(tblEventosAcademicos.eveaca_tituloEvento))
                {
                    ViewBag.tblHojaVida_ID = new SelectList(db.tblHojaVida, "tblHojaVida_ID", "hojVid_tituloSecundaria", tblEventosAcademicos.tblHojaVida_ID);
                    ViewBag.tblUsuarioPlataforma = userid;
                    ViewBag.SqlError = "El evento ya existe";
                    return View(tblEventosAcademicos);
                }
                long id = long.Parse(Request.Form["tblEventosAcademicos_ID"]);
                tblEventosAcademicos tblEveAca = db.tblEventosAcademicos.Find(id);
                if (tblEveAca.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tblEveAca.eveaca_tituloEvento = Request.Form["eveaca_tituloEvento"];
                tblEveAca.eveaca_evento = Request.Form["eveaca_evento"];
                tblEveAca.eveaca_lugarEvento = Request.Form["eveaca_lugarEvento"];
                tblEveAca.eveaca_anoTerminacion = DateTime.Parse(Request.Form["eveaca_anoTerminacion"]);
                db.Entry(tblEveAca).State = EntityState.Modified;
                db.SaveChanges();
                tblHojaVida hojaVida = (from t in db.tblHojaVida where t.tblHojaVida_ID == tblEventosAcademicos.tblHojaVida_ID select t).FirstOrDefault();
                if (hojaVida == null)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Investigacion", new { tabexpactive = 3 });
            }
            ViewBag.idHojaVida = tblEventosAcademicos.tblHojaVida_ID;
            ViewBag.idUsuarioPlataforma = AspNetUsers.GetUserId(User.Identity.Name);
            return View(tblEventosAcademicos);
        }

        public ActionResult BorrarEventosAcademicos(long? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Maestro", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            tblEventosAcademicos miEventoAcademico = db.tblEventosAcademicos.Find(id);
            if (miEventoAcademico.tblHojaVida.AspNetUsers.UserName != User.Identity.Name)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.tblEventosAcademicos.Remove(miEventoAcademico);
            db.SaveChanges();
            return RedirectToAction("Investigacion", new { tabexpactive = 3 });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}