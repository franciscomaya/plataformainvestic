﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using dl = ClassLibrary;

namespace INI.Controllers
{
    //[Authorize(Roles = "Administrator,Maestro")]
    [Authorize]
    public class EjeInvestigacionController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: EjeInvestigacion
        public ActionResult Index(int code=0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ViewBag.Message = dl.ErrorCodes.ErrorCodeToString(code);
            return View(db.tblEjeInvestigacion.ToList());
        }

        // GET: EjeInvestigacion/Details/5
        public ActionResult Details(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblEjeInvestigacion tblEjeInvestigacion = db.tblEjeInvestigacion.Find(id);
            if (tblEjeInvestigacion == null)
            {
                return HttpNotFound();
            }
            return View(tblEjeInvestigacion);
        }

        // GET: EjeInvestigacion/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View();
        }

        // POST: EjeInvestigacion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tblEjeInvestigacion_ID,ejeInv_nombre")] tblEjeInvestigacion tblEjeInvestigacion)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblEjeInvestigacion variablep = db.tblEjeInvestigacion.SingleOrDefault(model => model.ejeInv_nombre == tblEjeInvestigacion.ejeInv_nombre);
                if (variablep == null)
                {
                    db.tblEjeInvestigacion.Add(tblEjeInvestigacion);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }
            }

            return View(tblEjeInvestigacion);
        }

        // GET: EjeInvestigacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblEjeInvestigacion tblEjeInvestigacion = db.tblEjeInvestigacion.Find(id);
            if (tblEjeInvestigacion == null)
            {
                return HttpNotFound();
            }
            return View(tblEjeInvestigacion);
        }

        // POST: EjeInvestigacion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tblEjeInvestigacion_ID,ejeInv_nombre")] tblEjeInvestigacion tblEjeInvestigacion)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                tblEjeInvestigacion variablep = db.tblEjeInvestigacion.SingleOrDefault(model => model.ejeInv_nombre == tblEjeInvestigacion.ejeInv_nombre && model.tblEjeInvestigacion_ID != tblEjeInvestigacion.tblEjeInvestigacion_ID);
                if (variablep == null)
                {
                    db.Entry(tblEjeInvestigacion).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index", new { code = 13905 });
                }

            }
            return View(tblEjeInvestigacion);
        }

        // GET: EjeInvestigacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblEjeInvestigacion tblEjeInvestigacion = db.tblEjeInvestigacion.Find(id);
            if (tblEjeInvestigacion == null)
            {
                return HttpNotFound();
            }
            return View(tblEjeInvestigacion);
        }

        // POST: EjeInvestigacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            try
            {
                tblEjeInvestigacion tblEjeInvestigacion = db.tblEjeInvestigacion.Find(id);
                db.tblEjeInvestigacion.Remove(tblEjeInvestigacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { code = 1390304 });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
