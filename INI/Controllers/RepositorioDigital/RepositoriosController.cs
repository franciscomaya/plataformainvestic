﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using INI.Models.DataBase;
using System.IO;
using System.ComponentModel;
using System.Drawing;

namespace INI.Controllers.RepositorioDigital
{
    //[Authorize(Roles = "Administrator,Editor")]
    [Authorize]
    public class RepositoriosController : Controller
    {
        private investicEntities db = new investicEntities();

        // GET: Repositorios
        public ActionResult Index()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var Repositorio = db.Repositorio.Include(r => r.SubCategoriaRepositorio);
            return View(Repositorio.ToList());
        }

        // GET: Repositorios/Details/5
        public ActionResult Details(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Repositorio Repositorio = db.Repositorio.Find(id);
            if (Repositorio == null)
            {
                return HttpNotFound();
            }
            return View(Repositorio);
        }

        // GET: Repositorios/Create
        public ActionResult Create()
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var q = db.CategoriaRepositorio.OrderBy(m => m.id).FirstOrDefault();
            if (q == null) return RedirectToAction("Index");
            var qsc = db.SubCategoriaRepositorio.Where(m => m.id_categoria == q.id).ToList();
            if (qsc == null || qsc.Count==0) return RedirectToAction("Index");
            ViewBag.id_SubCategoria = new SelectList(qsc, "id", "nombre");
            ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id), "id", "nombre");
            return View();
        }

        // POST: Repositorios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "title,urlRepositorio,id_SubCategoria,Description,urlfront")] Repositorio Repositorio)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {

                string folderPath = "";
                string folderPath2 = "";
                string PathforDB = "";
                string filename = "";
                string savedfileName;
                string edicion = Repositorio.title;
                string subcategoria = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).First().nombre;
                string categoria = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).First().CategoriaRepositorio.nombre;


                folderPath = Server.MapPath("~/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/");
                folderPath2 = folderPath + "images/";
                PathforDB = "/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/";



                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    if (!Directory.Exists(folderPath2))
                        Directory.CreateDirectory(folderPath2);


                    filename = string.Format("{0}-{1}",
                            DateTime.Now.ToString("ddMMyyyyHHmmss"),
                            Path.GetFileName(hpf.FileName));


                    if (file == "urlfront")
                    {
                        
                        savedfileName = folderPath + filename;
                        hpf.SaveAs(savedfileName);
                        Repositorio.urlfront = PathforDB + filename;
                    }

                }
                db.Repositorio.Add(Repositorio);

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {                    
                    if (ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key"))
                    {
                        ViewBag.SqlError = "El nombre ya existe";
                    }
                    else
                    {
                        ViewBag.SqlError = ex.InnerException.InnerException.Message;                        
                    }
                    var q1 = db.CategoriaRepositorio.OrderBy(m => m.id).First();
                    var qsc1 = db.SubCategoriaRepositorio.Where(m => m.id_categoria == q1.id);
                    if (Repositorio.SubCategoriaRepositorio == null)
                    {
                        ViewBag.id_SubCategoria = new SelectList(qsc1, "id", "nombre");
                        ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id), "id", "nombre");
                    }
                    else
                    {
                        ViewBag.id_SubCategoria = new SelectList(qsc1, "id", "nombre", Repositorio.id_SubCategoria);
                        ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id), "id", "nombre", Repositorio.SubCategoriaRepositorio.id_categoria);
                    }
                    return View(Repositorio);
                }


                
            }
            var q = db.CategoriaRepositorio.OrderBy(m => m.id).First();
            var qsc = db.SubCategoriaRepositorio.Where(m => m.id_categoria == q.id);
            if (Repositorio.SubCategoriaRepositorio == null)
            {
                ViewBag.id_SubCategoria = new SelectList(qsc, "id", "nombre");
                ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id), "id", "nombre");
            }
            else
            {
                ViewBag.id_SubCategoria = new SelectList(qsc, "id", "nombre", Repositorio.id_SubCategoria);
                ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id), "id", "nombre",Repositorio.SubCategoriaRepositorio.id_categoria);
            }
            return View(Repositorio);
        }


        // GET: Repositorios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Repositorio Repositorio = db.Repositorio.Find(id);
            if (Repositorio == null)
            {
                return HttpNotFound();
            }
            var q = db.CategoriaRepositorio.OrderBy(m => m.id).Select(m=>new {m.id, m.nombre});
            var qsc = db.SubCategoriaRepositorio.Where(m => m.id_categoria == Repositorio.SubCategoriaRepositorio.id_categoria);
            ViewBag.id_SubCategoria = new SelectList(qsc, "id", "nombre", Repositorio.id_SubCategoria);
            ViewBag.id_Categoria = new SelectList(q, "id", "nombre", Repositorio.SubCategoriaRepositorio.id_categoria);
            return View(Repositorio);
        }

        // POST: Repositorios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,title,urlRepositorio,id_SubCategoria,Description,urlfront")] Repositorio Repositorio)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (ModelState.IsValid)
            {
                string folderPathPDf = "";
                string folderPathFront = "";


                string filePathPDF = "";
                string filePathFront = "";

                string folderPath = "";
                string folderPath2 = "";
                string folderPathOld = "";
                string PathforDB = "";
                string filename = "";
                string savedfileName;
                string edicion = Repositorio.title;
                string subcategoria = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).First().nombre;
                string categoria = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).First().CategoriaRepositorio.nombre;

                string oldCategoria = (from m in db.Repositorio where m.id == Repositorio.id select m.SubCategoriaRepositorio.CategoriaRepositorio.nombre).FirstOrDefault();
                string oldSubCategoria = (from m in db.Repositorio where m.id == Repositorio.id select m.SubCategoriaRepositorio.nombre).FirstOrDefault();
                string oldEdicion = (from m in db.Repositorio where m.id == Repositorio.id select m.title).FirstOrDefault();                
                string oldUrlFront = (from m in db.Repositorio where m.id == Repositorio.id select m.urlfront).FirstOrDefault();

                folderPath = Server.MapPath("~/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/");
                folderPathFront = Server.MapPath("~/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/");
                folderPathOld = Server.MapPath("~/images/Repositorio/" + oldCategoria + "/" + oldSubCategoria + "/" + oldEdicion + "/");
                folderPath2 = folderPath + "images/";

                
                filePathFront = Server.MapPath("~" + oldUrlFront);

                PathforDB = "/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/";

                try
                {
                    db.Entry(Repositorio).State = EntityState.Modified;
                    db.SaveChanges();
                    

                    if (oldCategoria != categoria || oldSubCategoria != subcategoria)
                    {

                        if (Directory.Exists(folderPathOld))
                        {
                            //if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);

                            Directory.Move(folderPathOld, folderPath);

                            oldUrlFront = PathforDB + Path.GetFileName(filePathFront);

                            oldCategoria = categoria;
                            oldSubCategoria = subcategoria;

                            folderPathOld = Server.MapPath("~/images/Repositorio/" + oldCategoria + "/" + oldSubCategoria + "/" + oldEdicion + "/");

                            filePathFront = Server.MapPath("~" + oldUrlFront);

                            PathforDB = "/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/";
                        }

                    }
                    else if (oldEdicion != edicion)
                    {
                        Directory.Move(folderPathOld, folderPath);

                        oldUrlFront = PathforDB + Path.GetFileName(filePathFront);

                        filePathFront = Server.MapPath("~" + oldUrlFront);
                    }


                    Repositorio.urlfront = oldUrlFront;



                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        if (hpf.ContentLength == 0)
                            continue;

                        folderPathPDf = Server.MapPath("~/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/");
                        folderPath2 = folderPathPDf + "images/";
                        PathforDB = "/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/";



                        filename = string.Format("{0}-{1}",
                                DateTime.Now.ToString("ddMMyyyyHHmmss"),
                                Path.GetFileName(hpf.FileName));

                        if (file == "urlfront_file")
                        {
                            if (!String.IsNullOrEmpty(oldUrlFront))
                            {
                                if (System.IO.File.Exists(filePathFront)) System.IO.File.Delete(filePathFront);
                            }
                            savedfileName = folderPathFront + filename;
                            hpf.SaveAs(savedfileName);
                            Repositorio.urlfront = PathforDB + filename;
                        }

                    }
                    db.Entry(Repositorio).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key"))
                    {
                        ViewBag.SqlError = "El nombre ya existe en esta subcategoría";
                    }
                    else
                    {
                        ViewBag.SqlError = ex.InnerException.InnerException.Message;
                    }
                    var q1 = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).Select(m => new { m.id_categoria }).First();
                    var qsc1 = db.SubCategoriaRepositorio.Where(m => m.id_categoria == q1.id_categoria);
                    ViewBag.id_SubCategoria = new SelectList(qsc1, "id", "nombre", Repositorio.id_SubCategoria);
                    ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id).Select(m => new { m.id, m.nombre }), "id", "nombre", q1.id_categoria);
                    return View(Repositorio);
                }

                
                
               
            }
            var q = db.SubCategoriaRepositorio.Where(m=>m.id==Repositorio.id_SubCategoria).Select(m=>new {m.id_categoria}).First();
            var qsc = db.SubCategoriaRepositorio.Where(m => m.id_categoria == q.id_categoria);
            ViewBag.id_SubCategoria = new SelectList(qsc, "id", "nombre", Repositorio.id_SubCategoria);
            ViewBag.id_Categoria = new SelectList(db.CategoriaRepositorio.OrderBy(m => m.id).Select(m => new { m.id, m.nombre }), "id", "nombre", q.id_categoria);
            return View(Repositorio);
        }

        // GET: Repositorios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Repositorio Repositorio = db.Repositorio.Find(id);
            if (Repositorio == null)
            {
                return HttpNotFound();
            }
            return View(Repositorio);
        }

        // POST: Repositorios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Repositorio Repositorio = db.Repositorio.Find(id);
            string edicion = Repositorio.title;
            string subcategoria = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).First().nombre;
            string categoria = db.SubCategoriaRepositorio.Where(m => m.id == Repositorio.id_SubCategoria).First().CategoriaRepositorio.nombre;
            db.Repositorio.Remove(Repositorio);
            String folderPath = Server.MapPath("~/images/Repositorio/" + categoria + "/" + subcategoria + "/" + edicion + "/");
            try
            {
                if (Directory.Exists(folderPath)) Directory.Delete(folderPath, true);
            }
            catch
            {

            }
            finally
            {
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult GetSubcategoria(int id = 0)
        {
            if (!(AspNetUsersRoles.IsUserInRole("Administrator", User.Identity.Name) || AspNetUsersRoles.IsUserInRole("Editor", User.Identity.Name))) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var qsc = from q in db.SubCategoriaRepositorio where q.id_categoria == id select new { id = q.id, nombre = q.nombre };
            return Json(qsc, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
