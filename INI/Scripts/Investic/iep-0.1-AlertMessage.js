﻿function AlertMessage(msj) {
    if (msj != "") {
        var vencabezado = "Información";
        //var vcuerpo = "Mensaje informativo sobre algún suceso de la plataforma";
        var vcuerpo = msj;
        var modalw = document.createElement("div");
        modalw.innerHTML = '<div class="modal fade" id="myModal" role="dialog"><div class="modal-dialog modal-md"><div class="modal-content" style="background-color: #cccccc; background-image: url(/images/fondo_enterate.png); background-size: 222px 200px; background-repeat: repeat; background-position:20px top;"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="font-family: Chewy, serif; font-size: 24px; font-weight:100; text-shadow: 4px 4px 4px #aaa;">' + vencabezado + '</h4></div><div class="modal-body"><p style="font-family: Roboto, serif; font-size: 16px; font-weight:100;">' + vcuerpo + '</p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #3e97df; color:#ffffff; font-weight:normal;">Cerrar</button></div></div></div></div>';

        var buttonw = document.createElement("button");
        buttonw.id = "watchButton";
        buttonw.name = "watchButton";
        buttonw.type = "button";
        buttonw.className = "btn btn-info btn-lg";
        buttonw.setAttribute("data-toggle", "modal");
        buttonw.setAttribute("data-target", "#myModal");
        buttonw.style.display = "none";
        //buttonw.setAttribute("data-backdrop", "static");
        //buttonw.setAttribute("data-keyboard", "false");
        //buttonw.style.display = "none";

        document.body.appendChild(modalw);
        document.body.appendChild(buttonw);

        $(document).ready(function () {
            $("#watchButton").trigger('click');
        });
    }
}