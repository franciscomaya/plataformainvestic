﻿function ConfirmDelete(id) {
    window.location = "@Url.Action("Delete","CategoriaRevistas")/" + id;
}
function DeleteCategoria(id) {
            
    $.getJSON("@Url.Action("DeleteIsPosible","CategoriaRevistas")/"+id,function(data){
        if (data.rta == "Ok") {
            $.Dialog({
                overlay: true,
                shadow: true,
                flat: true,
                icon: '<span class="icon-remove"></span>',
                title: 'Confirmación',
                width: 400,
                content: '',
                onShow: function (_dialog) {
                    var content =
                        '<div class="confirmbox">' +
                        '<p>Esta seguro que desea eliminar la categoría porque contiene subcategorías asociadas</p>' +
                        '<button class="button primary" onclick="javascript:ConfirmDelete(' + id +
                        ');">Continuar</button> ' +
                        '<button class="button" type="button" onclick="$.Dialog.close()">Cancel</button> ' +
                        '</div>';
                    $.Dialog.content(content);
                }
            });
        }
        else if (data.rta == "Bad") {
            $.Dialog({
                overlay: true,
                shadow: true,
                flat: true,
                icon: '<span class="icon-remove"></span>',
                title: 'Confirmación',
                width: 400,
                content: '',
                onShow: function (_dialog) {
                    var content =
                        '<div class="confirmbox">' +
                        '<p>No se puede eliminar la categoría porque contiene subcategorías asociadas</p>' +                                
                        '<button class="button" type="button" onclick="$.Dialog.close()">Cancel</button> ' +
                        '</div>';
                    $.Dialog.content(content);
                }
            });

        }

    });

            
}