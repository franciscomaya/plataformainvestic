﻿$(function () {
    $('#nuevo-foro').dialog({
        autoOpen: false,
        height: 400,
        width: 980,
        modal: true,
        title: "Nuevo Foro",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $('#nueva-respuesta').dialog({
        autoOpen: false,
        height: 400,
        width: 980,
        modal: true,
        title: "Responder Foro",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $('#nueva-replica').dialog({
        autoOpen: false,
        height: 400,
        width: 980,
        modal: true,
        title: "Replicar respuesta",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#lnkCrearForo").on("click", function (e) {
        //e.preventDefault(); //use this or return false                
        $("#nuevo-foro").dialog('open');
        return false;
    });

    
});

function Responder(id, titulo) {
    $("#nueva-respuesta").dialog('open');
    var elementid = $("#fororta input")[3];
    $(elementid).val(id);
    var elementid = $("#fororta input")[6];
    $(elementid).val(titulo);
    
    //$("#fororta Titulo").attr("value", titulo);    
    return false;
}

function Replicar(id, titulo) {
    $("#nueva-replica").dialog('open');
    var elementid = $("#fororpl input")[3];
    $(elementid).val(id);
    var elementid = $("#fororpl input")[6];
    $(elementid).val(titulo);

    //$("#fororta Titulo").attr("value", titulo);    
    return false;
}

function Nuevo() {
    $("#nuevo-foro").dialog('open');
    document.getElementById("Titulo").readOnly = false;
    document.getElementById("Titulo").value = "";
    document.getElementById("enviarForo").innerHTML = "Crear foro";
    document.getElementById("ui-id-1").innerHTML = "Crear foro";
    return false;
}

function DeletePresupuesto(id) {
    $.Dialog({
        overlay: true,
        shadow: true,
        flat: true,
        icon: '<span class="icon-remove"></span>',
        title: 'Confirmación',
        width: 400,
        content: '',
        onShow: function (_dialog) {
            var content =
                '<div class="confirmbox">' +
                '<p>Desea continuar con la eliminación del registro?</p>' +
                '<button class="button primary" onclick="javascript:ConfirmDelete(' + id +
                ');">Continuar</button> ' +
                '<button class="button" type="button" onclick="$.Dialog.close()">Cancelar</button> ' +
                '</div>';
            $.Dialog.content(content);
        }
    });
}

function ConfirmDelete(id) {
    $.Dialog.close();
    window.location.href = "/AsistenteGruposInvestigacion/EliminarPresupuesto/".concat(id);
}