﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using INI.Models.DataBase;
namespace INI
{
    public class Conexion_bd
    {
        SqlConnection cn;
        SqlCommand cmd;
        SqlDataReader dr;
        string mensaje;
        private tblInfoGeneral info = new tblInfoGeneral();
        private tblLinkCVEMP infoCVEMP = new tblLinkCVEMP();

        public Conexion_bd()
        {
            try
            {
                cn = new SqlConnection("Data Source=investic.udenar.edu.co;Initial Catalog=investic;User ID=usupla;Password=prog*2014;MultipleActiveResultSets=True;Application Name=EntityFramework");
                cn.Open();
                mensaje = "Conectado con base de  datos";
            }catch(Exception ex){
                mensaje = "Error de conexion" + ex.ToString();
            }
        }
        public Boolean Ejecutar(string sql)
        {
            mensaje = "Actualización con exito!";
            try
            {
                cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
                return false;
            }
            //return mensaje;
        }
        public int contador(string sql){
            mensaje = "contador";
            int con = 0;

            try
            {
                cmd = new SqlCommand(sql, cn);
                con = (int)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
            }
            return con;
        }
        public List<tblInfoGeneral> listar()
        {
            string mensaje = "";
            List<tblInfoGeneral> lista = new List<tblInfoGeneral>();

            string sql = "SELECT * FROM tblInfoGeneral WHERE tblInfoGeneral.id=1";
            try
            {
                cmd = new SqlCommand(sql, cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    info.id = dr.GetInt32(0);
                    info.nombre = dr.GetString(1);
                    info.campo = dr.GetString(2);
                    info.password = dr.GetString(3);
                    lista.Add(info);
                }
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
            }
            cn.Close();
            return lista;
        }
        public List<tblLinkCVEMP> listarLinkCVEMP()
        {
            string mensaje = "";
            List<tblLinkCVEMP> datos = new List<tblLinkCVEMP>();
            //List<string> datos = new List<string>();
            string sql = "SELECT * FROM tblLinkCVEMP";
            try
            {
                cmd = new SqlCommand(sql, cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    infoCVEMP.id = dr.GetInt32(0);
                    infoCVEMP.categoria = dr.GetString(1);
                    infoCVEMP.elemento = dr.GetString(2);
                    infoCVEMP.link = dr.GetString(3);
                    //infoCVEMP.id = Convert.ToInt16(dr[0]);

                    datos.Add(infoCVEMP);

                }
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
            }
            cn.Close();
            return datos;
        }
        public List<string> listarLinkCVEMP1()
        {
            string mensaje = "";
            List<string> datos = new List<string>();
            string sql = "SELECT * FROM tblLinkCVEMP";
            try
            {
                cmd = new SqlCommand(sql, cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    infoCVEMP.id = dr.GetInt32(0);
                    infoCVEMP.categoria = dr.GetString(1);
                    infoCVEMP.elemento = dr.GetString(2);
                    infoCVEMP.link = dr.GetString(3);
                    datos.Add(Convert.ToString(infoCVEMP.id) + "," + Convert.ToString(infoCVEMP.categoria) + "," + Convert.ToString(infoCVEMP.elemento) + "," + Convert.ToString(infoCVEMP.link));

                }
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
            }
            cn.Close();
            return datos;
        }
        public List<tblLinkCVEMP> EditLista(int id)
        {
            string mensaje = "";
            List<tblLinkCVEMP> lista = new List<tblLinkCVEMP>();

            string sql = "SELECT * FROM tblLinkCVEMP WHERE tblLinkCVEMP.id=" + id;
            try
            {
                cmd = new SqlCommand(sql, cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    infoCVEMP.id = dr.GetInt32(0);
                    infoCVEMP.categoria = dr.GetString(1);
                    infoCVEMP.elemento = dr.GetString(2);
                    infoCVEMP.link = dr.GetString(3);
                    lista.Add(infoCVEMP);
                }
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
            }
            cn.Close();
            return lista;
        }
        public List<tblLinkCVEMP> ReturLista(int id)
        {
            string mensaje = "";
            List<tblLinkCVEMP> lista = new List<tblLinkCVEMP>();

            string sql = "SELECT * FROM tblLinkCVEMP WHERE tblLinkCVEMP.id=" + id;
            try
            {
                cmd = new SqlCommand(sql, cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    infoCVEMP.id = dr.GetInt32(0);
                    infoCVEMP.categoria = dr.GetString(1);
                    infoCVEMP.elemento = dr.GetString(2);
                    infoCVEMP.link = dr.GetString(3);
                    lista.Add(infoCVEMP);
                }
            }
            catch (Exception ex)
            {
                mensaje = "Error de conexion" + ex.ToString();
            }
            cn.Close();
            return lista;
        }
    }
}