﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporLibrary.Models
{
    public class CropDates
    {
        public String CropName { get; set; }
        public double Humidity { get; set; }
        public bool IsElectrovalveStart { get; set; }
    }
}
