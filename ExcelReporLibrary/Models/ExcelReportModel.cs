﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporLibrary.Models
{
    public class ExcelReportModel
    {
        //public String FechaInicial { get; set; }
        //public String Institution { get; set; }
        //public String Latitud { get; set; }
        //public String Longitud { get; set; }
        //public String Altitud { get; set; }        

        public List<LogRow>LstLog{ get; set; }
    }

    public class NavigationLog_
    {

        public int id { get; set; }

        public int reporttype { get; set; }

        public string Rol { get; set; }


        public string Usuario { get; set; }


        public string IP { get; set; }


        public string Latitud { get; set; }


        public string Longitud { get; set; }


        public string Altitud { get; set; }

        public Nullable<System.DateTime> FechaInicioSesion { get; set; }

        public Nullable<System.DateTime> FechaCierreSesion { get; set; }

        public List<NavegationModel_> navigations { get; set; }
    }
}
