﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporLibrary.Models
{
    public class LogRow
    {
        public String Rol { get; set; }
        public String Usuario { get; set; }
        public String Nombre { get; set; }
        public String IP { get; set; }
        public String Latitud { get; set; }
        public String Longitud { get; set; }
        public String Altitud { get; set; }
        public DateTime FechaInicioSesion { get; set; }
        public DateTime FechaCierreSesion { get; set; }
        
    }

    public class NavegationModel_
    {
        public DateTime ImputDateUrl { get; set; }
        public String Url { get; set; }
    }

}
