﻿using ClosedXML.Excel;
using ExcelReporLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReporLibrary
{
    public class ExcelReport
    {
        public static XLWorkbook Lista()
        {
            var book = new XLWorkbook();

            var sheet = book.Worksheets.Add("Lista");

            sheet.Column("A").Width = 30;
            sheet.Column("B").Width = 30;
            sheet.Column("C").Width = 30;
            sheet.Column("D").Width = 30;
            sheet.Column("E").Width = 30;
            sheet.Column("F").Width = 30;
            sheet.Column("G").Width = 30;

            sheet.Cell("A1").Value = "Simple";

            sheet.Cell("A2").Value = 1;
            sheet.Cell("A3").Value = 3;
            sheet.Cell("A4").SetFormulaA1("=A2+A3");

            sheet.Cell("B1").Value = "Formulas";
            sheet.Cell("B2").SetValue(34780)
                .Style
                .NumberFormat.Format = "$ #,##0.00";

            sheet.SheetView.Freeze(2, 2);

            sheet.Range("C1:F1")
                .Merge()
                .SetValue("Rangos")
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            return book;
        }

        public static XLWorkbook Lista(string archivo)
        {
            var workbook = new XLWorkbook(archivo);

            var worksheet = workbook.Worksheet("Hoja1");

            worksheet.Cell("B7").Value = "Aula 208 Ingenieria";
            worksheet.Cell("B8").Value = System.DateTime.Now.ToShortDateString();
            worksheet.Cell("B9").Value = "Capacitacion Excel";
            worksheet.Cell("B10").Value = "Carlos Rojas";

            string tmp = worksheet.Cell("A7").GetString();
            worksheet.Cell("D7").Value = tmp;

            int j = 12;
            for (int i = 0; i < 10; i++)
            {
                j++;
                worksheet.Cell(string.Format("A{0}", j.ToString())).Value = i + 1;
                worksheet.Cell(string.Format("B{0}", j.ToString())).Value = "Héctor Mora";
                worksheet.Cell(string.Format("C{0}", j.ToString())).Value = j;
            }

            return workbook;
        }

        public static XLWorkbook HistoricReport(string archivo, ExcelReportModel r)
        {
            var workbook = new XLWorkbook(archivo);
            var worksheet = workbook.Worksheet("Hoja1");

            worksheet.Column("B").Width = 15;
            worksheet.Column("C").Width = 15;
            worksheet.Column("D").Width = 45;
            worksheet.Column("E").Width = 15;
            worksheet.Column("F").Width = 15;
            worksheet.Column("G").Width = 12;
            worksheet.Column("H").Width = 16;
            worksheet.Column("I").Width = 16;
                                          
            List<LogRow> trames=r.LstLog;
            int iniinfo = 7;
            int lastinfo= iniinfo+trames.Count;

            worksheet.Cell(string.Format("B{0}", iniinfo-1)).Value = "Rol";
            worksheet.Cell(string.Format("C{0}", iniinfo-1)).Value = "Usuario";
            worksheet.Cell(string.Format("D{0}", iniinfo - 1)).Value = "Nombre";
            worksheet.Cell(string.Format("E{0}", iniinfo-1)).Value = "Latitud";
            worksheet.Cell(string.Format("F{0}", iniinfo-1)).Value = "Longitud";
            worksheet.Cell(string.Format("G{0}", iniinfo-1)).Value = "IP";
            worksheet.Cell(string.Format("H{0}", iniinfo-1)).Value = "Fecha inicio";
            worksheet.Cell(string.Format("I{0}", iniinfo-1)).Value = "Fecha fin";

            for (int i = iniinfo; i < lastinfo; i++)
            {
                worksheet.Cell(string.Format("B{0}", i.ToString())).Value = trames[i - iniinfo].Rol;
                worksheet.Cell(string.Format("C{0}", i.ToString())).Value = trames[i - iniinfo].Usuario;
                worksheet.Cell(string.Format("D{0}", i.ToString())).Value = trames[i - iniinfo].Nombre;
                worksheet.Cell(string.Format("E{0}", i.ToString())).Value = trames[i - iniinfo].Latitud;
                worksheet.Cell(string.Format("F{0}", i.ToString())).Value = trames[i - iniinfo].Longitud;
                worksheet.Cell(string.Format("G{0}", i.ToString())).Value = trames[i - iniinfo].IP;
                worksheet.Cell(string.Format("H{0}", i.ToString())).Value = trames[i - iniinfo].FechaInicioSesion;
                worksheet.Cell(string.Format("I{0}", i.ToString())).Value = trames[i - iniinfo].FechaCierreSesion;               
            }
            //worksheet.Cell(iniinfo, "B").Value=trames;
            var range = worksheet.Range(string.Format("B{0}:I{1}",iniinfo-1,lastinfo-1));
            var table = range.CreateTable();           
            table.ShowTotalsRow = true;            
            return workbook;
        }

        public static XLWorkbook HistoricNavigateReport(string archivo, NavigationLog_ r)
        {
            var workbook = new XLWorkbook(archivo);
            var worksheet = workbook.Worksheet("Hoja1");

            worksheet.Column("B").Width = 16;
            worksheet.Column("C").Width = 119;


            List<NavegationModel_> trames = r.navigations;
            int iniinfo = 12;
            int lastinfo = iniinfo + trames.Count;

            worksheet.Cell("B6").Value = "Nombre";
            worksheet.Cell("C6").Value = r.Usuario;
            worksheet.Cell("B7").Value = "Rol";
            worksheet.Cell("C7").Value = r.Rol;
            worksheet.Cell("B8").Value = "IP";
            worksheet.Cell("C8").Value = r.IP;

            worksheet.Cell(string.Format("B{0}", iniinfo-1)).Value = "Fecha";
            worksheet.Cell(string.Format("C{0}", iniinfo-1)).Value = "URL";

            for (int i = iniinfo; i < lastinfo; i++)
            {
                worksheet.Cell(string.Format("B{0}", i.ToString())).Value = trames[i - iniinfo].ImputDateUrl;
                worksheet.Cell(string.Format("C{0}", i.ToString())).Value = trames[i - iniinfo].Url;
                
            }
            //worksheet.Cell(iniinfo, "B").Value=trames;
            var range = worksheet.Range(string.Format("B{0}:C{1}", iniinfo - 1, lastinfo-1));
            var table = range.CreateTable();
            table.ShowTotalsRow = true;
            return workbook;
        }
    }
}

